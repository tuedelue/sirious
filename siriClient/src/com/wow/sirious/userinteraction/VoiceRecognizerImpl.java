package com.wow.sirious.userinteraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import com.wow.sirious.MyLog;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.data.MyPreferencesManager;

public class VoiceRecognizerImpl implements VoiceRecognizer {

	private static final int			MAX_RESULTS					= 10;
	private static String				NOTHING_RECOGNIZED;

	/**
	 * recognitions for a request-id
	 */
	private Map<String, Recognition>	mRecognitions				= new HashMap<String, Recognition>();

	volatile private boolean			mListening					= false;
	private Handler						mListeningWatchDogHandler	= new Handler();
	private WatchDogRunnable			mListeningWatchDogRunnable	= new WatchDogRunnable();
	private long						WATCH_DOG_MILLIS			= 10000;

	/**
	 * just clears the listening flag
	 * 
	 * @author helge
	 * 
	 */
	private class WatchDogRunnable implements Runnable {
		public void run() {
			mListening = false;
		}
	}

	/**
	 * clear listening flag aftet a time period
	 */
	private void setUpListeningWithWatchDog() {
		mListening = true;
		mListeningWatchDogHandler.removeCallbacks(mListeningWatchDogRunnable);
		mListeningWatchDogHandler.postDelayed(mListeningWatchDogRunnable, WATCH_DOG_MILLIS);
	}

	private Intent				mVoiceRecognitionIntent;

	private SiriClientActivity	callback;
	private SpeechRecognizer	mSpeechRecognizer;

	public VoiceRecognizerImpl(SiriClientActivity callback) {
		this.callback = callback;
		init();
	}

	/**
	 * 
	 */
	private void init() {
		NOTHING_RECOGNIZED = callback.getResources().getString(R.string.recognition_nothing_recognized);

		mVoiceRecognitionIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		mVoiceRecognitionIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, VoiceRecognizerImpl.class.getPackage().getName());
		mVoiceRecognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
		mVoiceRecognitionIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_RESULTS);
		// lang in *_* format
		MyPreferencesManager mgr = new MyPreferencesManager(callback);
		String lang = mgr.getStringPref(R.string.preferences_language_lang_key, R.string.preferences_language_lang_default).replace("-", "_");
		mVoiceRecognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, lang);

		mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(callback);
		mSpeechRecognizer.setRecognitionListener(mRecognizerLister);

		// Check to see if a recognition activity is present
		PackageManager pm = callback.getPackageManager();
		List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
		if (activities.size() != 0) {
			callback.getSpeakButtonController().activate();
		} else {
			callback.getSpeakButtonController().deactivate();
		}
	}

	public boolean close() {
		if (mSpeechRecognizer == null)
			return false;

		mSpeechRecognizer.destroy();
		mSpeechRecognizer = null;
		return true;
	}

	/**
	 * basic RecognitionListener implementation
	 * 
	 * @author helge
	 * 
	 */
	private class MyRecognitionListener implements RecognitionListener {

		public void onRmsChanged(float rmsdB) {
		}

		public void onResults(Bundle results) {
			ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

			float[] scoresArray = results.getFloatArray("confidence_scores");
			List<Float> scores = new ArrayList<Float>();

			if (scoresArray != null) {
				for (float f : scoresArray) {
					scores.add(new Float(f));
				}
			}

			Recognition r = new Recognition(matches, scores);

			String aceId = callback.startCorrectedSpeechRequest(r.getTopFilteredRecognition());
			mRecognitions.put(aceId, r);

			callback.getAddViewsController().addRecognition(r);
		}

		public void onReadyForSpeech(Bundle params) {
			callback.getSpeakButtonController().setSpeechInUse();
			// Toast.makeText(SiriClientActivity.this,
			// "speak now",
			// Toast.LENGTH_SHORT).show();
		}

		public void onPartialResults(Bundle partialResults) {
		}

		public void onEvent(int eventType, Bundle params) {
		}

		public void onError(int error) {
			callback.getSpeakButtonController().setSpeechDone();
			mListening = false;
			Toast.makeText(callback, NOTHING_RECOGNIZED, Toast.LENGTH_SHORT).show();
		}

		public void onEndOfSpeech() {
			callback.getSpeakButtonController().setSpeechDone();
			mListening = false;
			// Toast.makeText(SiriClientActivity.this,
			// "fnished",
			// Toast.LENGTH_SHORT).show();
		}

		public void onBufferReceived(byte[] buffer) {
		}

		public void onBeginningOfSpeech() {
			setUpListeningWithWatchDog();
		}

	}

	/**
	 * recognition listener
	 */
	private RecognitionListener	mRecognizerLister				= new MyRecognitionListener();

	/**
	 * handler to initiate voice recognition
	 */
	private Handler				mStartVoiceRecognitionHandler	= new Handler() {
																	@Override
																	public void handleMessage(Message msg) {
																		listenNow();
																	}
																};

	/**
	 * start recognition with handler
	 */
	public void startVoiceRecognition() {
		mStartVoiceRecognitionHandler.obtainMessage().sendToTarget();
	}

	/**
	 * 
	 */
	private void listenNow() {
		if (callback.getSpeaker() == null) {
			MyLog.i(getClass().getSimpleName(), "startVoiceRecognition(): speaker not initialized");
			Toast.makeText(callback, callback.getResources().getString(R.string.ui_tts_not_available), Toast.LENGTH_LONG);
		}

		if (mListening == true) {
			MyLog.i(getClass().getSimpleName(), "startVoiceRecognition(): already listening");
			return;
		}

		if (callback.getSpeaker() != null && callback.getSpeaker().isSpeaking()) {
			callback.getSpeaker().setListenAfterSpeaking();
			Log.i(getClass().getSimpleName(), "isSpeaking()=true, waiting");
			return;
		}

		if (mSpeechRecognizer == null) {
			init();
		}
		mSpeechRecognizer.startListening(mVoiceRecognitionIntent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.userinteraction.VoiceRecognizer#
	 * cancelVoiceRecognition()
	 */
	public boolean cancelVoiceRecognition() {
		mSpeechRecognizer.cancel();
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.userinteraction.VoiceRecognizer#
	 * getRecogitionForAceId(java.lang.String)
	 */
	public Recognition getRecogitionForAceId(String aceId) {
		if (mRecognitions.containsKey(aceId))
			return mRecognitions.get(aceId);

		return null;
	}

	public void run() {
		// TODO Auto-generated method stub

	}

}
