package com.wow.sirious.userinteraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.wow.sirious.MyLog;
import com.wow.sirious.activities.SiriClientActivity;

import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;

public class SpeakerImpl implements Speaker {

	private TextToSpeech								mTts;
	volatile private List<String>						mQueuedIds				= new ArrayList<String>();
	private Map<String, OnUtteranceCompletedListener>	mListeners				= new HashMap<String, TextToSpeech.OnUtteranceCompletedListener>();
	private boolean										mListenAfterSpeaking	= false;
	private SiriClientActivity							callback;

	public SpeakerImpl(TextToSpeech mTts, SiriClientActivity callback) {
		this.mTts = mTts;
		this.callback = callback;
		mTts.setOnUtteranceCompletedListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siri.siriclient.userinteraction.Speaker#speakText(java.lang.
	 * String)
	 */
	public boolean speakText(String text) {
		return speakText(text, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siri.siriclient.userinteraction.Speaker#speakText(java.lang.
	 * String, android.speech.tts.TextToSpeech.OnUtteranceCompletedListener)
	 */
	public boolean speakText(String text, OnUtteranceCompletedListener listener) {
		if (text == null)
			return false;
		HashMap<String, String> m = new HashMap<String, String>();
		String utteranceId = UUID.randomUUID().toString();
		m.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
		int success = mTts.speak(text, TextToSpeech.QUEUE_ADD, m);
		// Log.i("SpeakerImpl", "utteranceId=" + utteranceId + " queued");
		mQueuedIds.add(utteranceId);

		// add listener
		if (listener != null) {
			mListeners.put(utteranceId, listener);
		}

		return success == TextToSpeech.SUCCESS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.userinteraction.Speaker#cancelSpeaking()
	 */
	public void cancelSpeaking() {
		mTts.stop();
		mQueuedIds.clear();
		notifyAllDone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.userinteraction.Speaker#isSpeaking()
	 */
	public boolean isSpeaking() {
		if (mTts != null) {
			return mTts.isSpeaking() || mQueuedIds.size() > 0;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siri.siriclient.userinteraction.Speaker#setListenAfterSpeaking()
	 */
	public void setListenAfterSpeaking() {
		mListenAfterSpeaking = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.speech.tts.TextToSpeech.OnUtteranceCompletedListener#
	 * onUtteranceCompleted(java.lang.String)
	 */
	public void onUtteranceCompleted(String utteranceId) {
		mQueuedIds.remove(utteranceId);
		MyLog.i("SpeakerImpl", "utteranceId=" + utteranceId + " done, queuesize=" + mQueuedIds.size() + " listenAfter=" + mListenAfterSpeaking);

		if (mListeners.containsKey(utteranceId)) {
			notifyDone(utteranceId);
		}

		if (mListenAfterSpeaking && mQueuedIds.size() == 0) {
			// Log.i("SpeakerImpl", "now listen");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mListenAfterSpeaking = false;
			callback.getVoiceRecognizer().startVoiceRecognition();
		}
	}

	/**
	 * @param utteranceId
	 */
	private void notifyDone(String utteranceId) {
		if (!mListeners.containsKey(utteranceId))
			return;

		OnUtteranceCompletedListener l = mListeners.get(utteranceId);
		l.onUtteranceCompleted(utteranceId);
		mListeners.remove(utteranceId);
	}

	/**
	 * 
	 */
	private void notifyAllDone() {
		Set<String> keys = mListeners.keySet();
		for (String id : keys) {
			notifyDone(id);
		}
	}
}
