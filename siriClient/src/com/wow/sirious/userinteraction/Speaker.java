package com.wow.sirious.userinteraction;

import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;

/**
 * generic interface for text to speech
 * 
 * @author helge
 * 
 */
public interface Speaker extends OnUtteranceCompletedListener {
	/**
	 * speak text
	 * 
	 * @param text
	 * @return <code>true</code> on success
	 */
	public boolean speakText(String text);

	/**
	 * @param text
	 * @param listener
	 * @return
	 */
	public boolean speakText(String text, OnUtteranceCompletedListener listener);

	/**
	 * abort current speaking (if)
	 */
	public void cancelSpeaking();

	/**
	 * check wehter speaking currently
	 * 
	 * @return <code>true</code> if so
	 */
	public boolean isSpeaking();

	/**
	 * trigger new voicerecognition after finishing current speech
	 */
	public void setListenAfterSpeaking();
}
