package com.wow.sirious.userinteraction;

public interface VoiceRecognizer {
	/**
	 * start new voice recognition
	 */
	public void startVoiceRecognition();

	/**
	 * cancel all current voice recognitions
	 * 
	 * @return <code>true</code> if there was one ongoing
	 */
	public boolean cancelVoiceRecognition();

	/**
	 * get the recognition für an ace-id
	 * 
	 * @param aceId
	 * @return the recognition object
	 */
	public Recognition getRecogitionForAceId(String aceId);

	/**
	 * destroys the recognizer
	 * 
	 * @return
	 */
	public boolean close();
}
