package com.wow.sirious.userinteraction;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * generic recognition object for a speech-search
 * 
 * @author helge
 * 
 */
public class Recognition {
	public final List<String>	recognitions;
	public final List<Float>	scores;
	public final String			aceId;

	/**
	 * @param recognitions
	 * @param scores
	 */
	public Recognition(List<String> recognitions, List<Float> scores) {
		super();
		this.recognitions = recognitions;
		this.scores = scores;
		this.aceId = UUID.randomUUID().toString();
	}

	/**
	 * @return the aceId
	 */
	public String getAceId() {
		return aceId;
	}

	/**
	 * @param string
	 * @return filtered string
	 */
	private String filterMatch(String string) {
		// string = string.replace("-", "");

		// capitalize
		String output = "";
		StringTokenizer tkn = new StringTokenizer(string, " ");
		boolean first = true;
		while (tkn.hasMoreTokens()) {
			String token = tkn.nextToken();
			if (first == true) {
				first = false;
			} else {
				output += " ";
			}
			output += token.substring(0, 1).toUpperCase() + token.substring(1);
		}
		return output;
	}

	/**
	 * @return filteres recognitions list
	 */
	public List<String> getFilteredRecognitions() {
		List<String> filtered = new ArrayList<String>();
		for (String s : recognitions) {
			filtered.add(filterMatch(s));
		}

		return filtered;
	}

	/**
	 * @return top filtered recognition
	 */
	public String getTopFilteredRecognition() {
		if (recognitions.size() > 0)
			return filterMatch(recognitions.get(0));
		else
			return null;
	}

	/**
	 * @return score of best match
	 */
	public float getTopScore() {
		if (scores.size() > 0)
			return scores.get(0);
		else
			return 0f;
	}

	/**
	 * @return the recognitions
	 */
	public List<String> getRawRecognitions() {
		return recognitions;
	}

	/**
	 * @return the scores
	 */
	public List<Float> getScores() {
		return scores;
	}

}
