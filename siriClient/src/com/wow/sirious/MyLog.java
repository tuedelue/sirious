package com.wow.sirious;

import android.util.Log;

public class MyLog {

	/**
	 * Log.WARN = 5 Log.INFO = 4 Log.DEBUG = 3 Log.VERBOSE = 2
	 */
	public static final int	DEBUG	= Log.WARN;

	public static void i(String tag, String msg) {
		if (DEBUG <= Log.INFO)
			Log.i(tag, msg);
	}

	public static void v(String tag, String msg) {
		if (DEBUG <= Log.VERBOSE)
			Log.v(tag, msg);
	}

	public static void w(String tag, String msg) {
		if (DEBUG <= Log.WARN)
			Log.w(tag, msg);
	}

	public static void wtf(String tag, String msg) {
		Log.wtf(tag, msg);
	}

	public static void d(String tag, String msg) {
		if (DEBUG <= Log.DEBUG)
			Log.wtf(tag, msg);
	}
}
