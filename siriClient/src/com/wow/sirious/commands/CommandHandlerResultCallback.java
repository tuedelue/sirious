package com.wow.sirious.commands;

import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.system.ResultCallback;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerResultCallback implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof ResultCallback))
			return false;

		ResultCallback obj = (ResultCallback) cmd;
		List<ClientBoundCommand> commands = obj.getCommands();
		for (ClientBoundCommand c : commands) {
			cmdHandler.doCommand(c);
		}

		return true;
	}

}
