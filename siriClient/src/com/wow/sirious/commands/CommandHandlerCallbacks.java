package com.wow.sirious.commands;

import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerCallbacks implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		List<ClientBoundCommand> callbacks = cmd.getCallbacks();
		if (callback == null)
			return false;

		for (ClientBoundCommand o : callbacks) {
			cmdHandler.doCommand((ClientBoundCommand) o);
		}
		return true;
	}

}
