package com.wow.sirious.commands;

import java.net.URLEncoder;

import android.content.Intent;
import android.net.Uri;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.websearch.WebSearch;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerWebSearch implements CommandHandler {
	public static final String	providerUri	= "https://www.google.com/search?q=";

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof WebSearch))
			return false;

		WebSearch obj = (WebSearch) cmd;

		String url = providerUri + URLEncoder.encode(obj.getQuery());
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		callback.startActivity(i);
		return true;
	}

}
