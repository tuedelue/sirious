package com.wow.sirious.commands;

import java.util.UUID;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.system.GetRequestOrigin;
import com.wow.siriobjects.system.SetRequestOrigin;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerGetRequestOrigin implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof GetRequestOrigin))
			return false;

		GetRequestOrigin obj = (GetRequestOrigin) cmd;
		// Log.i(this.getClass().getSimpleName(),
		// "GetRequestOrigin: desiredAcc=" + obj.getDesiredAccuracy() +
		// " timeout=" + obj.getSearchTimeout());
		long age = callback.getLocationProvider().getAge();
		double lat = callback.getLocationProvider().getmLatitute();
		double lon = callback.getLocationProvider().getmLongitude();
		double alt = callback.getLocationProvider().getmAltitude();
		float speed = callback.getLocationProvider().getmSpeed();
		float bearing = callback.getLocationProvider().getmBearing();
		long timestap = callback.getLocationProvider().getmLocationTimestamp();

		SetRequestOrigin next = new SetRequestOrigin(UUID.randomUUID().toString(), obj.getAceId(), timestap, SetRequestOrigin.getStatusValid(), speed, bearing, obj.getDesiredAccuracy(), alt, age,
				null, null, lon, lat);
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}
}
