package com.wow.sirious.commands;

import java.util.HashMap;
import java.util.Map;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.RequestCompleted;
import com.wow.siriobjects.contact.PersonSearch;
import com.wow.siriobjects.phone.Call;
import com.wow.siriobjects.speech.SpeechRecognized;
import com.wow.siriobjects.system.DomainObjectCancel;
import com.wow.siriobjects.system.DomainObjectCommit;
import com.wow.siriobjects.system.DomainObjectCreate;
import com.wow.siriobjects.system.DomainObjectRetrieve;
import com.wow.siriobjects.system.DomainObjectUpdate;
import com.wow.siriobjects.system.GetRequestOrigin;
import com.wow.siriobjects.system.ResultCallback;
import com.wow.siriobjects.system.SendCommands;
import com.wow.siriobjects.ui.AddViews;
import com.wow.siriobjects.weather.WeatherLocationAdd;
import com.wow.siriobjects.weather.WeatherLocationDelete;
import com.wow.siriobjects.weather.WeatherLocationSearch;
import com.wow.siriobjects.weather.WeatherShowWeatherLocations;
import com.wow.siriobjects.websearch.WebSearch;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

/**
 * MAIN class for command handling; registers all possible commands and manages
 * them
 * 
 * @author helge
 * 
 */
public class CommandControllerImpl implements CommandHandler, CommandController {
	private static final String			CALLBACKS_IDENTIFIER	= "callbacks";
	private OnSiriResponseListener		responseListener		= null;
	private SiriSession					session					= null;
	private Map<String, CommandHandler>	map						= null;
	private SiriClientActivity			callback				= null;

	public CommandControllerImpl(SiriClientActivity callback, SiriSession session, OnSiriResponseListener responselistener) {
		this.session = session;
		this.responseListener = responselistener;
		this.callback = callback;

		map = new HashMap<String, CommandHandler>();
		map.put(DomainObjectRetrieve.class.getName(), new CommandHandlerDomainObject());
		map.put(DomainObjectCreate.class.getName(), new CommandHandlerDomainObject());
		map.put(DomainObjectUpdate.class.getName(), new CommandHandlerDomainObject());
		map.put(DomainObjectCommit.class.getName(), new CommandHandlerDomainObject());
		map.put(DomainObjectCancel.class.getName(), new CommandHandlerDomainObject());

		map.put(WeatherLocationDelete.class.getName(), new CommandHandlerWeather());
		map.put(WeatherLocationSearch.class.getName(), new CommandHandlerWeather());
		map.put(WeatherLocationAdd.class.getName(), new CommandHandlerWeather());
		map.put(WeatherShowWeatherLocations.class.getName(), new CommandHandlerWeather());

		map.put(AddViews.class.getName(), new CommandHandlerAddViews());
		map.put(Call.class.getName(), new CommandHandlerCall());
		map.put(CALLBACKS_IDENTIFIER, new CommandHandlerCallbacks());
		map.put(GetRequestOrigin.class.getName(), new CommandHandlerGetRequestOrigin());
		map.put(PersonSearch.class.getName(), new CommandHandlerPersonSearch());
		map.put(RequestCompleted.class.getName(), new CommandHandlerRequestCompleted());
		map.put(ResultCallback.class.getName(), new CommandHandlerResultCallback());
		map.put(SpeechRecognized.class.getName(), new CommandHandlerSpeechRecognized());
		map.put(SendCommands.class.getName(), new CommandHandlerSendCommands());
		map.put(WebSearch.class.getName(), new CommandHandlerWebSearch());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siri.siriclient.commands.CommandHandlerController#doCallbacks
	 * (de.helge.siri.siriobjects.ClientBoundCommand)
	 */
	public boolean doCallbacks(ClientBoundCommand cmd) {
		CommandHandler h = map.get(CALLBACKS_IDENTIFIER);
		return h.doCommand(this, session, callback, responseListener, cmd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siri.siriclient.commands.CommandHandlerController#doCommand(
	 * de.helge.siri.siriobjects.ClientBoundCommand)
	 */
	public boolean doCommand(ClientBoundCommand cmd) {
		return doCommand(this, session, callback, responseListener, cmd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siriclient.commands.CommandHandler#doCommand(de.helge.siriclient
	 * .commands.CommandHandlerImpl, de.helge.siriclient.comm.SiriSession,
	 * de.helge.siriclient.activities.SiriClientActivity,
	 * de.helge.siriclient.comm.OnSiriResponseListener,
	 * de.helge.siriclient.siriobjects.ClientBoundCommand)
	 */
	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		String className = cmd.getClass().getName();
		if (!map.containsKey(className))
			return false;

		CommandHandler h = map.get(className);
		return h.doCommand(this, session, callback, responseListener, cmd);
	}
}
