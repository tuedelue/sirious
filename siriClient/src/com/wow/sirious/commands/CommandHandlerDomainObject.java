package com.wow.sirious.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.telephony.SmsManager;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.sms.SmsSms;
import com.wow.siriobjects.system.DomainObject;
import com.wow.siriobjects.system.DomainObjectCancel;
import com.wow.siriobjects.system.DomainObjectCancelCompleted;
import com.wow.siriobjects.system.DomainObjectCommit;
import com.wow.siriobjects.system.DomainObjectCommitCompleted;
import com.wow.siriobjects.system.DomainObjectCreate;
import com.wow.siriobjects.system.DomainObjectCreateCompleted;
import com.wow.siriobjects.system.DomainObjectRetrieve;
import com.wow.siriobjects.system.DomainObjectRetrieveCompleted;
import com.wow.siriobjects.system.DomainObjectUpdate;
import com.wow.siriobjects.system.DomainObjectUpdateCompleted;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.data.MyDomainObjectLibrary;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerDomainObject implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, final SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (cmd == null)
			return false;

		if (cmd instanceof DomainObjectCommit) {
			return commit(callback, cmd);
		}
		if (cmd instanceof DomainObjectCancel) {
			return cancel(callback, cmd);
		}
		if (cmd instanceof DomainObjectCreate) {
			return create(callback, cmd);
		}
		if (cmd instanceof DomainObjectRetrieve) {
			return retrieve(callback, cmd);
		}
		if (cmd instanceof DomainObjectUpdate) {
			return update(callback, cmd);
		}
		return false;
	}

	private boolean commit(final SiriClientActivity callback, ClientBoundCommand cmd) {
		DomainObjectCommit obj = (DomainObjectCommit) cmd;
		String identifier = obj.getIdentifier().getIdentifier();

		DomainObject dobj = callback.getDomainObjectLibrary().getObject(identifier);
		if (dobj != null) {
			if (dobj instanceof SmsSms) {
				SmsSms sms = (SmsSms) dobj;

				SmsManager sm = SmsManager.getDefault();
				final ArrayList<String> messageList = sm.divideMessage(sms.getMessage());
				for (String number : sms.getRecipients()) {
					sm.sendMultipartTextMessage(number, null, messageList, null, null);
				}
			}
		}

		DomainObjectCommitCompleted next = new DomainObjectCommitCompleted(UUID.randomUUID().toString(), obj.getAceId(), identifier);
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}

	private boolean cancel(final SiriClientActivity callback, ClientBoundCommand cmd) {
		DomainObjectCancel obj = (DomainObjectCancel) cmd;
		String identifier = obj.getIdentifier().getIdentifier();

		callback.getDomainObjectLibrary().removeObject(identifier);

		DomainObjectCancelCompleted next = new DomainObjectCancelCompleted(UUID.randomUUID().toString(), obj.getAceId(), identifier);
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}

	private boolean create(final SiriClientActivity callback, ClientBoundCommand cmd) {
		DomainObjectCreate obj = (DomainObjectCreate) cmd;
		DomainObject domainObj = obj.getObject();
		String identifier = domainObj.getIdentifier();
		if (identifier == null) { // this will probably be the case...
			identifier = UUID.randomUUID().toString();
			domainObj.setIdentifier(identifier);
		}

		MyDomainObjectLibrary lib = callback.getDomainObjectLibrary();
		lib.addObject(domainObj);

		DomainObjectCreateCompleted next = new DomainObjectCreateCompleted(UUID.randomUUID().toString(), obj.getAceId(), identifier);
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}

	private boolean retrieve(final SiriClientActivity callback, ClientBoundCommand cmd) {
		DomainObjectRetrieve obj = (DomainObjectRetrieve) cmd;
		List<DomainObject> results = new ArrayList<DomainObject>();

		if (obj.getIdentifiers() != null) {
			List<DomainObject> ids = obj.getIdentifiers();

			for (DomainObject do_id : ids) {
				String currentId = do_id.getIdentifier();
				DomainObject do_obj = callback.getDomainObjectLibrary().getObject(currentId);
				if (do_obj != null)
					results.add(do_obj);
			}
		}

		DomainObjectRetrieveCompleted next = new DomainObjectRetrieveCompleted(UUID.randomUUID().toString(), obj.getAceId(), results);
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}

	private boolean update(final SiriClientActivity callback, ClientBoundCommand cmd) {
		DomainObjectUpdate obj = (DomainObjectUpdate) cmd;

		String identifier = obj.getIdentifier().getIdentifier();
		DomainObject add = obj.getAddFields();
		DomainObject remove = obj.getRemoveFields();
		DomainObject set = obj.getSetFields();

		MyDomainObjectLibrary lib = callback.getDomainObjectLibrary();
		lib.updateObject(identifier, add, remove, set);

		DomainObjectUpdateCompleted next = new DomainObjectUpdateCompleted(UUID.randomUUID().toString(), obj.getAceId(), obj.getIdentifier());
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}

}
