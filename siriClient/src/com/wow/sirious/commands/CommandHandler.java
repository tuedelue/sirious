package com.wow.sirious.commands;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

/**
 * generic command handler interface
 * 
 * @author helge
 * 
 */
public interface CommandHandler {
	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd);
}
