package com.wow.sirious.commands;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.RequestCompleted;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerRequestCompleted implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof RequestCompleted))
			return false;

		RequestCompleted obj = (RequestCompleted) cmd;

		cmdHandler.doCallbacks(cmd);
		return true;
	}

}
