package com.wow.sirious.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.contact.PersonSearch;
import com.wow.siriobjects.contact.PersonSearchCompleted;
import com.wow.siriobjects.system.Person;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.data.MyPhonebookProvider;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerPersonSearch implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof PersonSearch)) {
			return false;
		}
		PersonSearch obj = (PersonSearch) cmd;

		List<Person> results;
		if (obj.isMe()) {
			results = new ArrayList<Person>();
			results.add(new Person("id", null, null, null, null, null, null, true, null, null, "mr. boombastico", null, null, null, null, null, null));
		} else {
			results = MyPhonebookProvider.getAllMatchingPhoneContacts(callback, obj.getName());
		}

		PersonSearchCompleted next = new PersonSearchCompleted(UUID.randomUUID().toString(), obj.getAceId(), results);
		callback.sendCommandAndRegisterForAnswer(next);
		return true;
	}
}
