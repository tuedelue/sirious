package com.wow.sirious.commands;

import java.util.UUID;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.weather.WeatherLocationAdd;
import com.wow.siriobjects.weather.WeatherLocationAddCompleted;
import com.wow.siriobjects.weather.WeatherLocationDelete;
import com.wow.siriobjects.weather.WeatherLocationDeleteCompleted;
import com.wow.siriobjects.weather.WeatherLocationSearch;
import com.wow.siriobjects.weather.WeatherLocationSearchCompleted;
import com.wow.siriobjects.weather.WeatherShowWeatherLocations;
import com.wow.siriobjects.weather.WeatherShowWeatherLocationsCompleted;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerWeather implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {

		String newAceId = UUID.randomUUID().toString();
		if (cmd instanceof WeatherLocationAdd) {
			WeatherLocationAdd obj = (WeatherLocationAdd) cmd;
			// TODO
			String id = UUID.randomUUID().toString();
			if (obj.getWeatherLocation() != null && obj.getWeatherLocation().getLocationId() != null)
				id = obj.getWeatherLocation().getLocationId();
			WeatherLocationAddCompleted next = new WeatherLocationAddCompleted(newAceId, obj.getAceId(), null, id);
			callback.sendCommandAndRegisterForAnswer(next);
			return true;
		} else if (cmd instanceof WeatherLocationDelete) {
			WeatherLocationDelete obj = (WeatherLocationDelete) cmd;
			// TODO
			WeatherLocationDeleteCompleted next = new WeatherLocationDeleteCompleted(newAceId, obj.getAceId());
			callback.sendCommandAndRegisterForAnswer(next);
			return true;
		} else if (cmd instanceof WeatherLocationSearch) {
			WeatherLocationSearch obj = (WeatherLocationSearch) cmd;
			// TODO
			WeatherLocationSearchCompleted next = new WeatherLocationSearchCompleted(newAceId, obj.getAceId(), null);
			callback.sendCommandAndRegisterForAnswer(next);
			return true;
		} else if (cmd instanceof WeatherShowWeatherLocations) {
			// WeatherShowWeatherLocations obj = (WeatherShowWeatherLocations)
			// cmd;
			// TODO
			WeatherShowWeatherLocationsCompleted next = new WeatherShowWeatherLocationsCompleted();
			callback.sendCommandAndRegisterForAnswer(next);
			return true;
		}
		return false;
	}
}
