package com.wow.sirious.commands;

import com.wow.siriobjects.ClientBoundCommand;

public interface CommandController {

	/**
	 * triggers the {@link CommandHandlerCallbacks} handler
	 * 
	 * @param cmd
	 * @return
	 */
	public abstract boolean doCallbacks(ClientBoundCommand cmd);

	/**
	 * @param cmd
	 * @return
	 */
	public abstract boolean doCommand(ClientBoundCommand cmd);

}
