package com.wow.sirious.commands;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.phone.Call;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerCall implements CommandHandler {
	public boolean doCommand(CommandController cmdHandler, SiriSession session, final SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof Call))
			return false;

		Call obj = (Call) cmd;
		final String number = "tel:" + obj.getRecipient();

		new Handler().postDelayed(new Runnable() {
			public void run() {
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse(number));
				callback.startActivity(callIntent);
			}
		}, 5000);
		return true;
	}

}
