package com.wow.sirious.commands;

import java.util.UUID;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.ServerBoundCommand;
import com.wow.siriobjects.system.SendCommands;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerSendCommands implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof SendCommands))
			return false;

		SendCommands obj = (SendCommands) cmd;
		for (ServerBoundCommand s : obj.getCommands()) {
			s.setAceId(UUID.randomUUID().toString());
			s.setRefId(obj.getAceId());
			callback.sendCommandAndRegisterForAnswer(s);
		}
		return true;
	}
}
