package com.wow.sirious.commands;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.ui.AddViews;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerAddViews implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof AddViews))
			return false;

		AddViews obj = (AddViews) cmd;
		callback.getAddViewsController().createView(obj);

		cmdHandler.doCallbacks(cmd);
		return true;
	}

}
