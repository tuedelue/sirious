package com.wow.sirious.commands;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.speech.SpeechRecognized;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.SiriSession;

public class CommandHandlerSpeechRecognized implements CommandHandler {

	public boolean doCommand(CommandController cmdHandler, SiriSession session, SiriClientActivity callback, OnSiriResponseListener responseListener, ClientBoundCommand cmd) {
		if (!(cmd instanceof SpeechRecognized))
			return false;
		/*
		 * SpeechRecognized obj = (SpeechRecognized) cmd; String recognition =
		 * obj.getRecognition().getPhrases().iterator().next().
		 * getInterpretations
		 * ().iterator().next().getTokens().iterator().next().getText();
		 * addText(false, recognition);
		 */
		return true;
	}

}
