package com.wow.sirious.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.text.InputType;

import com.wow.sirious.R;
import com.wow.sirious.data.MyPreferencesManager;

public class PreferencesActivity extends PreferenceActivity {

	private MyPreferencesManager	mgr;
	private Preference				server_host;
	private Preference				server_port;
	private Preference				langPref;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.preference.PreferenceActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mgr = new MyPreferencesManager(this);

		// standard
		addPreferencesFromResource(R.xml.preferences);

		OnPreferenceChangeListener listener = new OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				String key = preference.getKey();
				if (key.equals(getResources().getString(R.string.preferences_server_host_key)) || key.equals(getResources().getString(R.string.preferences_server_port_key))) {
					findPreference(key).setSummary(newValue.toString());
					return true;
				}
				return false;
			}
		};

		// predefined
		Preference server = findPreference(getResources().getString(R.string.preferences_server_predefined_key));
		Intent i = new Intent(this, PreferencesSelectServer.class);
		server.setIntent(i);

		// host
		server_host = findPreference(getResources().getString(R.string.preferences_server_host_key));
		server_host.setOnPreferenceChangeListener(listener);
		((EditTextPreference) server_host).getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_URI);

		// port
		server_port = findPreference(getResources().getString(R.string.preferences_server_port_key));
		server_port.setOnPreferenceChangeListener(listener);
		((EditTextPreference) server_port).getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);

		// language
		langPref = findPreference(getResources().getString(R.string.preferences_language_lang_key));
		Intent i2 = new Intent(this, PreferencesSelectLanguage.class);
		langPref.setIntent(i2);

		// privacy
		Preference privacy = findPreference(getResources().getString(R.string.preferences_privacy_confirmed_key));
		Intent i3 = new Intent("android.intent.action.VIEW", Uri.parse(getResources().getString(R.string.privacy_url)));
		privacy.setIntent(i3);

		// website
		Preference website = findPreference(getResources().getString(R.string.preferences_sirious_website_key));
		Intent i4 = new Intent("android.intent.action.VIEW", Uri.parse(getResources().getString(R.string.website)));
		website.setIntent(i4);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		server_host.setSummary(mgr.getStringPref(R.string.preferences_server_host_key, R.string.preferences_server_host_default));
		server_port.setSummary(mgr.getStringPref(R.string.preferences_server_port_key, R.string.preferences_server_port_default));
		langPref.setSummary(mgr.getStringPref(R.string.preferences_language_lang_key, R.string.preferences_language_lang_default));
	}

}
