package com.wow.sirious.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.wow.sirious.R;
import com.wow.sirious.data.MyPreferencesManager;
import com.wow.sirious.data.SupportedLanguages;
import com.wow.sirious.data.SupportedLanguages.OnSupportedLanguagesResultListener;

public class PreferencesSelectLanguage extends ListActivity {

	private SupportedLanguages					mSupportedLanguages	= null;
	private OnSupportedLanguagesResultListener	mLangListener		= null;
	private List<String>						mLanguages			= new ArrayList<String>();
	private ProgressDialog						mProgressDialog		= null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(R.string.preferences_language_title);

		// setup languages
		mSupportedLanguages = new SupportedLanguages();
		mSupportedLanguages.refreshVoiceSettings(this);

		mLangListener = new SupportedLanguages.OnSupportedLanguagesResultListener() {
			public void handleLanguagesResult(List<String> languages) {
				mProgressDialog.dismiss();
				mLanguages = languages;

				// setup arrayadapter
				String[] values = mLanguages.toArray(new String[mLanguages.size()]);
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(PreferencesSelectLanguage.this, android.R.layout.simple_list_item_1, values);
				setListAdapter(adapter);
			}
		};

		mSupportedLanguages.addListener(mLangListener);

		// setup progress dialog
		String message = getResources().getString(R.string.loading);
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setTitle(R.string.preferences_language_title);
		mProgressDialog.setMessage(message);

		mProgressDialog.setOnCancelListener(new OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				PreferencesSelectLanguage.this.finish();
			}
		});

		// show dialog
		mProgressDialog.show();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String item = (String) getListAdapter().getItem(position);

		MyPreferencesManager mgr = new MyPreferencesManager(this);
		mgr.setStringPref(R.string.preferences_language_lang_key, item);
		mgr.setStringPref(R.string.session_assistant_id_key, "");

		finish();
	}
}
