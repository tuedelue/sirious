package com.wow.sirious.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.wow.sirious.MyLog;
import com.wow.sirious.R;
import com.wow.sirious.data.MyPreferencesManager;

public class PreferencesSelectServer extends ListActivity {

	public class MyServer {
		private String	host;
		private int		port;
		private String	title;
		private String	summary;

		public MyServer(String host, int port, String title, String summary) {
			this.host = host;
			this.port = port;
			this.title = title;
			this.summary = summary;
		}

		/**
		 * @return the host
		 */
		public String getHost() {
			return host;
		}

		/**
		 * @return the port
		 */
		public int getPort() {
			return port;
		}

		/**
		 * @return the title
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * @return the summary
		 */
		public String getSummary() {
			return summary;
		}
	}

	public class MyServerAdapter extends ArrayAdapter<MyServer> {

		private Context			context	= null;
		private List<MyServer>	objects	= new ArrayList<PreferencesSelectServer.MyServer>();

		public MyServerAdapter(Context context, List<MyServer> objects) {
			super(context, R.layout.ui_preferences_select_server_item, objects);
			this.objects = objects;
			this.context = context;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View,
		 * android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			if (rowView == null) {
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = inflater.inflate(R.layout.ui_preferences_select_server_item, null);
				TextView summary = (TextView) rowView.findViewById(R.id.ui_preferences_select_server_summary);
				TextView title = (TextView) rowView.findViewById(R.id.ui_preferences_select_server_title);

				final String titleStr = objects.get(position).getTitle();
				summary.setText(objects.get(position).getSummary());
				title.setText(titleStr);

				// onclicklistener
				final String host = objects.get(position).getHost();
				final String port = Integer.toString(objects.get(position).getPort());

				rowView.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						MyLog.i(getClass().getSimpleName(), "onClick " + host + ":" + port);
						MyPreferencesManager mgr = new MyPreferencesManager(context);
						mgr.setStringPref(R.string.preferences_server_host_key, host);
						mgr.setStringPref(R.string.preferences_server_port_key, port);
						Toast.makeText(context, titleStr + " selected", Toast.LENGTH_SHORT).show();

						doneSelection();
					}
				});
			}

			return rowView;
		}
	}

	private static final String	SERVER_LIST_URL	= "http://siriclient.com/servers.php";

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(R.string.preferences_server_predefined_title);

		// Use your own layout
		List<MyServer> list = getServers();

		ArrayAdapter<MyServer> adapter = new MyServerAdapter(this, list);
		setListAdapter(adapter);

		setContentView(R.layout.ui_preferences_select_server_list);
	}

	private void doneSelection() {
		finish();
	}

	public void cancelClicked(View v) {
		doneSelection();
	}

	private List<MyServer> getServers() {
		List<MyServer> list = new ArrayList<PreferencesSelectServer.MyServer>();
		try {
			String str = getJSONStr();
			if (str == null) {
				return list;
			}

			JSONObject json = new JSONObject(str);
			JSONArray arr = json.getJSONArray("servers");

			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i);
				if (obj != null) {
					String name = obj.getString("name");
					String description = obj.getString("description");
					String host = obj.getString("host");
					int port = obj.getInt("port");

					MyServer s = new MyServer(host, port, name, description);
					list.add(s);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return list;
	}

	private String getJSONStr() {
		String str = null;
		try {
			HttpClient hc = new DefaultHttpClient();
			HttpGet post = new HttpGet(SERVER_LIST_URL);

			HttpResponse rp = hc.execute(post);

			if (rp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				str = EntityUtils.toString(rp.getEntity());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return str;
	}
}
