//TODO listener or konkreten speech abwarten bevor activity gestartet wird bspw map
//TODO cancel/done/request completed
//TODO move all data do datamodel - past requests and so on
//TODO check for TTS & google voice search
//TODO SESSION management
//TODO LOGGING
package com.wow.sirious.activities;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.debug.hv.ViewServer;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.dd.plist.PropertyListException;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListSerializable;
import com.wow.siriobjects.speech.StartCorrectedSpeechRequest;
import com.wow.siriobjects.system.CommandFailed;
import com.wow.sirious.MyLog;
import com.wow.sirious.R;
import com.wow.sirious.commands.CommandController;
import com.wow.sirious.commands.CommandControllerImpl;
import com.wow.sirious.data.MyDomainObjectLibrary;
import com.wow.sirious.data.MyLocationProvider;
import com.wow.sirious.data.MyPreferencesManager;
import com.wow.sirious.net.OnSiriResponseListener;
import com.wow.sirious.net.OnSiriSessionHandler;
import com.wow.sirious.net.SiriRequest;
import com.wow.sirious.net.SiriResponse;
import com.wow.sirious.net.SiriSession;
import com.wow.sirious.net.SiriSessionImpl;
import com.wow.sirious.ui.AddViewsController;
import com.wow.sirious.ui.AddViewsControllerImpl;
import com.wow.sirious.ui.SpeakButtonControlImpl;
import com.wow.sirious.ui.SpeakButtonController;
import com.wow.sirious.ui.views.ConfirmationOptionsView;
import com.wow.sirious.userinteraction.Speaker;
import com.wow.sirious.userinteraction.SpeakerImpl;
import com.wow.sirious.userinteraction.VoiceRecognizer;
import com.wow.sirious.userinteraction.VoiceRecognizerImpl;

public class SiriClientActivity extends Activity {
	private static final byte[]	SALT				= { -1, -2, -3, -4, -5, -6, -7, -8, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	private String				BASE64_PUBLIC_KEY	= "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk87ejhGtretOaRI2+OtYWHV+qxWxbiHEGAlkZKoZ2TaNHzM1gVDWEpUKRdd5Nt+Voi10Wjh+zEnBGWuGtrgD7ujZYB6K/0CzNpVisa8wQtd64h+yLePd+ejWDl4/Nsp6mIBu/Q4KBqlmn2HbOUIGnHds9cYYB+cNiGB3HayRlM3TL0PKnZ/zmeTEWMLqo8JjoBnjhy5VfxUfjQabEePXThmpJv9/iO/lLDnDuF651/fbZYMBXvuKIBsK0thWFDCmRoZETk78IupyusmxXwtspwKz8k+Zh1y6G0iR7JkZXPoTGA96iBceE0ewiACiH1Olx7++n2gdhWQxXS3mGHLyDQIDAQAB";

	private class MyLicenseCheckerCallback implements LicenseCheckerCallback {
		public void allow(int policyReason) {
			if (isFinishing()) {
				// Don't update UI if Activity is finishing.
				return;
			}
			// Should allow user access.
			// displayResult("licensed");
		}

		public void dontAllow(int policyReason) {
			if (isFinishing()) {
				// Don't update UI if Activity is finishing.
				return;
			}
			// displayResult("not licensed");
			// Should not allow access. In most cases, the app should assume
			// the user has access unless it encounters this. If it does,
			// the app should inform the user of their unlicensed ways
			// and then either shut down the app or limit the user to a
			// restricted set of features.
			// In this example, we show a dialog that takes the user to Market.
			// If the reason for the lack of license is that the service is
			// unavailable or there is another problem, we display a
			// retry button on the dialog and a different message.
			displayDialog(policyReason == Policy.RETRY);
		}

		public void applicationError(int errorCode) {
			if (isFinishing()) {
				// Don't update UI if Activity is finishing.
				return;
			}
			// This is a polite way of saying the developer made a mistake
			// while setting up or calling the license checker library.
			// Please examine the error code and fix the error.
			// String result = String.format("error: %1$s", errorCode);
			// displayResult(result);
		}
	}

	protected Dialog onCreateDialog(int id) {
		final boolean bRetry = id == 1;
		return new AlertDialog.Builder(this)
				.setTitle(getString(R.string.license_no_license_dialog_title))
				.setMessage(bRetry ? getString(R.string.license_no_license_dialog_body_retry) : getString(R.string.license_no_license_dialog_body))
				.setPositiveButton(bRetry ? getString(R.string.license_no_license_dialog_button_retry) : getString(R.string.license_no_license_dialog_button_buy),
						new DialogInterface.OnClickListener() {
							boolean	mRetry	= bRetry;

							public void onClick(DialogInterface dialog, int which) {
								if (mRetry) {
									doCheck();
								} else {
									Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://market.android.com/details?id=" + getPackageName()));
									startActivity(marketIntent);
								}
							}
						}).setNegativeButton(getString(R.string.license_no_license_dialog_button_quit), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).create();
	}

	private void doCheck() {
		mChecker.checkAccess(mLicenseCheckerCallback);
	}

	private void displayResult(final String result) {
		mSiriClientActivityHandler.post(new Runnable() {
			public void run() {
				Toast.makeText(SiriClientActivity.this, result, Toast.LENGTH_LONG).show();
			}
		});
	}

	private void displayDialog(final boolean showRetry) {
		mSiriClientActivityHandler.post(new Runnable() {
			public void run() {
				// setProgressBarIndeterminateVisibility(false);
				showDialog(showRetry ? 1 : 0);
				// mCheckLicenseButton.setEnabled(true);
			}
		});
	}

	private LicenseChecker						mChecker							= null;
	private LicenseCheckerCallback				mLicenseCheckerCallback				= null;

	private boolean								confirmation_dialog_shown			= false;

	private GoogleAnalyticsTracker				tracker;

	public static final String					TRACKING_UI_CATEGORY				= "ui";
	public static final String					TRACKING_UI_ACTION_DEFAULT			= "default";
	public static final String					TRACKING_UI_ACTION_NOT_UNDERSTOOD	= "not understood";
	public static final String					TRACKING_SESSION_CATEGORY			= "session";
	public static final String					TRACKING_SESSION_ACTION_CONNECT		= "connect";
	public static final String					TRACKING_SESSION_ACTION_ESTABLISHED	= "established";
	public static final String					TRACKING_SESSION_ACTION_CLOSED		= "closed";
	public static final String					TRACKING_SESSION_ACTION_ERROR		= "error";

	private MyPreferencesManager				mPreferenceManager					= null;
	private Speaker								mSpeaker							= null;
	private MyLocationProvider					mLocationProvider					= null;
	private VoiceRecognizer						mVoiceRecognizer					= null;
	private TextToSpeech						mTts								= null;
	private AddViewsController					mAddViewsController					= null;
	private MyDomainObjectLibrary				mDomainObjectLibrary				= null;

	private OnSharedPreferenceChangeListener	mPrefChangeListener					= null;
	/**
	 * siri session
	 */
	private SiriSessionImpl						mSiriSession						= null;

	private static final int					TEXT_TO_SPEECH_REQUEST_CODE			= 5678;

	private static final String					GOOGLE_VOICE_PACKAGE				= "com.google.android.voicesearch";

	private Handler								mSiriClientActivityHandler			= new Handler();

	/**
	 * @author helge
	 * 
	 */
	private class MyOnSiriResponseListener implements OnSiriResponseListener {
		public void handleResponse(SiriRequest req, SiriResponse res) {
			boolean handled = false;

			String cmdStr = "unknown";
			try {
				cmdStr = res.getPlist().objectForKey("group").toString() + "/" + res.getPlist().objectForKey("class").toString();
			} catch (Exception e) {
				// nothing
			}

			if (res.getObject() instanceof ClientBoundCommand) {
				ClientBoundCommand c = (ClientBoundCommand) res.getObject();
				handled = handleCommand(c);

				tracker.trackPageView("/handledCommand/" + cmdStr);
				tracker.dispatch();
			}
			if (!handled) {
				Log.i(getClass().getSimpleName(), "unhandled response: " + res.toString());
				unhandledTracking(res.getPlist());
				Toast.makeText(SiriClientActivity.this, getResources().getString(R.string.ui_response_not_understood), Toast.LENGTH_LONG).show();

				tracker.trackPageView("/unHandledCommand/" + cmdStr);
				tracker.dispatch();
			}
		}
	}

	/**
	 * 
	 */
	private OnSiriResponseListener	mSiriResponseHandler		= new MyOnSiriResponseListener();

	/**
	 * 
	 */
	private CommandController		mCommandHandler				= null;

	/**
	 * 
	 */
	private SpeakButtonController	mSpeakButtonController;
	private OnSiriSessionHandler	mSessionHandler;
	private boolean					TTS_SERVICE_REQUEST_PENDING	= false;

	/**
	 * @return <code>true</code> if handled
	 */
	public boolean handleCommand(ClientBoundCommand cmd) {
		boolean handled = mCommandHandler.doCommand(cmd);
		if (handled == true)
			return true;

		sendCommandFailed(cmd);
		return false;
	}

	/**
	 * send a command failed object
	 * 
	 * @param cmd
	 */
	private void sendCommandFailed(ClientBoundCommand cmd) {
		unhandledTracking(cmd.toPlist());

		Log.i(getClass().getSimpleName(), "unhandled command: " + cmd);
		CommandFailed cmdFailed = new CommandFailed(UUID.randomUUID().toString(), cmd.getAceId(), "unimplemented", -1);

		SiriRequest req;
		try {
			req = mSiriSession.getmRequestFactory().create(cmdFailed);
			req.addOnSiriResponseListener(mSiriResponseHandler);
			mSiriSession.addToSendQueue(req);
		} catch (PropertyListException e) {
			e.printStackTrace();
			Log.w(getClass().getSimpleName(), e.getMessage());
		}
	}

	/**
	 * @param cmd
	 */
	private void unhandledTracking(NSDictionary plist) {
		String descriptor = "unknown";
		try {
			String clazz = ((NSString) plist.objectForKey("class")).toString();
			String group = ((NSString) plist.objectForKey("group")).toString();
			descriptor = group + ":" + clazz;
		} catch (Exception e) {
		} finally {
			tracker.trackEvent(TRACKING_UI_CATEGORY, TRACKING_UI_ACTION_NOT_UNDERSTOOD, descriptor, 0);
			tracker.dispatch();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (tracker != null)
			tracker.stopSession();

		MyLog.i(getClass().getSimpleName(), "onDestroy");

		if (mTts != null)
			mTts.shutdown();
		ViewServer.get(this).removeWindow(this);

		if (mChecker != null)
			mChecker.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		MyLog.i(getClass().getSimpleName(), "onResume");
		// ViewServer.get(this).setFocusedWindow(this);

		initLicensing();
		doCheck();

		// check privacy & then init all
		if (!checkPrivacyConfirmed())
			return;

		// init the session AFTER SETTING UP TTS TODO
		if (!TTS_SERVICE_REQUEST_PENDING) {
			initSession();
		} else {
			MyLog.i(getClass().getSimpleName(), "TTS REQUEST pending");
		}

		mSpeakButtonController.activate();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onMenuOpened(int, android.view.Menu)
	 */
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		MenuItem disconnect = menu.findItem(R.id.menu_main_disconnect);
		MenuItem connect = menu.findItem(R.id.menu_main_connect);

		if (mSiriSession != null && mSiriSession.isClosed() == false) {
			disconnect.setVisible(true);
			connect.setVisible(false);
		} else {
			disconnect.setVisible(false);
			connect.setVisible(true);
		}

		return super.onMenuOpened(featureId, menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Intent i;
		switch (item.getItemId()) {
			case R.id.menu_main_preferences:
				i = new Intent(this, PreferencesActivity.class);
				startActivity(i);
				return true;
			case R.id.menu_main_connect:
				initSession();
				return true;
			case R.id.menu_main_disconnect:
				mSiriSession.close();
				return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	/**
	 * Called with the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MyLog.i(getClass().getSimpleName(), "onCreate");

		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);

		initiAll();
	}

	private void initLicensing() {
		// Construct the LicenseCheckerCallback. The library calls this when
		// done.
		if (mLicenseCheckerCallback == null)
			mLicenseCheckerCallback = new MyLicenseCheckerCallback();

		// Construct the LicenseChecker with a Policy.
		String deviceId = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
		if (mChecker == null)
			mChecker = new LicenseChecker(this, new ServerManagedPolicy(this, new AESObfuscator(SALT, getPackageName(), deviceId)), BASE64_PUBLIC_KEY);
	}

	/**
	 * setup preference manager & check privacy confirmed status; inits all when
	 * everything is ok.
	 */
	private boolean checkPrivacyConfirmed() {
		// get confirmation status
		String confirmedStr = mPreferenceManager.getStringPref(R.string.preferences_privacy_confirmed_key, R.string.preferences_privacy_confirmed_default);
		boolean confirmed = Boolean.parseBoolean(confirmedStr);

		// if it is already confirmed, init all
		if (confirmed) {
			return true;
		}

		if (confirmation_dialog_shown)
			return confirmed;

		confirmation_dialog_shown = true;

		// show alert dialog
		AlertDialog.Builder quitDialog = new AlertDialog.Builder(this);
		quitDialog.setTitle(R.string.privacy_dialog_title);
		quitDialog.setCancelable(false);

		// with a webview
		WebView v = new WebView(this);
		v.loadUrl(getResources().getString(R.string.privacy_url));
		ScrollView sv = new ScrollView(this);
		sv.addView(v);

		quitDialog.setView(sv);

		// everything ok, set privacy confrimed and init.
		quitDialog.setPositiveButton(R.string.privacy_dialog_accept, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				mPreferenceManager.setStringPref(R.string.preferences_privacy_confirmed_key, "true");
				onResume();
			}
		});

		// not confirmed, quit.
		quitDialog.setNegativeButton(R.string.privacy_dialog_do_not_accept, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Toast.makeText(SiriClientActivity.this, getResources().getString(R.string.privacy_not_confirmed), Toast.LENGTH_LONG).show();
				finish();
			}
		});

		quitDialog.show();
		return false;
	}

	/**
	 * 
	 */
	public void changeLanguage() {
		mVoiceRecognizer = new VoiceRecognizerImpl(this);

		// text to speech!!!!!!!!!
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		TTS_SERVICE_REQUEST_PENDING = true;
		startActivityForResult(checkIntent, TEXT_TO_SPEECH_REQUEST_CODE);
	}

	/**
	 * 
	 */
	private void initiAll() {
		// getWindow().getDecorView().getBackground().setDither(true);
		// getWindow().setFormat(PixelFormat.RGBA_8888);

		// check for google voice search
		Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		List<ResolveInfo> list = getPackageManager().queryIntentActivities(i, PackageManager.MATCH_DEFAULT_ONLY);
		if (list.size() == 0) {
			Uri uri = Uri.parse("market://details?id=" + GOOGLE_VOICE_PACKAGE);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);
		}

		// Inflate our UI from its XML layout description.
		setContentView(R.layout.voice_recognition);

		mPreferenceManager = new MyPreferencesManager(this);

		tracker = GoogleAnalyticsTracker.getInstance();
		// Start the tracker in manual dispatch mode...
		tracker.startNewSession("UA-30625682-2", this);

		tracker.trackEvent(TRACKING_UI_CATEGORY, TRACKING_UI_ACTION_DEFAULT, null, 0);
		tracker.dispatch();

		mSessionHandler = new OnSiriSessionHandler() {
			public void handleClosed(final SiriSession s) {
				mSiriClientActivityHandler.post(new Runnable() {
					public void run() {
						tracker.trackEvent(TRACKING_SESSION_CATEGORY, TRACKING_SESSION_ACTION_ESTABLISHED, s.getServerAddress(), 0);
						tracker.dispatch();

						mSpeakButtonController.deactivate();
						mCommandHandler = null;
						MyLog.i(SiriSessionImpl.class.getSimpleName(), "session closed");

						String closedStr = getResources().getString(R.string.session_closed);
						Toast.makeText(SiriClientActivity.this, closedStr + " (" + s.getClosedReason() + ")", Toast.LENGTH_LONG).show();
					}
				});
			}

			public void handleEstablished(final SiriSession s) {
				mSiriClientActivityHandler.post(new Runnable() {
					public void run() {
						tracker.trackEvent(TRACKING_SESSION_CATEGORY, TRACKING_SESSION_ACTION_CLOSED, s.getServerAddress(), 0);
						tracker.dispatch();

						mSpeakButtonController.activate();
						mCommandHandler = new CommandControllerImpl(SiriClientActivity.this, mSiriSession, mSiriResponseHandler);
						MyLog.i(SiriSessionImpl.class.getSimpleName(), "session established");
					}
				});
			}
		};

		// listen for lang changes
		mPrefChangeListener = new OnSharedPreferenceChangeListener() {
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
				if (key.equals(getResources().getString(R.string.preferences_language_lang_key))) {
					changeLanguage();
					MyLog.i(getClass().getSimpleName(), "lang changed");
				}
			}
		};
		PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(mPrefChangeListener);

		mLocationProvider = new MyLocationProvider(this);

		// ViewServer.get(this).addWindow(this);

		mDomainObjectLibrary = new MyDomainObjectLibrary();

		mSpeakButtonController = new SpeakButtonControlImpl(this);
		mSpeakButtonController.deactivate();

		ViewGroup grp = (ViewGroup) findViewById(R.id.siriPlayground);
		mAddViewsController = new AddViewsControllerImpl(grp, this);

		// setup language specific settings
		changeLanguage();
	}

	/**
	 * initialise a session
	 * 
	 * @param host
	 * @param port
	 */
	private void initSession() {
		String host = mPreferenceManager.getStringPref(R.string.preferences_server_host_key, R.string.preferences_server_host_default);
		int port = 444;
		try {
			port = Integer.parseInt(mPreferenceManager.getStringPref(R.string.preferences_server_port_key, R.string.preferences_server_port_default));
		} catch (NumberFormatException e) {
			// nothing
		}

		if (mSiriSession != null) {
			if (!mSiriSession.isClosed())
				mSiriSession.close();
			mSiriSession = null;
		}

		String assistantId = mPreferenceManager.getStringPref(R.string.session_assistant_id_key, R.string.session_assistant_id_default);
		tracker.trackEvent(TRACKING_SESSION_CATEGORY, TRACKING_SESSION_ACTION_CONNECT, host + ":" + port, 0);
		tracker.dispatch();
		mSiriSession = new SiriSessionImpl(host, port, assistantId, this, mSessionHandler);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mVoiceRecognizer.close())
			MyLog.i(getClass().getSimpleName(), "onPause");
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mSiriSession != null)
			mSiriSession.close();
		MyLog.i(getClass().getSimpleName(), "onStop");
	}

	/**
	 * Handle the results from the recognition activity.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TEXT_TO_SPEECH_REQUEST_CODE) {
			TTS_SERVICE_REQUEST_PENDING = false;
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				// success, create the TTS instance
				mTts = new TextToSpeech(this, new OnInitListener() {
					public void onInit(int status) {
						String lang = mPreferenceManager.getStringPref(R.string.preferences_language_lang_key, R.string.preferences_language_lang_default);
						Locale l = new Locale(lang);
						int check = mTts.isLanguageAvailable(l);
						if (check == TextToSpeech.LANG_MISSING_DATA || check == TextToSpeech.LANG_NOT_SUPPORTED) {
							MyLog.w(getClass().getSimpleName(), "lang not supported: " + l + " code=" + check);
							Toast.makeText(SiriClientActivity.this, getResources().getText(R.string.ui_tts_missing_lang) + l.toString(), Toast.LENGTH_LONG).show();
						} else {
							mTts.setLanguage(l);
						}

						mSpeaker = new SpeakerImpl(mTts, SiriClientActivity.this);
					}
				});
			} else {
				// missing data, install it
				Intent installIntent = new Intent();
				installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installIntent);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * @param match
	 * @return
	 */
	public String startCorrectedSpeechRequest(String match) {
		String aceId = UUID.randomUUID().toString();
		StartCorrectedSpeechRequest req = new StartCorrectedSpeechRequest(aceId, match);
		SiriRequest rawReq;
		try {
			if (mSiriSession == null) {
				MyLog.w(getClass().getSimpleName(), "no siri session");
				return null;
			}
			if (mSiriSession.getmRequestFactory() == null) {
				MyLog.w(getClass().getSimpleName(), "requestfactory not available");
				return null;
			}
			rawReq = mSiriSession.getmRequestFactory().create(req);
			rawReq.addOnSiriResponseListener(mSiriResponseHandler);
			mSiriSession.addToSendQueue(rawReq);
			return aceId;
		} catch (PropertyListException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void sendCommandAndRegisterForAnswer(PropertyListSerializable cmd) {
		try {
			SiriRequest req;
			req = mSiriSession.getmRequestFactory().create(cmd);
			req.addOnSiriResponseListener(mSiriResponseHandler);
			mSiriSession.addToSendQueue(req);
		} catch (PropertyListException e) {
			e.printStackTrace();
			MyLog.w(ConfirmationOptionsView.class.getSimpleName(), e.getMessage());
		}
	}

	/**
	 * @return the speaker
	 */
	public Speaker getSpeaker() {
		return mSpeaker;
	}

	/**
	 * @return the mLocationProvider
	 */
	public MyLocationProvider getLocationProvider() {
		return mLocationProvider;
	}

	/**
	 * @return the mSpeakButton
	 */
	public SpeakButtonController getSpeakButtonController() {
		return mSpeakButtonController;
	}

	/**
	 * @return the mVoiceRecognizer
	 */
	public VoiceRecognizer getVoiceRecognizer() {
		return mVoiceRecognizer;
	}

	/**
	 * @return the mAddViewsController
	 */
	public AddViewsController getAddViewsController() {
		return mAddViewsController;
	}

	/**
	 * @return the mDomainObjectLibrary
	 */
	public MyDomainObjectLibrary getDomainObjectLibrary() {
		return mDomainObjectLibrary;
	}

	/**
	 * @return the mCommandHandler
	 */
	public CommandController getCommandHandler() {
		return mCommandHandler;
	}

	/**
	 * @return the mPreferenceManager
	 */
	public MyPreferencesManager getPreferenceManager() {
		return mPreferenceManager;
	}
}
