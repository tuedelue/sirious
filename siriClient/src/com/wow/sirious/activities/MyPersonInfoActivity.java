package com.wow.sirious.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.wow.siriobjects.system.Person;

public class MyPersonInfoActivity extends Activity {
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		Bundle b = getIntent().getExtras();

		Person p = (Person) b.get("person");

		setTitle(p.getFullName());

		TextView v = new TextView(this);
		v.setText(p.toString());

		setContentView(v);
	}
}
