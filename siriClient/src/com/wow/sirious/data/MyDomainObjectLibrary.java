package com.wow.sirious.data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.wow.siriobjects.system.DomainObject;

public class MyDomainObjectLibrary {
	Map<String, DomainObject>	objects		= new HashMap<String, DomainObject>();
	Map<String, Date>			timestamps	= new HashMap<String, Date>();

	public MyDomainObjectLibrary() {

	}

	/**
	 * @param obj
	 */
	public void addObject(DomainObject obj) {
		String id = obj.getIdentifier();
		objects.put(id, obj);
		timestamps.put(id, new Date());
	}

	/**
	 * @param identifier
	 * @return
	 */
	public DomainObject getObject(String identifier) {
		if (!objects.containsKey(identifier))
			return null;

		return objects.get(identifier);
	}

	/**
	 * @param identifier
	 * @return
	 */
	public Date getTimestamp(String identifier) {
		if (!timestamps.containsKey(identifier))
			return null;

		return timestamps.get(identifier);
	}

	/**
	 * @param identifier
	 * @return
	 */
	public boolean removeObject(String identifier) {
		if (!objects.containsKey(identifier))
			return false;

		objects.remove(identifier);
		timestamps.remove(identifier);

		return true;
	}

	/**
	 * @param identifier
	 * @param add
	 * @param remove
	 * @param set
	 * @return
	 */
	public boolean updateObject(String identifier, DomainObject add, DomainObject remove, DomainObject set) {
		DomainObject obj = getObject(identifier);

		if (obj == null)
			return false;

		obj.update(add, remove, set);
		return true;
	}
}
