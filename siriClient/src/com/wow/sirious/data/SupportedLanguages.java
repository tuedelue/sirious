package com.wow.sirious.data;

import java.util.ArrayList;
import java.util.List;

import com.wow.sirious.MyLog;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;

/**
 * possiblesystem languages
 * 
 * @author helge
 * 
 */
public class SupportedLanguages {
	private List<String>								mSupportedLanguages	= new ArrayList<String>();
	private String										mLanguage;

	private List<OnSupportedLanguagesResultListener>	mListeners			= new ArrayList<SupportedLanguages.OnSupportedLanguagesResultListener>();

	public interface OnSupportedLanguagesResultListener {
		public void handleLanguagesResult(List<String> languages);
	}

	public void refreshVoiceSettings(Activity ac) {
		MyLog.i(getClass().getSimpleName(), "Sending broadcast");
		ac.sendOrderedBroadcast(RecognizerIntent.getVoiceDetailsIntent(ac), null, mSupportedLanguagesBroadCastReceiver, null, Activity.RESULT_OK, null, null);
	}

	/**
	 * 
	 */
	private BroadcastReceiver	mSupportedLanguagesBroadCastReceiver	= new BroadcastReceiver() {

																			@Override
																			public void onReceive(Context context, final Intent intent) {
																				MyLog.i(SupportedLanguages.class.getSimpleName(), "Receiving broadcast " + intent);

																				final Bundle extra = getResultExtras(false);

																				if (getResultCode() != Activity.RESULT_OK) {
																				}

																				if (extra == null) {
																				}

																				if (extra.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES)) {
																					mSupportedLanguages = extra.getStringArrayList(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES);
																					notifyListeners(mSupportedLanguages);
																				}

																				if (extra.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE)) {
																					updateLanguagePreference(extra.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE));
																				}
																			}
																		};

	/**
	 * @param string
	 */
	private void updateLanguagePreference(String string) {
		this.mLanguage = string;
	}

	/**
	 * @return the mSupportedLanguages
	 */
	public List<String> getSupportedLanguages() {
		return mSupportedLanguages;
	}

	/**
	 * @return the mLanguage
	 */
	public String getLanguage() {
		return mLanguage;
	}

	/**
	 * @param listener
	 * @return
	 */
	public boolean addListener(OnSupportedLanguagesResultListener listener) {
		if (mListeners.contains(listener))
			return false;
		mListeners.add(listener);
		return true;
	}

	/**
	 * 
	 */
	public void removeAllListeners() {
		mListeners.clear();
	}

	/**
	 * @param listener
	 * @return
	 */
	public boolean removeListener(OnSupportedLanguagesResultListener listener) {
		if (mListeners.contains(listener)) {
			mListeners.remove(listener);
			return true;
		}
		return false;
	}

	/**
	 * @param langs
	 */
	public void notifyListeners(List<String> langs) {
		for (OnSupportedLanguagesResultListener l : mListeners) {
			l.handleLanguagesResult(langs);
		}
	}
}
