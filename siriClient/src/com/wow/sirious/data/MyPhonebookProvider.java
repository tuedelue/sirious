package com.wow.sirious.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.wow.siriobjects.system.Person;
import com.wow.siriobjects.system.Phone;

public class MyPhonebookProvider {

	/**
	 * get Person entries for a given name
	 * 
	 * @param ctx
	 * @param match
	 * @return
	 */
	public static List<Person> getAllMatchingPhoneContacts(Context ctx, String match) {
		List<Person> results = new ArrayList<Person>();

		// get all contacts with phone numbers...
		String[] projection = new String[] { ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME };
		String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0 AND " + ContactsContract.Contacts.DISPLAY_NAME + " LIKE ?";
		String[] selectionArguments = new String[] { "%" + match + "%" };
		Cursor cursor = ctx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, projection, selection, selectionArguments, ContactsContract.Contacts.DISPLAY_NAME);

		// iterate them
		while (cursor.moveToNext()) {
			String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

			// to get name info
			String whereName = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.CONTACT_ID + " = ?";
			String[] whereNameParams = new String[] { ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, contactId };
			Cursor nameCur = ctx.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, whereName, whereNameParams, null);

			if (nameCur == null || nameCur.getCount() == 0)
				break;

			nameCur.moveToNext();
			String displayName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
			String familyName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
			String givenName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
			String middleName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));
			String prefix = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PREFIX));
			String suffix = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.SUFFIX));
			String phoneticFamilyName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_FAMILY_NAME));
			String phoneticGivenName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_GIVEN_NAME));
			// String phoneticMiddleName =
			// nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_MIDDLE_NAME));
			nameCur.close();

			// and to get phone numbers
			List<Phone> phonesList = new ArrayList<Phone>();
			Cursor phones = ctx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);

			if (phones == null || phones.getCount() == 0)
				continue;

			while (phones.moveToNext()) {
				String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				// String label =
				// phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
				int type = Integer.parseInt(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)));
				String label = "";
				switch (type) {
					case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
						label = "_$!<Mobile>!$_";
						break;
					case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
					case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
						label = "_$!<Work>!$_";
						break;
					case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
					case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER:
						label = "_$!<Pager>!$_";
					case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME:
						label = "_$!<HomeFAX>!$_";
						break;
					case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK:
						label = "_$!<WorkFAX>!$_";
					case ContactsContract.CommonDataKinds.Phone.TYPE_ISDN:
					case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
						label = "_$!<Home>!$_";
					case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN:
						label = "_$!<Main>!$_";
						break;
					default:
						label = "_$!<Other>!$_";
				}
				phonesList.add(new Phone(phoneNumber, label, 0, 0));
			}
			phones.close();

			// so we can create a new person object.
			Person p = new Person(contactId, suffix, null, prefix, phonesList, null, middleName, false, phoneticFamilyName, familyName, displayName, phoneticGivenName, givenName, null, null, null,
					null);
			results.add(p);
		}
		cursor.close();
		return results;
	}
}
