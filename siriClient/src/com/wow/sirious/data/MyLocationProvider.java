package com.wow.sirious.data;

import com.wow.sirious.MyLog;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * class that listens for location updates and manages its data
 * 
 * @author helge
 * 
 */
public class MyLocationProvider {

	/**
	 * min distance in meters
	 */
	private static final float	MIN_DISTANCE			= 200;
	/**
	 * periodic updates in milliseonds
	 */
	private static final long	PERIODIC_UPDATE_DELAY	= 5 * 60 * 1000;
	private String				mLocationProvider;
	private double				mLatitute;
	private double				mLongitude;
	private float				mSpeed;
	private float				mBearing;
	private double				mAltitude;
	private long				mLocationTimestamp		= 0;
	private LocationManager		mLocationManager		= null;
	/**
	 * location update listener
	 */
	private LocationListener	mLocationListener		= new LocationListener() {

															public void onStatusChanged(String provider, int status, Bundle extras) {
															}

															public void onProviderEnabled(String provider) {
															}

															public void onProviderDisabled(String provider) {
															}

															public void onLocationChanged(Location location) {
																mLocationTimestamp = location.getTime();
																mSpeed = location.getSpeed();
																mBearing = location.getBearing();
																mAltitude = location.getAltitude();
																mLatitute = location.getLatitude();
																mLongitude = location.getLongitude();
																MyLog.i(MyLocationProvider.class.getSimpleName(), "location update: longitude=" + mLongitude + " latitude=" + mLatitute);
															}
														};

	/**
	 * @param ctx
	 */
	public MyLocationProvider(Context ctx) {
		mLocationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		mLocationProvider = mLocationManager.getBestProvider(criteria, false);
		Location location = mLocationManager.getLastKnownLocation(mLocationProvider);

		if (location != null) {
			Log.i(getClass().getSimpleName(), "Provider " + mLocationProvider + " has been selected.");
			mLatitute = location.getLatitude();
			mLongitude = location.getLongitude();
		} else {
			mLatitute = 0d;
			mLongitude = 0d;
		}

	}

	/**
	 * start periodic updates
	 */
	public void startUpdates() {
		mLocationManager.requestLocationUpdates(mLocationProvider, PERIODIC_UPDATE_DELAY, MIN_DISTANCE, mLocationListener);
	}

	/**
	 * stop periodic updates
	 */
	public void stopUpdates() {
		mLocationManager.removeUpdates(mLocationListener);
	}

	/**
	 * @return the mLatitute
	 */
	public double getmLatitute() {
		return mLatitute;
	}

	/**
	 * @return the mLongitude
	 */
	public double getmLongitude() {
		return mLongitude;
	}

	/**
	 * @return the mSpeed
	 */
	public float getmSpeed() {
		return mSpeed;
	}

	/**
	 * @return the mBearing
	 */
	public float getmBearing() {
		return mBearing;
	}

	/**
	 * @return the mAltitude
	 */
	public double getmAltitude() {
		return mAltitude;
	}

	/**
	 * @return the mLocationTimestamp
	 */
	public long getmLocationTimestamp() {
		return mLocationTimestamp;
	}

	/**
	 * @return the age in milliseconds
	 */
	public long getAge() {
		return System.currentTimeMillis() - mLocationTimestamp;
	}
}
