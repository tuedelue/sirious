//TODO!!

package com.wow.sirious.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.wow.siriobjects.system.Person;

public class MyPreferencesManager {
	private SharedPreferences	sp;
	private Person				me;
	private Context				ctx;

	public MyPreferencesManager(Context ctx) {
		this.ctx = ctx;
		sp = PreferenceManager.getDefaultSharedPreferences(ctx);
	}

	/**
	 * @param key
	 * @param defValue
	 * @return
	 */
	public String getStringPref(String key, String defValue) {
		return sp.getString(key, defValue);
	}

	/**
	 * @param key_resource_id
	 * @param default_val_resource_id
	 * @return
	 */
	public String getStringPref(int key_resource_id, int default_val_resource_id) {
		String key = ctx.getResources().getString(key_resource_id);
		String default_val = ctx.getResources().getString(default_val_resource_id);
		return getStringPref(key, default_val);
	}

	/**
	 * @param key
	 * @param val
	 */
	public boolean setStringPref(String key, String val) {
		Editor e = sp.edit();
		e.putString(key, val);
		return e.commit();
	}

	/**
	 * @param key_resource_id
	 * @param val
	 */
	public boolean setStringPref(int key_resource_id, String val) {
		String key = ctx.getResources().getString(key_resource_id);
		return setStringPref(key, val);
	}

	/**
	 * @return the me
	 */
	public Person getMe() {
		return me;
	}

	/**
	 * @param me
	 *            the me to set
	 */
	public void setMe(Person me) {
		this.me = me;
	}

}
