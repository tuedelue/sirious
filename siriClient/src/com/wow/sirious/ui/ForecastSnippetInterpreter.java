package com.wow.sirious.ui;

import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wow.siriobjects.ui.AceView;
import com.wow.siriobjects.weather.WeatherForecastSnippet;
import com.wow.siriobjects.weather.WeatherObject;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.ui.views.WeatherView;

public class ForecastSnippetInterpreter extends ViewInterpreterImpl {

	public ForecastSnippetInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, AceView aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		WeatherForecastSnippet obj = (WeatherForecastSnippet) aceView;

		ViewGroup newView = (ViewGroup) new LinearLayout(callback);
		newView.setTag(aceId);

		for (WeatherObject o : obj.getAceWeathers()) {
			newView.addView(new WeatherView(callback, o));
		}

		view.addView(newView);
	}

}
