package com.wow.sirious.ui.views;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

public class ConfirmationOptionsView extends LinearLayout {

	private ConfirmationOptions	options;
	private SiriClientActivity	callback;

	private ConfirmationOptionsView(Context context) {
		super(context);
	}

	public ConfirmationOptionsView(Context context, ConfirmationOptions options, SiriClientActivity callback) {
		this(context);
		this.options = options;
		this.callback = callback;
		View.inflate(callback, R.layout.ui_confirmationoptions, this);

		setup();
	}

	private void setup() {
		Button btn_ok = (Button) findViewById(R.id.ui_confirmationoptions_button_confirm);
		Button btn_cancel = (Button) findViewById(R.id.ui_confirmationoptions_button_cancel);
		Button btn_deny = (Button) findViewById(R.id.ui_confirmationoptions_button_deny);

		// Random r = new Random(System.currentTimeMillis());
		// btn_ok.setId(r.nextInt());
		// btn_cancel.setId(r.nextInt());
		// btn_deny.setId(r.nextInt());

		btn_ok.setText(options.getConfirmText());
		btn_cancel.setText(options.getCancelLabel());
		btn_deny.setTag(options.getDenyText());

		btn_ok.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				for (ClientBoundCommand cmd : options.getConfirmCommands()) {
					callback.getCommandHandler().doCommand(cmd);
				}
				/*
				 * for (ClientBoundCommand cmd : options.getSubmitCommands()) {
				 * callback.getCommandHandler().doCommand(cmd); }
				 */
			}
		});
		btn_cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				for (ClientBoundCommand cmd : options.getCancelCommands()) {
					callback.getCommandHandler().doCommand(cmd);
				}
			}
		});
		btn_deny.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				for (ClientBoundCommand cmd : options.getDenyCommands()) {
					callback.getCommandHandler().doCommand(cmd);
				}
			}
		});
	}
}
