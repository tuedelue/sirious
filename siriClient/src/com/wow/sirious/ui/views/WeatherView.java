package com.wow.sirious.ui.views;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wow.siriobjects.weather.WeatherCondition;
import com.wow.siriobjects.weather.WeatherCurrentConditions;
import com.wow.siriobjects.weather.WeatherDailyForecast;
import com.wow.siriobjects.weather.WeatherHourlyForecast;
import com.wow.siriobjects.weather.WeatherObject;
import com.wow.siriobjects.weather.WeatherUnits;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * <pre>
 * <code>
 *        conditionsArray={
 *        		"cloudy"	{
 *      			"conditionCodeIndex":26,"conditionCode":"Cloudy",
 *       			"night":{
 *       				"conditionCodeIndex":27,"conditionCode":"MostlyCloudyNight"
 *       			}
 *       		},
 *       		"rain":{
 *       			"conditionCodeIndex":11,
 *       			"conditionCode":"Showers"
 *       		},
 *       		"unknown":{
 *       			"conditionCodeIndex":26,
 *       			"conditionCode":"Cloudy"
 *       		},
 *       		"partlycloudy":{
 *       			"conditionCodeIndex":30,
 *       			"conditionCode":"PartlyCloudyDay",
 *       			"night":{
 *       				"conditionCodeIndex":29,
 *       				"conditionCode":"PartlyCloudyNight"
 *       			}
 *       		},
 *       		"tstorms":{
 *       			"conditionCodeIndex":4,
 *       			"conditionCode":
 *       			"Thunderstorms"
 *       		},
 *       		"sunny":{
 *       			"conditionCodeIndex":32,
 *       			"conditionCode":"Sunny",
 *       			"night":{
 *       				"conditionCodeIndex":31,
 *       				"conditionCode":"ClearNight"
 *       			}
 *       		},
 *       		"snow":{
 *       			"conditionCodeIndex":16,
 *       			"conditionCode":"Snow"
 *       		},
 *       		"sleet":{
 *       			"conditionCodeIndex":18,
 *       			"conditionCode":"Sleet"
 *       		},
 *       		"partlysunny":{
 *       			"conditionCodeIndex":30,
 *       			"conditionCode":"PartlyCloudyDay",
 *       			"night":{
 *       				"conditionCodeIndex":29,
 *       				"conditionCode":"PartlyCloudyNight"
 *       			}
 *       		},
 *       		"mostlysunny":{
 *       			"conditionCodeIndex":34,
 *       			"conditionCode":"FairDay",
 *       			"night":{
 *       				"conditionCodeIndex":33,
 *       				"conditionCode":"FairNight"
 *       			}
 *       		},
 *       		"mostlycloudy":{
 *       			"conditionCodeIndex":28,
 *       			"conditionCode":"MostlyCloudyDay",
 *       			"night":{
 *       				"conditionCodeIndex":27,
 *       				"conditionCode":"MostlyCloudyNight"
 *       			}
 *       		},
 *       		"hazy":{
 *       			"conditionCodeIndex":21,
 *       			"conditionCode":"Haze",
 *       			"night":{
 *       				"conditionCodeIndex":29,
 *       				"conditionCode":"PartlyCloudyNight"
 *       			}
 *       		},
 *       		"fog":{
 *       			"conditionCodeIndex":20,
 *       			"conditionCode":"Foggy"
 *       		},
 *       		"flurries":{
 *       			"conditionCodeIndex":13,
 *       			"conditionCode":"SnowFlurries"
 *       		},
 *       		"clear":{
 *       			"conditionCodeIndex":32,
 *       			"conditionCode":"Sunny",
 *       			"night":{
 *       				"conditionCodeIndex":31,
 *       				"conditionCode":"ClearNight"
 *       			}
 *       		},
 *       		"chancetstorms":{
 *       				"conditionCodeIndex":38,
 *       				"conditionCode":"ScatteredThunderstorms"
 *       		},
 *       		"chancesnow":{
 *       			"conditionCodeIndex":42,
 *       			"conditionCode":"ScatteredSnowShowers"
 *       		},
 *       		"chancesleet":{
 *       			"conditionCodeIndex":6,
 *       			"conditionCode":"MixedRainAndSleet"
 *       		},
 *       		"chancerain":{
 *       			"conditionCodeIndex":40,
 *       			"conditionCode":"ScatteredShowers"
 *       		},
 *       		"chanceflurries":{
 *       			"conditionCodeIndex":13,
 *       			"conditionCode":"SnowFlurries"
 *       		}
 *       }</code>
 * </pre>
 */
public class WeatherView extends LinearLayout {

	private SiriClientActivity				callback;
	private WeatherObject					weatherObject;

	private static Map<Integer, Integer>	conditionsArrayDay		= new HashMap<Integer, Integer>();
	private static Map<Integer, Integer>	conditionsArrayNight	= new HashMap<Integer, Integer>();

	static {
		// "thunderstorms"
		conditionsArrayDay.put(4, R.drawable.weather_storm);
		// "mixedRainAndSleet"
		conditionsArrayDay.put(6, R.drawable.weather_snow_rain);
		// "shower"
		conditionsArrayDay.put(11, R.drawable.weather_showers);
		// "snowFlurries"
		conditionsArrayDay.put(13, R.drawable.weather_snow_scattered);
		// "snow"
		conditionsArrayDay.put(16, R.drawable.weather_snow);
		// "sleet"
		conditionsArrayDay.put(18, R.drawable.weather_hail);
		// "foggy"
		conditionsArrayDay.put(20, R.drawable.weather_few_clouds);
		// "haze"
		conditionsArrayDay.put(21, R.drawable.weather_few_clouds);
		// "cloudy"
		conditionsArrayDay.put(26, R.drawable.weather_clouds);
		// "mostylCloudy"
		conditionsArrayNight.put(27, R.drawable.weather_clouds_night);
		// "mostlyCloudy"
		conditionsArrayDay.put(28, R.drawable.weather_clouds);
		// "partyCloudy"
		conditionsArrayNight.put(29, R.drawable.weather_few_clouds_night);
		// "partlyCloudy"
		conditionsArrayDay.put(30, R.drawable.weather_few_clouds);
		// "clear"
		conditionsArrayNight.put(31, R.drawable.weather_clear_night);
		// "sunny"
		conditionsArrayDay.put(32, R.drawable.weather_clear);
		// "fairnight"
		conditionsArrayNight.put(33, R.drawable.weather_few_clouds_night);
		// "fairday"
		conditionsArrayDay.put(34, R.drawable.weather_few_clouds);
		// "scatteredThunderstorms"
		conditionsArrayDay.put(38, R.drawable.weather_storm);
		// "scatteredShowers"
		conditionsArrayDay.put(40, R.drawable.weather_showers);
		// "scatteredSnowShowers"
		conditionsArrayDay.put(42, R.drawable.weather_snow_scattered);
	}

	private WeatherView(Context context) {
		super(context);
	}

	public WeatherView(SiriClientActivity callback, WeatherObject o) {
		this(callback);

		this.callback = callback;
		this.weatherObject = o;

		View.inflate(callback, R.layout.ui_weather_forecast_snippet, this);
		setup();
	}

	/**
	 * 
	 */
	private void setup() {
		ViewGroup mainLayout = (ViewGroup) findViewById(R.id.weatherMainLayout);

		// current condition
		WeatherCurrentConditions current = weatherObject.getCurrentConditions();
		if (current != null) {
			WeatherUnits units = weatherObject.getUnits();

			String city = "";
			if (weatherObject.getWeatherLocation() != null)
				city = weatherObject.getWeatherLocation().getCity();

			int resId = current.getCondition().getConditionCodeIndex();
			String humidity = current.getPercentHumidity() + "%";
			double moon_vis = current.getPercentOfMoonFaceVisible();
			String sunrise = current.getSunrise();
			String sunset = current.getSunset();
			String temperature = current.getTemperature() + " " + units.getTemperatureUnits();

			String other = "Sunrise: " + sunrise + " Sunset: " + sunset + ", Moon visible by " + Math.round(moon_vis) + "%";

			((TextView) findViewById(R.id.weatherTxtLocation)).setText(city);
			((TextView) findViewById(R.id.weatherTxtTemperature)).setText(temperature);
			((TextView) findViewById(R.id.weatherTxtHumidity)).setText(humidity);
			((TextView) findViewById(R.id.weatherTxtOther)).setText(other);

			ImageView img = (ImageView) findViewById(R.id.weatherImgCurrent);
			int imgId = getImageDrawableIdToCondition(resId);
			img.setImageResource(imgId);
		}

		// forecasts
		String viewType = weatherObject.getView();
		if (viewType.equals(WeatherObject.ViewDAILYValue)) {
			// daily
			LinearLayout layout = new LinearLayout(callback);
			layout.setOrientation(LinearLayout.HORIZONTAL);
			layout.setGravity(Gravity.CENTER_HORIZONTAL);

			List<WeatherDailyForecast> daily = weatherObject.getDailyForecasts();
			int num = (daily.size() == 0) ? 1 : daily.size();
			int w = callback.getWindowManager().getDefaultDisplay().getWidth() / num - 5;
			for (WeatherDailyForecast h : daily) {
				WeatherCondition c = h.getCondition();
				String temperature_H = Long.toString(Math.round(h.getHighTemperature())) + "°";
				String temperature_L = Long.toString(Math.round(h.getLowTemperature())) + "°";

				String[] dow = callback.getResources().getStringArray(R.array.day_of_week_short);
				String time = dow[h.getTimeIndex() % 7];

				// setup view
				LinearLayout forecast = new LinearLayout(callback);
				forecast.setOrientation(LinearLayout.VERTICAL);
				forecast.setGravity(Gravity.CENTER_HORIZONTAL);

				TextView timeTxt = new TextView(callback);
				timeTxt.setTextAppearance(callback, android.R.style.TextAppearance_Medium);
				timeTxt.setText(time);
				timeTxt.setGravity(Gravity.CENTER);

				TextView tempLTxt = new TextView(callback);
				tempLTxt.setGravity(Gravity.CENTER);
				tempLTxt.setText(temperature_L);

				TextView tempHTxt = new TextView(callback);
				tempHTxt.setGravity(Gravity.CENTER);
				tempHTxt.setText(temperature_H);

				ImageView imgView = new ImageView(callback);
				imgView.setLayoutParams(new LayoutParams(w, w));
				imgView.setScaleType(ScaleType.CENTER_INSIDE);
				imgView.setImageResource(getImageDrawableIdToCondition(c.getConditionCodeIndex()));

				forecast.addView(timeTxt);
				forecast.addView(imgView);
				forecast.addView(tempLTxt);
				forecast.addView(tempHTxt);

				layout.addView(forecast);
			}
			mainLayout.addView(layout);
		} else if (viewType.equals(WeatherObject.ViewHOURLYValue)) {
			// hourly
			LinearLayout layout = new LinearLayout(callback);
			layout.setOrientation(LinearLayout.HORIZONTAL);
			layout.setGravity(Gravity.CENTER_HORIZONTAL);

			List<WeatherHourlyForecast> hourly = weatherObject.getHourlyForecasts();
			int num = (hourly.size() == 0) ? 1 : hourly.size() - 50;
			int w = callback.getWindowManager().getDefaultDisplay().getWidth() / num;
			for (WeatherHourlyForecast h : hourly) {
				WeatherCondition c = h.getCondition();
				double temperature = h.getTemperature();
				int time = h.getTimeIndex();

				// setup view
				LinearLayout forecast = new LinearLayout(callback);
				forecast.setOrientation(LinearLayout.VERTICAL);
				forecast.setGravity(Gravity.CENTER_HORIZONTAL);

				TextView timeTxt = new TextView(callback);
				timeTxt.setText(Integer.toString(time) + "h");
				TextView tempTxt = new TextView(callback);
				tempTxt.setText(Double.toString(temperature));

				ImageView imgView = new ImageView(callback);
				imgView.setLayoutParams(new LayoutParams(w, w));
				imgView.setScaleType(ScaleType.CENTER_INSIDE);
				imgView.setImageResource(getImageDrawableIdToCondition(c.getConditionCodeIndex()));

				forecast.addView(timeTxt);
				forecast.addView(imgView);
				forecast.addView(tempTxt);

				layout.addView(forecast);
			}
		}//

		// link for more...
		TextView link = new TextView(callback);
		link.setGravity(Gravity.RIGHT);
		final String url = weatherObject.getExtendedForecastUrl();
		link.setText(url);
		link.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				callback.startActivity(browserIntent);
			}
		});
		mainLayout.addView(link);

	}

	/**
	 * @param resId
	 * @return
	 */
	private int getImageDrawableIdToCondition(int resId) {
		int imgId = R.drawable.dummy;
		if (conditionsArrayDay.containsKey(resId))
			imgId = conditionsArrayDay.get(resId);
		else if (conditionsArrayNight.containsKey(resId))
			imgId = conditionsArrayNight.get(resId);
		return imgId;
	}
}
