package com.wow.sirious.ui.views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wow.siriobjects.sms.SmsSms;
import com.wow.siriobjects.system.PersonAttribute;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

public class SmsSnippetView extends LinearLayout {
	String						identifier;
	private SiriClientActivity	callback;
	private Context				ctx;
	boolean						mMessageTextChanged	= false;
	private EditText			etMsg;
	private Button				btn;

	public SmsSnippetView(Context ctx, SiriClientActivity callback, String identifier) {
		super(ctx);
		this.ctx = ctx;
		this.callback = callback;
		this.identifier = identifier;

		View.inflate(callback, R.layout.ui_sms_snippet, this);
		setup();
	}

	private void setup() {
		SmsSms sms = (SmsSms) callback.getDomainObjectLibrary().getObject(identifier);

		if (sms == null)
			return;

		// we have the sms, set content
		TextView txtTo = (TextView) findViewById(R.id.sms_snippet_to);
		etMsg = (EditText) findViewById(R.id.sms_snippet_message);
		btn = (Button) findViewById(R.id.sms_snippet_commitbtn);

		String recipients = "";
		if (sms.getMsgRecipients() != null) {
			boolean first = true;
			for (PersonAttribute pa : sms.getMsgRecipients()) {
				if (first == true)
					recipients = pa.getDisplayText();
				else
					recipients += ", " + pa.getDisplayText();
			}
		}
		txtTo.setText(recipients);

		etMsg.setText(sms.getMessage());

		etMsg.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (mMessageTextChanged == false) {
					changeColorAndEnableButton();
				}
				mMessageTextChanged = true;
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});

		// set btn commands
		btn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				commitChangesAndDeactivateButton();
			}
		});
	}

	private void changeColorAndEnableButton() {
		btn.setEnabled(true);
	}

	private void commitChangesAndDeactivateButton() {
		btn.setEnabled(false);
		String msg = etMsg.getText().toString();
		if (msg.contains("\n")) {
			msg = msg.replace("\n", "");

			SmsSms set = new SmsSms(null, null, null, msg, null, null, false, null, null, null, null);
			SmsSms sms = (SmsSms) callback.getDomainObjectLibrary().getObject(identifier);
			if (sms != null)
				sms.update(null, null, set);
		}

	}
}
