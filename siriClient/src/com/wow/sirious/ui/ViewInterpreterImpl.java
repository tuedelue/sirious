package com.wow.sirious.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.wow.siriobjects.ui.ViewObjectInterface;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * asbtract class for view-subitems
 * 
 * @author helge
 * 
 */
public abstract class ViewInterpreterImpl implements ViewInterpreter {
	public static final long		WAIT_BEFORE_LISTEN_AGAINT_MILLIS	= 1000;
	protected ViewGroup				view;
	protected SiriClientActivity	callback;
	protected Context				ctx;
	protected ViewObjectInterface	aceView								= null;
	protected String				aceId								= null;

	/**
	 * @param aceId
	 * @param view
	 * @param callback
	 * @param aceView
	 */
	public ViewInterpreterImpl(String aceId, ViewGroup view, SiriClientActivity callback, ViewObjectInterface aceView) {
		super();
		this.aceId = aceId;
		this.callback = callback;
		this.ctx = callback.getApplicationContext();

		LinearLayout container = new LinearLayout(ctx);
		container.setPadding(5, 5, 5, 5);
		container.setOrientation(LinearLayout.VERTICAL);
		container.setBackgroundResource(R.drawable.gradient_siri);
		view.addView(container);

		this.view = container;

		if (aceView == null) {
			throw new IllegalArgumentException("ViewInterpreter called with aceView=null");
		}
		this.aceView = aceView;
	}

	/**
	 * calculate hash with aceId and another hash
	 * 
	 * @param aceId
	 * @param hash
	 * @return
	 */
	protected static int getHash(String aceId, int hash) {
		return (aceId + Integer.toString(hash)).hashCode();
	}

	/**
	 * parse text input that may contain special symbols for special functions
	 * 
	 * @param ctx
	 * @param text
	 * @return parsed string
	 */
	protected static String parseTextInput(Context ctx, String text) {
		if (text == null)
			return null;

		if (text.contains("@{fn#currentTime}")) {
			Calendar cal = new GregorianCalendar(TimeZone.getDefault());
			Date time = cal.getTime();

			String timeStr = DateUtils.formatDateTime(ctx, time.getTime(), DateUtils.FORMAT_SHOW_TIME);
			text = text.replace("@{fn#currentTime}", timeStr);
		} else if (text.contains("@{fn#currentTimeIn#")) {
			String matchPattern = "[\\w ]?@\\{fn#currentTimeIn#([\\w\\/]+)\\}([\\w ]?)";
			Pattern p = Pattern.compile(matchPattern);
			Matcher m = p.matcher(text);
			if (m.find() && m.groupCount() > 1) {
				TimeZone tz = TimeZone.getTimeZone(m.group(1));
				Calendar cal = new GregorianCalendar(tz);
				long time = cal.get(Calendar.HOUR_OF_DAY) * 60 * 60 + cal.get(Calendar.MINUTE) * 60 + cal.get(Calendar.SECOND);
				// time -= tz.getDSTSavings() / 1000;

				String timeStr = DateUtils.formatDateTime(ctx, time * 1000, DateUtils.FORMAT_SHOW_TIME);
				text = m.replaceAll(" " + timeStr);
			} else {
				text = "Error";
			}
		}

		return text;
	}

}
