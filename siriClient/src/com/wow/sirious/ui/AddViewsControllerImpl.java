package com.wow.sirious.ui;

import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.wow.siriobjects.answer.AnswerSnippet;
import com.wow.siriobjects.clock.ClockSnippet;
import com.wow.siriobjects.contact.PersonSnippet;
import com.wow.siriobjects.localsearch.MapItemSnippet;
import com.wow.siriobjects.sms.SmsSnippet;
import com.wow.siriobjects.system.DisambiguationList;
import com.wow.siriobjects.ui.AddViews;
import com.wow.siriobjects.ui.AssistantUtteranceView;
import com.wow.siriobjects.ui.Button;
import com.wow.siriobjects.ui.ViewObjectInterface;
import com.wow.siriobjects.weather.WeatherForecastSnippet;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.userinteraction.Recognition;

/**
 * controller that manages display a AddViews objects
 * 
 * @author helge
 * 
 */
public class AddViewsControllerImpl implements AddViewsController {
	private static final long	SCOLL_DOWN_WAIT	= 300;
	private ViewGroup			view;
	private SiriClientActivity	callback;
	private Context				ctx;

	public AddViewsControllerImpl(ViewGroup view, SiriClientActivity callback) {
		this.view = view;
		this.ctx = view.getContext();
		this.callback = callback;
	}

	/**
	 * add new textview to siri screen & add popups
	 * 
	 * @param text
	 */
	public void addRecognition(Recognition recognition) {
		String text = recognition.getTopFilteredRecognition();

		View newView = creatText(text);

		// setup popup
		List<String> filteredRecognitions = recognition.getFilteredRecognitions();
		if (filteredRecognitions.size() > 1) {

			// setup pop-view
			final ViewGroup alternatives = (ViewGroup) View.inflate(ctx, R.layout.ui_alternatives, null);
			final PopupWindow pw = new PopupWindow(ctx);
			pw.setContentView(alternatives);

			// setup spinner data
			Spinner spn = (Spinner) alternatives.findViewById(R.id.ui_alternatives_spinner);
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, filteredRecognitions);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spn.setAdapter(dataAdapter);

			spn.setOnItemSelectedListener(new OnItemSelectedListener() {
				// onItemSelected will be triggered on creation - discard that
				// event with this flag
				private boolean	first	= true;

				public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
					if (first == true) {
						first = false;
						return;
					}
					String item = parent.getItemAtPosition(pos).toString();
					creatText(item);
					callback.startCorrectedSpeechRequest(item);
					pw.dismiss();
				}

				public void onNothingSelected(AdapterView<?> arg0) {
					pw.dismiss();
				}
			});

			// calc width and height
			int displayw = callback.getWindowManager().getDefaultDisplay().getWidth();
			final int width = Math.round(displayw * 0.75f);
			final int xoff = Math.round(displayw * .25f);
			final int height = 70;

			// set it touchable, dismiss non outside-touch
			pw.setOutsideTouchable(true);
			pw.setTouchInterceptor(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
						pw.dismiss();
						return true;
					}
					return false;
				}
			});

			// finally add onclicklistener to view
			newView.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					pw.showAsDropDown(v, xoff, 5);
					// pw.showAtLocation(v, Gravity.RIGHT | Gravity.TOP, 5, 5);
					pw.update(v, xoff, -5, width, height);
				}
			});
		}

		// scroll down.
		scrollDown();

	}

	private View creatText(String text) {
		// setup text
		ViewGroup table = (ViewGroup) callback.findViewById(R.id.siriPlayground);

		LinearLayout container = new LinearLayout(callback);
		container.setPadding(5, 5, 5, 5);
		container.setOrientation(LinearLayout.VERTICAL);
		container.setBackgroundResource(R.drawable.gradient_me);
		table.addView(container);

		View newView = View.inflate(callback, R.layout.ui_assistant_utterance_view, (ViewGroup) container);
		newView.setTag("dummy");

		// add text to view
		TextView txtView = (TextView) newView.findViewById(R.id.text);
		txtView.setId(text.hashCode());
		txtView.setText(PREFIX_ME + text);
		return newView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.ui.AddViewsControllerImpl#createView()
	 */
	public void createView(AddViews addViews) {
		String aceId = addViews.getAceId();
		for (ViewObjectInterface o : addViews.getViews()) {
			if (o instanceof AssistantUtteranceView) {
				new AssistantUttereanceViewInterpreter(aceId, view, callback, (AssistantUtteranceView) o).showAceView();
				continue;
			}
			if (o instanceof DisambiguationList) {
				new DisambiguationListInterpreter(aceId, view, callback, (DisambiguationList) o).showAceView();
				continue;
			}
			if (o instanceof ClockSnippet) {
				new ClockSnippetInterpreter(aceId, view, callback, (ClockSnippet) o).showAceView();
				continue;
			}
			if (o instanceof MapItemSnippet) {
				new MapItemSnippetInterpreter(aceId, view, callback, (MapItemSnippet) o).showAceView();
				continue;
			}
			if (o instanceof AnswerSnippet) {
				new AnswerSnippetInterpreter(aceId, view, callback, (AnswerSnippet) o).showAceView();
				continue;
			}
			if (o instanceof Button) {
				new ButtonInterpreter(aceId, view, callback, (Button) o).showAceView();
				continue;
			}
			if (o instanceof PersonSnippet) {
				new PersonSnippetInterpreter(aceId, view, callback, (PersonSnippet) o).showAceView();
				continue;
			}
			if (o instanceof SmsSnippet) {
				new SmsSnippetInterpreter(aceId, view, callback, (SmsSnippet) o).showAceView();
				continue;
			}
			if (o instanceof WeatherForecastSnippet) {
				new ForecastSnippetInterpreter(aceId, view, callback, (WeatherForecastSnippet) o).showAceView();
				continue;
			}
		}// for
		scrollDown();
	}

	/**
	 * scrolls the scollview down
	 */
	private void scrollDown() {
		new Handler().postDelayed(new Runnable() {
			public void run() {
				ScrollView s = (ScrollView) callback.findViewById(R.id.scrollView1);
				s.fullScroll(ScrollView.FOCUS_DOWN);
			}
		}, SCOLL_DOWN_WAIT);
	}
}
