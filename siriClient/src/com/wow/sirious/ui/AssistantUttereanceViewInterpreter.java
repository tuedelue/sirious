package com.wow.sirious.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wow.siriobjects.ui.AssistantUtteranceView;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * @see {@link AssistantUtteranceView}
 * @author helge
 * 
 */
public class AssistantUttereanceViewInterpreter extends ViewInterpreterImpl {

	public AssistantUttereanceViewInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, AssistantUtteranceView aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		AssistantUtteranceView obj = (AssistantUtteranceView) aceView;

		if (obj.getText() != null && !obj.getText().equals("")) {

			View newView = View.inflate(ctx, R.layout.ui_assistant_utterance_view, view);
			newView.setTag(aceId);

			// add text to view
			TextView txtView = (TextView) newView.findViewById(R.id.text);
			txtView.setText(AddViewsController.PREFIX_SIRI + parseTextInput(ctx, obj.getText()));

			txtView.setId(getHash(aceId, obj.hashCode()));
		}

		// speak
		String speakableText = parseTextInput(ctx, obj.getSpeakableText());
		if (callback.getSpeaker() != null)
			callback.getSpeaker().speakText(speakableText);

		// listen again?!
		// TODO
		/*
		 * if (obj.isListenAfterSpeaking()) { new Handler().postDelayed(new
		 * Runnable() { public void run() {
		 * callback.getVoiceRecognizer().startVoiceRecognition(); } },
		 * ViewInterpreterImpl.WAIT_BEFORE_LISTEN_AGAINT_MILLIS); }
		 */
	}

}
