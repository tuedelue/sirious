package com.wow.sirious.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

public class SpeakButtonControlImpl implements SpeakButtonController {
	private ImageButton			mSpeakButton	= null;
	private SiriClientActivity	callback		= null;

	public SpeakButtonControlImpl(SiriClientActivity callback) {
		this.callback = callback;

		// Get display items for later interaction
		mSpeakButton = (ImageButton) callback.findViewById(R.id.btn_speak);
		mSpeakButton.setBackgroundResource(R.drawable.speak_button_inactive);
		deactivate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.ui.SpeakButtonController#avtivate()
	 */
	public void activate() {
		mSpeakButton.setBackgroundResource(R.drawable.speak_button_inactive);
		mSpeakButton.setEnabled(true);
		mSpeakButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (v.getId() == R.id.btn_speak) {
					callback.getVoiceRecognizer().startVoiceRecognition();
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.ui.SpeakButtonController#deactivate()
	 */
	public void deactivate() {
		mSpeakButton.setBackgroundResource(R.drawable.speak_button_disabled);
		mSpeakButton.setEnabled(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.ui.SpeakButtonController#setSpeechInUse()
	 */
	public void setSpeechInUse() {
		mSpeakButton.setBackgroundResource(R.drawable.speak_button_active);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.ui.SpeakButtonController#setSpeechDone()
	 */
	public void setSpeechDone() {
		mSpeakButton.setBackgroundResource(R.drawable.speak_button_inactive);
	}
}
