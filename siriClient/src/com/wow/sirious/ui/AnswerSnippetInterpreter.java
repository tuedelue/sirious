package com.wow.sirious.ui;

import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;

import com.wow.siriobjects.answer.Answer;
import com.wow.siriobjects.answer.AnswerLine;
import com.wow.siriobjects.answer.AnswerSnippet;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * @see {@link AnswerSnippet}
 * @author helge
 * 
 */
public class AnswerSnippetInterpreter extends ViewInterpreterImpl {

	public AnswerSnippetInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, AnswerSnippet aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		AnswerSnippet obj = (AnswerSnippet) aceView;

		String data = "";

		data = "<html><body>" + data + "</body></html>";

		for (Answer a : obj.getAnswers()) {
			data += "<p><b>" + a.getTitle() + "</b><br/>";
			for (AnswerLine l : a.getLines()) {
				String text = l.getText();
				if (text != null) {
					data += l.getText().replace("\n", "<br/>");
				}
				if (l.getImage() != null && l.getImage() != "") {
					data += "<img src='" + l.getImage() + "' alt=''/>";
				}
			}
			data += "</p>";
		}

		WebView w = new WebView(ctx);
		w.loadData(data, MimeTypeMap.getSingleton().getMimeTypeFromExtension("html"), "utf-8");
		view.addView(w);

		/*
		 * ViewGroup snippet = (ViewGroup) View.inflate(ctx,
		 * R.layout.ui_answer_snippet, view); snippet.setTag(aceId);
		 * 
		 * for (SiriAnswer answer : obj.getAnswers()) { View answerView =
		 * View.inflate(ctx, R.layout.ui_answer_answer, snippet);
		 * answerView.setTag(aceId); TextView titleTxtView = (TextView)
		 * answerView.findViewById(R.id.title);
		 * titleTxtView.setId(getHash(aceId, answer.hashCode()));
		 * titleTxtView.setText(answer.getTitle());
		 * 
		 * TableLayout answerTable = (TableLayout)
		 * answerView.findViewById(R.id.answers); for (SiriAnswerLine line :
		 * answer.getLines()) { View newLine = View.inflate(ctx,
		 * R.layout.ui_answer_line, answerTable); TableRow row = (TableRow)
		 * newLine.findViewById(R.id.line); newLine.setId(getHash(aceId,
		 * line.hashCode()));
		 * 
		 * TextView txt = (TextView) newLine.findViewById(R.id.text); if
		 * (!line.getText().equals("")) { txt.setText(line.getText());
		 * txt.setId(getHash(aceId, line.getText().hashCode())); } else {
		 * txt.setVisibility(View.GONE); ((ViewGroup) newLine).removeView(txt);
		 * }
		 * 
		 * final ImageView img = (ImageView) newLine.findViewById(R.id.image);
		 * if (!line.getImage().equals("")) { img.setId(getHash(aceId,
		 * line.getImage().hashCode())); final String url = line.getImage(); new
		 * Handler().post(new Runnable() { public void run() { try { URL
		 * myFileUrl; myFileUrl = new URL(url); HttpURLConnection conn =
		 * (HttpURLConnection) myFileUrl.openConnection();
		 * conn.setDoInput(true); conn.connect(); InputStream is =
		 * conn.getInputStream();
		 * 
		 * Bitmap bmImg = BitmapFactory.decodeStream(is);
		 * img.setImageBitmap(bmImg); } catch (MalformedURLException e) {
		 * e.printStackTrace(); Log.w("siri", e.getMessage()); } catch
		 * (IOException e) { e.printStackTrace(); Log.w("siri", e.getMessage());
		 * }
		 * 
		 * } }); } else { img.setVisibility(View.GONE); ((ViewGroup)
		 * newLine).removeView(img); } } }
		 */
	}

}
