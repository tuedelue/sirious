package com.wow.sirious.ui;

import java.util.List;
import java.util.UUID;

import android.view.View;
import android.view.ViewGroup;

import com.wow.siriobjects.system.SendCommands;
import com.wow.siriobjects.system.StartRequest;
import com.wow.siriobjects.ui.Button;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * @see {@link Button}
 * @author helge
 * 
 */
public class ButtonInterpreter extends ViewInterpreterImpl {

	public ButtonInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, Button aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		Button obj = (Button) aceView;

		final List<SendCommands> sendCommands = obj.getCommands();
		String text = obj.getText();
		android.widget.Button btn = new android.widget.Button(ctx);
		btn.setText(text);
		btn.setId(UUID.randomUUID().hashCode());
		btn.setOnClickListener(new android.widget.Button.OnClickListener() {
			public void onClick(View v) {
				for (SendCommands commands : sendCommands) {
					List<StartRequest> startRequests = commands.getCommands();
					for (StartRequest s : startRequests) {
						callback.startCorrectedSpeechRequest(s.getUtterance());
					}
				}
			}
		});
		view.addView(btn);
	}

}
