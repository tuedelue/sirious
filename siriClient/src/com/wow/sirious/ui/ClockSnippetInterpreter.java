package com.wow.sirious.ui;

import java.util.TimeZone;

import android.view.View;
import android.view.ViewGroup;

import com.wow.siriobjects.clock.ClockObject;
import com.wow.siriobjects.clock.ClockSnippet;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * @see {@link ClockSnippet}
 * @author helge
 * 
 */
public class ClockSnippetInterpreter extends ViewInterpreterImpl {

	public ClockSnippetInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, ClockSnippet aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		ClockSnippet obj = (ClockSnippet) aceView;

		ViewGroup newView = (ViewGroup) View.inflate(ctx, R.layout.ui_clock_snippet, view);
		newView.setTag(aceId);

		for (ClockObject clock : obj.getClocks()) {
			View clockView = (View) View.inflate(ctx, R.layout.ui_clock, newView);

			TimeZone zone = TimeZone.getTimeZone(clock.getTimezoneId());
		}

	}

}
