package com.wow.sirious.ui;

import android.content.Intent;
import android.net.Uri;
import android.view.ViewGroup;

import com.wow.siriobjects.localsearch.MapItem;
import com.wow.siriobjects.localsearch.MapItemSnippet;
import com.wow.siriobjects.system.Location;
import com.wow.sirious.MyLog;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * 
 * @see de.helge.siri.siriobjects.localsearch.MapItemSnippet
 * @author helge
 * 
 */
public class MapItemSnippetInterpreter extends ViewInterpreterImpl {

	private static final long	SHOW_MAP_DELAY_MILLIS	= 0;

	public MapItemSnippetInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, MapItemSnippet aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		MapItemSnippet obj = (MapItemSnippet) aceView;
		MapItem mapItem = obj.getItems().iterator().next();
		Location l = mapItem.getLocation();

		callback.getSpeaker().cancelSpeaking();

		Uri uri = Uri.parse("geo:0,0?q=" + l.getLatitude() + "," + l.getLongitude() + " (" + l.getLabel() + ")");
		MyLog.i(getClass().getSimpleName(), "opening uri: " + uri.toString());
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
		callback.startActivity(intent);
		/*
		 * final Intent i = new Intent(ctx, MyMapActivity.class); if (l != null)
		 * { i.putExtra("latitude", l.getLatitude()); i.putExtra("longitude",
		 * l.getLongitude()); i.putExtra("label", l.getLabel()); }
		 * 
		 * new Handler().postDelayed(new Runnable() {
		 * 
		 * public void run() { callback.startActivity(i); } },
		 * SHOW_MAP_DELAY_MILLIS);
		 */
	}
}
