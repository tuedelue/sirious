package com.wow.sirious.ui;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wow.siriobjects.contact.PersonSnippet;
import com.wow.siriobjects.system.Person;
import com.wow.siriobjects.ui.ViewObjectInterface;
import com.wow.sirious.activities.MyPersonInfoActivity;
import com.wow.sirious.activities.SiriClientActivity;

public class PersonSnippetInterpreter extends ViewInterpreterImpl {

	public PersonSnippetInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, ViewObjectInterface aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		PersonSnippet obj = (PersonSnippet) aceView;

		List<Person> persons = obj.getPersons();

		if (persons == null || persons.size() == 0)
			return;

		for (final Person p : persons) {
			TextView v = new TextView(ctx);
			v.setText(p.getFullName());

			v.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent i = new Intent(ctx, MyPersonInfoActivity.class);
					Bundle b = new Bundle();
					b.putSerializable("person", p);
					i.putExtras(b);

					callback.startActivity(i);
				}
			});

			view.addView(v);
		}
	}

}
