package com.wow.sirious.ui;

import com.wow.siriobjects.ui.AddViews;
import com.wow.sirious.userinteraction.Recognition;

public interface AddViewsController {
	String	PREFIX_ME	= "Me: ";
	String	PREFIX_SIRI	= "Siri: ";

	/**
	 * iterate alle views and create the sub-items
	 */
	public void createView(AddViews obj);

	/**
	 * simply add a text from "ME"
	 * 
	 * @param text
	 */
	public void addRecognition(Recognition recognition);

}
