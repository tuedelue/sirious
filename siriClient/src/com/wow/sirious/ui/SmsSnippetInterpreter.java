package com.wow.sirious.ui;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;

import com.wow.siriobjects.sms.SmsSms;
import com.wow.siriobjects.sms.SmsSnippet;
import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.siriobjects.ui.ViewObjectInterface;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.ui.views.ConfirmationOptionsView;
import com.wow.sirious.ui.views.SmsSnippetView;

public class SmsSnippetInterpreter extends ViewInterpreterImpl {

	public SmsSnippetInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, ViewObjectInterface aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		SmsSnippet obj = (SmsSnippet) aceView;

		List<SmsSms> smslist = obj.getSmss();
		for (final SmsSms sms : smslist) {
			View newView = new SmsSnippetView(ctx, callback, sms.getIdentifier());
			newView.setTag(aceId);
			view.addView(newView);
		}

		// confirmationoptions
		ConfirmationOptions options = obj.getConfirmationOptions();
		if (options != null) {
			ConfirmationOptionsView cv = new ConfirmationOptionsView(ctx, options, callback);
			view.addView(cv);
		}
	}

}
