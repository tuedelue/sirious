package com.wow.sirious.ui;

public interface SpeakButtonController {

	public abstract void activate();

	public abstract void deactivate();

	public abstract void setSpeechInUse();

	public abstract void setSpeechDone();

}
