package com.wow.sirious.ui;

import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.system.DisambiguationList;
import com.wow.siriobjects.system.ListItem;
import com.wow.sirious.activities.SiriClientActivity;

/**
 * @see {@link DisambiguationList}
 * @author helge
 * 
 */
public class DisambiguationListInterpreter extends ViewInterpreterImpl {

	public DisambiguationListInterpreter(String aceId, ViewGroup view, SiriClientActivity callback, DisambiguationList aceView) {
		super(aceId, view, callback, aceView);
	}

	public void showAceView() {
		DisambiguationList obj = (DisambiguationList) aceView;
		final String selectionResponse = obj.getSelectionResponse();
		final String speakableDemitter = obj.getSpeakableDelimitter();
		final String speakableEndDelimiter = obj.getSpeakableFinalDelimitter();

		boolean first = true;
		for (ListItem i : obj.getItems()) {
			if (first == true) {
				first = false;
			} else {
				callback.getSpeaker().speakText(speakableDemitter);
			}

			final List<ClientBoundCommand> commands = i.getCommands();
			String text = i.getSelectionText();
			String title = i.getTitle();
			String selectionText = i.getSelectionText();
			String speakbaleText = i.getSpeakableText();

			TextView txtView = new TextView(ctx);
			txtView.setTag(aceId);
			txtView.setText(selectionText);

			txtView.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					callback.getSpeaker().cancelSpeaking();
					callback.getSpeaker().speakText(selectionResponse);

					for (ClientBoundCommand s : commands) {
						callback.handleCommand(s);
					}
				}
			});

			view.addView(txtView);

			callback.getSpeaker().speakText(speakbaleText);
		}

		if (first == false) {
			callback.getSpeaker().speakText(speakableEndDelimiter);
		}
	}

}
