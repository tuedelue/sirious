package com.wow.sirious.net;

public interface OnSiriSessionHandler {
	public void handleClosed(SiriSession s);

	public void handleEstablished(SiriSession s);
}
