package com.wow.sirious.net;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface SiriSession {

	/**
	 * terminates prediodic pings, terminates the receiver-thread and closes all
	 * streams the socket
	 * 
	 * @throws IOException
	 */
	public void close();

	/**
	 * @return
	 */
	public String getClosedReason();

	/**
	 * add a new message to the sending queue - MAIN ENTRANCE for this.
	 * 
	 * @param req
	 */
	public void addToSendQueue(SiriRequest req);

	/**
	 * @return the Requests
	 */
	public Map<String, SiriRequest> getRequests();

	/**
	 * @return the responses
	 */
	public Map<String, List<SiriResponse>> getResponses();

	/**
	 * @return the mResponseFactory
	 */
	public SiriResponse.factory getmResponseFactory();

	/**
	 * @return the mRequestFactory
	 */
	public SiriRequest.factory getmRequestFactory();

	/**
	 * @return the assistant id
	 */
	public String getAssistantId();

	/**
	 * @return is the connection closed
	 */
	public boolean isClosed();

	/**
	 * trigger server to delete assistant this will not ensure that this really
	 * happens!
	 * 
	 * @return true on successful submit
	 */
	public boolean destroyAssistant();

	/**
	 * @return
	 */
	public String getServerAddress();
}
