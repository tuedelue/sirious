package com.wow.sirious.net;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.dd.plist.BinaryPropertyListParser;
import com.dd.plist.BinaryPropertyListWriter;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.PropertyListException;
import com.wow.siriobjects.ObjectCreator;
import com.wow.siriobjects.PropertyListSerializable;

/**
 * generic siri message, this class holds the object as well as its
 * plist-representation
 * 
 * @author helge
 * 
 */
public abstract class SiriMessage implements Serializable {
	private static final long			serialVersionUID	= 6853304743221867564L;
	/**
	 * type of this message
	 */
	protected CMD						cmd;

	/**
	 * timestamp at creation
	 */
	private Date						timestamp			= new Date();

	/**
	 * object representation
	 */
	private PropertyListSerializable	object				= null;
	/**
	 * plist representation
	 */
	private NSDictionary				plist				= null;
	/**
	 * binary representation
	 */
	private byte[]						plistBytes			= null;

	/**
	 * sets up the Plist from binary
	 * 
	 * @throws PropertyListException
	 */
	private void setPlistFromBytes() throws PropertyListException {
		if (plistBytes == null)
			return;
		NSObject obj = BinaryPropertyListParser.parse(plistBytes);
		plist = (NSDictionary) obj;
	}

	/**
	 * setup binary from plist
	 * 
	 * @throws PropertyListException
	 */
	private void setPlistBytesFromNSObject() throws PropertyListException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			BinaryPropertyListWriter.write(out, plist);
		} catch (IOException e) {
			throw new PropertyListException(e);
		}
		this.plistBytes = out.toByteArray();
	}

	/**
	 * sets up the plist and binary form from object
	 * 
	 * @param plist
	 *            the plist to set
	 * @throws PropertyListException
	 */
	protected void setObjectAndPlistAndBytes(PropertyListSerializable plist) throws PropertyListException {
		if (plist == null)
			return;
		this.object = plist;
		this.plist = plist.toPlist();
		setPlistBytesFromNSObject();
	}

	/**
	 * sets up the object from binary
	 * 
	 * @param plistBytes
	 *            the plistBytes to set
	 * @throws PropertyListException
	 */
	protected void setObjectFromPlistBytes(byte[] plistBytes) throws PropertyListException {
		if (plistBytes == null)
			return;
		this.plistBytes = plistBytes;
		setPlistFromBytes();

		/*
		 * if (((NSString)
		 * plist.objectForKey("class")).toString().equals("AddViews")) {
		 * 
		 * @SuppressWarnings("unused") AddViews tmp = AddViews.fromPlist(plist);
		 * }
		 */
		PropertyListSerializable obj = ObjectCreator.getInstance().createInstance(plist);
		this.object = obj;
	}

	/**
	 * @return the cmd
	 */
	public CMD getCmd() {
		return cmd;
	}

	/**
	 * @return the command object
	 */
	public PropertyListSerializable getObject() {
		return object;
	}

	/**
	 * @return the plist
	 */
	public NSDictionary getPlist() {
		return plist;
	}

	/**
	 * @return the plistBytes
	 */
	public byte[] getPlistBytes() {
		return plistBytes;
	}

	/**
	 * @return unix timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @return formatted timestamp <code>HH:mm:ss</code>
	 */
	public String getTimestampFormatted() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(timestamp);
	}
}
