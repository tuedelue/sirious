package com.wow.sirious.net;

import java.net.ProtocolException;

/**
 * generic protocol exception
 * 
 * @author helge
 * 
 */
public class SiriProtocolException extends ProtocolException {
	private static final long	serialVersionUID	= -9030333816034771825L;

	private final String		detailMessage;

	public SiriProtocolException(String detailMessage) {
		super();
		this.detailMessage = detailMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return detailMessage;
	}

}
