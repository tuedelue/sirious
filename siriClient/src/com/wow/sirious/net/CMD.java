package com.wow.sirious.net;

/**
 * This Enum denotes a command being either a PLIST, PING or PONG
 * 
 * @author helge
 * 
 */
public enum CMD {
	PLIST((byte) 0x02), PING((byte) 0x03), PONG((byte) 0x04);
	private byte	id;

	CMD(byte id) {
		this.id = id;
	}

	public String toString() {
		return name();
	}

	public byte getId() {
		return id;
	}

	public static CMD getEnumForId(byte cmd) {
		for (CMD v : CMD.values()) {
			if (v.getId() == cmd)
				return v;
		}
		return null;
	}
}
