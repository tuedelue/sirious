package com.wow.sirious.net;

/**
 * Interface for a ResponseListener to a SiriRequest All subsequent Responses
 * having the requests AceId as RefId are taken into account
 * 
 * @author helge
 * 
 */
public interface OnSiriResponseListener {
	public void handleResponse(SiriRequest req, SiriResponse res);
}
