package com.wow.sirious.net;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.PropertyListException;
import com.wow.siriobjects.PropertyListSerializable;

/**
 * Siri Request Message
 * 
 * @author helge
 * 
 */
public class SiriRequest extends SiriMessage {
	private static final long				serialVersionUID	= 402915695600516924L;
	private final int						ping;
	public static int						NEXT_PING			= 1;
	/**
	 * list of registered responselisteners
	 */
	private List<OnSiriResponseListener>	callback			= new ArrayList<OnSiriResponseListener>();

	/**
	 * @param cmd
	 * @param plist
	 * @param ping
	 * @throws PropertyListException
	 */
	private SiriRequest(CMD cmd, PropertyListSerializable plist, int ping) throws PropertyListException {
		this.cmd = cmd;
		this.ping = ping;

		setObjectAndPlistAndBytes(plist);
	}

	/**
	 * factory for requests
	 * 
	 * @author helge
	 * 
	 */
	public static class factory {
		private factory() {
		}

		/**
		 * creates next ping
		 * 
		 * @return next ping request
		 */
		public SiriRequest createPingNext() {
			try {
				return new SiriRequest(CMD.PING, null, NEXT_PING++);
			} catch (PropertyListException e) {
				throw new IllegalArgumentException(e);
			}
		}

		/**
		 * create ping with a certain ping sequence number
		 * 
		 * @param ping
		 * @return certain ping request
		 */
		public SiriRequest createPing(int ping) {
			try {
				return new SiriRequest(CMD.PING, null, ping);
			} catch (PropertyListException e) {
				throw new IllegalArgumentException(e);
			}
		}

		/**
		 * create message from Object
		 * 
		 * @param plist
		 * @return object request
		 * @throws PropertyListException
		 */
		public SiriRequest create(PropertyListSerializable plist) throws PropertyListException {
			return new SiriRequest(CMD.PLIST, plist, -1);
		}

		/**
		 * @return factory instance
		 */
		public static factory getInstance() {
			return new factory();
		}
	}

	/**
	 * @return the ping sequence
	 */
	public int getPing() {
		return ping;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		switch (this.cmd) {
			case PING:
				return "(" + getTimestampFormatted() + ") PING seq=" + ping;
			case PLIST:
				String name = null;
				if (getObject() != null) {
					name = getObject().getClass().getSimpleName();
				}
				return "(" + getTimestampFormatted() + ") REQ " + name + " PLIST=" + getPlist();
			default:
				throw new NullPointerException("wrong cmd: " + cmd);
		}
	}

	/**
	 * add a new response listener
	 * 
	 * @param callback
	 */
	public void addOnSiriResponseListener(OnSiriResponseListener callback) {
		if (!this.callback.contains(callback)) {
			this.callback.add(callback);
		}
	}

	/**
	 * remove a response listener
	 * 
	 * @param callback
	 */
	public void removeSiriResponseListener(OnSiriResponseListener callback) {
		if (this.callback.contains(callback)) {
			this.callback.remove(callback);
		}
	}

	/**
	 * remove all response listeners
	 */
	public void removeAllSiriResponseListeners() {
		this.callback.clear();
	}

	/**
	 * notity all listeners about a response
	 * 
	 * @param response
	 */
	public void notifyListeners(SiriResponse res) {
		if (callback == null || callback.size() == 0)
			return;

		for (OnSiriResponseListener l : callback) {
			l.handleResponse(this, res);
		}
	}
}
