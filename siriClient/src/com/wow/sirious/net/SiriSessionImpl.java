package com.wow.sirious.net;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.dd.plist.PropertyListException;
import com.jcraft.jzlib.DeflaterOutputStream;
import com.jcraft.jzlib.InflaterInputStream;
import com.wow.siriobjects.ObjectCreator;
import com.wow.siriobjects.PropertyListSerializable;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.AssistantCreated;
import com.wow.siriobjects.system.AssistantLoaded;
import com.wow.siriobjects.system.AssistantNotFound;
import com.wow.siriobjects.system.CommandFailed;
import com.wow.siriobjects.system.CreateAssistant;
import com.wow.siriobjects.system.CreateSessionInfoRequest;
import com.wow.siriobjects.system.CreateSessionInfoResponse;
import com.wow.siriobjects.system.DestroyAssistant;
import com.wow.siriobjects.system.LoadAssistant;
import com.wow.siriobjects.system.Person;
import com.wow.siriobjects.system.SetAssistantData;
import com.wow.sirious.MyLog;
import com.wow.sirious.R;
import com.wow.sirious.activities.SiriClientActivity;
import com.wow.sirious.data.MyPreferencesManager;
import com.wow.sirious.data.PhoneUUID;

/**
 * @author helge
 * 
 */
/**
 * @author helge
 * 
 */
public class SiriSessionImpl implements SiriSession {
	private OnSiriSessionHandler						mSessionHandler				= null;
	public static final String							CHARSET						= "utf-8";
	public static final String							CRLF						= "\r\n";
	// private static final String DEFAULT_USER_AGENT =
	// "Assistant(iPhone/iPhone3,1; iPhone OS/5.0.1/9A405) Ace/1.0";
	private static final String							DEFAULT_USER_AGENT			= "Assistant(Android) Ace/1.0";
	private String										httpRequestHeader			= null;
	private static final String							DEBUG_TAG					= SiriSessionImpl.class.getSimpleName();
	private static final String							HEADER_STATUSLINE_KEY		= "##header#line##";
	private static final String							HANDLER_SEND_DATA_KEY		= "data";

	private String										HOST						= null;
	private int											PORT						= 0;

	/**
	 * the socket
	 */
	private Socket										sslsocket;
	volatile private BufferedOutputStream				bout;
	volatile private BufferedInputStream				bin;

	/**
	 * all sent requests stored by their aceId
	 */
	volatile private Map<String, SiriRequest>			mRequests					= new HashMap<String, SiriRequest>();
	/**
	 * all responses stored by refId
	 */
	volatile private Map<String, List<SiriResponse>>	mResponses					= new HashMap<String, List<SiriResponse>>();

	private static final int							HANDLER_SEND_CMD			= 42;
	private static final int							INIT_SEND_HTTP				= 50;
	private static final int							INIT_SEND_ACE2				= 51;
	private static final int							INIT_RECV_HTTP				= 60;
	private static final int							INIT_RECV_ACE2				= 61;
	private static final int							INIT_START_REMAINING		= 70;
	/**
	 * receiver instance
	 */
	private Receiver									mReceiver					= null;
	/**
	 * receiver thread running the instance
	 */
	private Thread										mReveicerThread				= null;

	private SiriResponse.factory						mResponseFactory			= null;
	private SiriRequest.factory							mRequestFactory				= null;

	/**
	 * periodic ping interval
	 */
	private long										PING_DELAY_MILLIS			= 1 * 1000;
	private boolean										PING_ENABLE_PERIODICALLY	= true;

	/**
	 * task sending pings
	 */
	private Runnable									mSendPingTask				= new Runnable() {
																						public void run() {
																							Message msg = mSendHandler.obtainMessage(HANDLER_SEND_CMD);
																							SiriRequest req = mRequestFactory.createPingNext();
																							Bundle b = new Bundle();
																							b.putSerializable(HANDLER_SEND_DATA_KEY, req);
																							msg.setData(b);
																							msg.sendToTarget();

																							if (PING_ENABLE_PERIODICALLY)
																								mSendPeriodicPingsHandler.postDelayed(mSendPingTask, PING_DELAY_MILLIS);
																						}
																					};
	/**
	 * handler running the sending ping task
	 */
	private final Handler								mSendPeriodicPingsHandler	= new Handler();

	/**
	 * handler for sending a new message
	 */
	private final Handler								mSendHandler				= new Handler() {
																						@Override
																						public void handleMessage(Message msg) {
																							switch (msg.what) {
																								case HANDLER_SEND_CMD:
																									SiriRequest req = (SiriRequest) msg.getData().get(HANDLER_SEND_DATA_KEY);
																									sendCommand(req);
																									break;
																								default:
																									super.handleMessage(msg);

																							}
																						}
																					};

	/**
	 * handler managing all incoming messages
	 */
	private final Handler								mReceiveHandler				= new Handler() {
																						@Override
																						public void handleMessage(Message msg) {
																							CMD cmd = CMD.getEnumForId((byte) msg.what);
																							SiriResponse res;
																							switch (cmd) {
																								case PONG:
																									res = (SiriResponse) msg.getData().get(HANDLER_SEND_DATA_KEY);
																									break;
																								case PLIST:
																									res = (SiriResponse) msg.getData().get(HANDLER_SEND_DATA_KEY);
																									addAnswer(res);
																									MyLog.i(DEBUG_TAG, res.toString());
																									break;
																								default:
																									super.handleMessage(msg);

																							}
																						}
																					};
	volatile private DeflaterOutputStream				zout;
	volatile private InflaterInputStream				zin;

	/**
	 * currents session ASSISTANT_ID
	 */
	private String										ASSISTANT_ID				= null;
	private boolean										mClosed						= false;
	private String										mClosedReason				= null;
	private SiriClientActivity							callback					= null;
	private StartHandler								mInitHandler				= null;

	/**
	 * @param host
	 * @param port
	 */
	public SiriSessionImpl(String host, int port, String assitantId, SiriClientActivity callback, OnSiriSessionHandler closedHandler) {
		this(host, port, DEFAULT_USER_AGENT, assitantId, callback, closedHandler);
	}

	/**
	 * @param host
	 * @param port
	 * @param userAgent
	 */
	public SiriSessionImpl(String host, int port, String userAgent, String assistantId, SiriClientActivity callback, OnSiriSessionHandler closedHandler) {
		this.mSessionHandler = closedHandler;
		this.callback = callback;
		this.HOST = host;
		this.PORT = port;

		this.ASSISTANT_ID = assistantId;

		String X_HOST_UUID = UUID.randomUUID().toString();
		String xAceHost = ""; // "X-Ace-Host: " + X_HOST_UUID + CRLF
		this.httpRequestHeader = "ACE /ace HTTP/1.0" + CRLF + "Host: " + host + CRLF + "User-Agent: " + userAgent + CRLF + xAceHost + "Content-Length: 2000000000" + CRLF + CRLF;

		// instantiate ObjectCreator
		ObjectCreator.getInstance();
		try {
			setupSSLSocket();

			mResponseFactory = SiriResponse.factory.getInstance();
			mRequestFactory = SiriRequest.factory.getInstance();

			mReceiver = new Receiver();
			mReveicerThread = new Thread(mReceiver);

			mInitHandler = new StartHandler();

			// trigger init
			mInitHandler.obtainMessage(INIT_SEND_HTTP).sendToTarget();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
			close("Could not connect: " + e.getMessage());
		} catch (SocketException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
			close("Could not connect: " + e.getMessage());
		} catch (KeyManagementException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
			close("Could not connect: " + e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
			close("Could not connect: " + e.getMessage());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
			close("Could not connect: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
			close("Could not connect: " + e.getMessage());
		}
	}

	/**
	 * 
	 */
	private void initRemaining() {
		// start receiver thread
		if (mReveicerThread != null) {
			mReveicerThread.start();
		} else {
			mClosed = true;
			close();
		}

		if (mClosed) {
			return;
		}

		// start pinging
		if (PING_ENABLE_PERIODICALLY)
			mSendPeriodicPingsHandler.postDelayed(mSendPingTask, PING_DELAY_MILLIS);

		// request session info
		try {
			getSessionInfo();
		} catch (PropertyListException e) {
			e.printStackTrace();
			MyLog.w(DEBUG_TAG, e.getMessage());
		}
	}

	/**
	 * get session info and trigger assistant load
	 * 
	 * @throws PropertyListException
	 */
	private void getSessionInfo() throws PropertyListException {
		// GetSessionCertificate req = new
		// GetSessionCertificate(UUID.randomUUID().toString());

		CreateSessionInfoRequest req = new CreateSessionInfoRequest(UUID.randomUUID().toString(), null, null, 15, PhoneUUID.getUUID(callback));
		SiriRequest rawreq = mRequestFactory.create(req);
		rawreq.addOnSiriResponseListener(new OnSiriResponseListener() {
			public void handleResponse(SiriRequest req, SiriResponse res) {
				// setup the assistant
				try {
					if (!(res.getObject() instanceof CreateSessionInfoResponse))
						MyLog.i(DEBUG_TAG, "CreateSessionInfoResponse failed:" + res);
					setupAssistant();
				} catch (PropertyListException e) {
					e.printStackTrace();
					MyLog.w(DEBUG_TAG, e.getMessage());
				}
			}
		});
		addToSendQueue(rawreq);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSession#destroyAssistant()
	 */
	public boolean destroyAssistant() {
		if (ASSISTANT_ID == null)
			return false;

		try {
			DestroyAssistant req = new DestroyAssistant(UUID.randomUUID().toString(), null, ASSISTANT_ID);
			SiriRequest rawreq;
			rawreq = SiriRequest.factory.getInstance().create(req);
			addToSendQueue(rawreq);
			ASSISTANT_ID = null;
			return true;
		} catch (PropertyListException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * init assistant
	 * 
	 * @throws PropertyListException
	 * 
	 */
	private void setupAssistant() throws PropertyListException {
		if (ASSISTANT_ID != null) {
			setupAssistantLoad();
		} else {
			setupAssistantCreate();
		}
	}

	/**
	 * create new assistant and set it up
	 * 
	 * @throws PropertyListException
	 */
	private void setupAssistantCreate() throws PropertyListException {
		// create new one
		OnSiriResponseListener l = new OnSiriResponseListener() {
			public void handleResponse(SiriRequest req, SiriResponse res) {
				if (res.getObject() instanceof AssistantCreated) {
					ASSISTANT_ID = PropertyListUtils.getStringProperty(res.getPlist(), "assistantId");

					// update preferences
					callback.getPreferenceManager().setStringPref(R.string.session_assistant_id_key, ASSISTANT_ID);

					// set data
					String refId = PropertyListUtils.getAceId(res.getPlist());
					setAssistantData(refId);
					mSessionHandler.handleEstablished(SiriSessionImpl.this);
				} else {
					MyLog.w(DEBUG_TAG, "assistant NOT created.");
				}
			}
		};

		CreateAssistant req = new CreateAssistant(UUID.randomUUID().toString(), null);
		SiriRequest rawReq = SiriRequest.factory.getInstance().create(req);
		rawReq.addOnSiriResponseListener(l);
		addToSendQueue(rawReq);
	}

	/**
	 * Load assistant; if this was not done, create new.
	 * 
	 * @throws PropertyListException
	 */
	private void setupAssistantLoad() throws PropertyListException {
		// we have an assistant
		String speechId = UUID.randomUUID().toString();
		String assistantId = ASSISTANT_ID;
		LoadAssistant req = new LoadAssistant(UUID.randomUUID().toString(), assistantId, speechId, null);
		SiriRequest rawReq = SiriRequest.factory.getInstance().create(req);

		rawReq.addOnSiriResponseListener(new OnSiriResponseListener() {
			public void handleResponse(SiriRequest req, SiriResponse res) {
				PropertyListSerializable obj = res.getObject();
				if (obj instanceof CommandFailed || obj instanceof AssistantNotFound) {
					ASSISTANT_ID = null;
					try {
						setupAssistantCreate();
					} catch (PropertyListException e) {
						e.printStackTrace();
						MyLog.w(DEBUG_TAG, e.getMessage());
					}
				} else if (obj instanceof AssistantLoaded) {
					// set data
					String refId = PropertyListUtils.getAceId(res.getPlist());
					setAssistantData(refId);

					mSessionHandler.handleEstablished(SiriSessionImpl.this);
				} else {
					MyLog.i(DEBUG_TAG, "waiting for loadassistant answer; unexpected: " + res.toString());
				}
			}
		});

		addToSendQueue(rawReq);
	}

	/**
	 * read initial http responde and turn on compression
	 */
	private void initialHttpResponse() {
		try {
			readHeader();
		} catch (IOException e) {
			close("Could not connect: " + e.getMessage());
			MyLog.w(DEBUG_TAG, "initialHttpResponse failed: " + e.getMessage());
		}
	}

	/**
	 * @throws IOException
	 * @throws SiriProtocolException
	 */
	private void readACE2() throws IOException, SiriProtocolException {
		int aa = 0, cc = 0, ee = 0, O2 = 0;

		// read ace2
		aa = bin.read();
		cc = bin.read(); // cc
		ee = bin.read(); // ee
		O2 = bin.read(); // 02

		if (aa != 0xaa || cc != 0xcc || ee != 0xee || O2 != 0x02) {
			MyLog.w(getClass().getSimpleName(), "ACE2 expected");
		}
		MyLog.i(DEBUG_TAG, "ACE2 read");
		zin = new InflaterInputStream(bin);
	}

	/**
	 * send initial ACE/Http request and turn on compression
	 */
	private void initialHttpRequest() {
		try {
			bout.write(httpRequestHeader.getBytes(CHARSET));
		} catch (UnsupportedEncodingException e) {
			close("Could not connect: " + e.getMessage());
			MyLog.w(DEBUG_TAG, "initialHttpRequest failed: " + e.getMessage());
		} catch (IOException e) {
			close("Could not connect: " + e.getMessage());
			MyLog.w(DEBUG_TAG, "initialHttpRequest failed: " + e.getMessage());
		}
	}

	private void writeACE2() throws IOException {
		// write ACE2
		bout.write(0xaa);
		bout.write(0xcc);
		bout.write(0xee);
		bout.write(0x02);
		bout.flush();

		zout = new DeflaterOutputStream(bout);
		zout.setSyncFlush(true);
	}

	/**
	 * <code><pre>
	 *  +----------------------+
	 *  | ssl socket is set up |
	 *  +----------------------+
	 *            |
	 *            V
	 *  +-----------------------+       +------------------+
	 *  | initial HTTP Req sent |------>| initial ACE sent |
	 *  +-----------------------+       +------------------+
	 *                                           |
	 *                                           V
	 *  +---------------------------+   +----------------------------+
	 *  | initial HTTP ACE-Req sent |<--| initial HTTP RESPONSE recv |
	 *  +---------------------------+   +----------------------------+
	 *          |
	 *          V
	 *  +---------+
	 *  | done... |
	 *  +---------+
	 *  
	 *  </pre></code>
	 * 
	 * @author helge
	 * 
	 */
	private class StartHandler extends Handler {
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case INIT_SEND_HTTP:
						MyLog.i(DEBUG_TAG, "INIT_SEND_HTTP");
						initialHttpRequest();
						mInitHandler.obtainMessage(INIT_SEND_ACE2).sendToTarget();
						return;
					case INIT_SEND_ACE2:
						MyLog.i(DEBUG_TAG, "INIT_SEND_ACE2");
						writeACE2();
						mInitHandler.obtainMessage(INIT_RECV_HTTP).sendToTarget();
						return;
					case INIT_RECV_HTTP:
						MyLog.i(DEBUG_TAG, "INIT_RECV_HTTP");
						initialHttpResponse();
						mInitHandler.obtainMessage(INIT_RECV_ACE2).sendToTarget();
						return;
					case INIT_RECV_ACE2:
						MyLog.i(DEBUG_TAG, "INIT_RECV_ACE2 - skipped.");
						// readACE2();
						mInitHandler.obtainMessage(INIT_START_REMAINING).sendToTarget();
						return;
					case INIT_START_REMAINING:
						MyLog.i(DEBUG_TAG, "INIT_START_REMAINING");
						initRemaining();
						return;
					default:
				}
			} catch (SiriProtocolException e) {
				e.printStackTrace();
				MyLog.w(DEBUG_TAG, e.getMessage());
				close("Could not connect: " + e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
				MyLog.w(DEBUG_TAG, e.getMessage());
				close("Could not connect: " + e.getMessage());
			}
			super.handleMessage(msg);
		}

	}

	/**
	 * Receiver Runnable class
	 * 
	 * @author helge
	 * 
	 */
	private class Receiver extends Thread {
		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			try {
				readACE2();
			} catch (SiriProtocolException e) {
				MyLog.i(getClass().getSimpleName(), e.getMessage());
				close("Could not connect: " + e.getMessage());
			} catch (IOException e) {
				MyLog.i(getClass().getSimpleName(), e.getMessage());
				close("Could not connect: " + e.getMessage());
			}

			while (!mClosed) {
				try {
					byte cmd = (byte) zin.read();

					SiriResponse res = null;
					switch (CMD.getEnumForId(cmd)) {
						case PONG:
							// get value
							int pongval = readInteger(zin);
							res = mResponseFactory.createPong(pongval);
							break;
						case PLIST:
							// get size - 4byte
							int contentSize = readInteger(zin);

							// and get answer
							byte[] content = new byte[contentSize];
							zin.read(content);
							res = mResponseFactory.create(content);
							// MyLog.i(DEBUG_TAG, "RECEIVED: " +
							// res.toString());
							break;
						case PING:
							throw new SiriProtocolException("received PING");
						default:
							throw new SiriProtocolException("unknown command: " + cmd);
					}

					Bundle b = new Bundle();
					b.putSerializable(HANDLER_SEND_DATA_KEY, res);

					Message msg = mReceiveHandler.obtainMessage(cmd);
					msg.setData(b);
					msg.sendToTarget();
				} catch (EOFException e) {
					MyLog.w(DEBUG_TAG, "Receiver.run(): " + e.getMessage());
					close("Could not connect: " + e.getMessage());
				} catch (IOException e) {
					MyLog.w(DEBUG_TAG, "Receiver.run(): " + e.getMessage());
					close("Could not connect: " + e.getMessage());
					break;
				} catch (PropertyListException e) {
					MyLog.w(DEBUG_TAG, "Receiver.run(): " + e.getMessage());
					close("Could not connect: " + e.getMessage());
					break;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSession#close()
	 */
	public void close() {
		close("user closed");
	}

	/**
	 * @param reason
	 */
	public void close(String reason) {
		if (mClosed == true)
			return;

		try {
			mClosedReason = reason;

			mClosed = true;

			MyLog.w(DEBUG_TAG, "close()");
			mSendPeriodicPingsHandler.removeCallbacks(mSendPingTask);

			mReceiver = null;
			mReveicerThread = null;

			if (zout != null)
				zout.close();
			if (zin != null)
				zin.close();
			if (bout != null)
				bout.close();
			if (bin != null)
				bin.close();
			if (sslsocket != null)
				sslsocket.close();

			if (mSessionHandler != null)
				mSessionHandler.handleClosed(this);
		} catch (IOException e) {
			MyLog.w(DEBUG_TAG, "close() failed: " + e.getMessage());
		}
	}

	/**
	 * sets up the ssl socket and create buffered in- & ouputstream
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private void setupSSLSocket() throws NoSuchAlgorithmException, KeyManagementException, UnknownHostException, SocketException, IllegalArgumentException, IOException {
		// setup ssl context
		X509TrustManager tm = new X509TrustManager() {

			public void checkClientTrusted(X509Certificate[] xcs, String string) {
			}

			public void checkServerTrusted(X509Certificate[] xcs, String string) {
			}

			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};

		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(null, new TrustManager[] { tm }, null);

		// get new socket
		SSLSocketFactory factory = ctx.getSocketFactory();

		Socket socket = new Socket(HOST, PORT);
		sslsocket = factory.createSocket(socket, HOST, PORT, true);

		// create buffered streams
		bout = new BufferedOutputStream(sslsocket.getOutputStream());
		bin = new BufferedInputStream(sslsocket.getInputStream());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.helge.siri.siriclient.net.SiriSessionI#addToSendQueue(de.helge.siri
	 * .siriclient.net.SiriRequest)
	 */
	public void addToSendQueue(SiriRequest req) {
		Bundle b = new Bundle();
		b.putSerializable(HANDLER_SEND_DATA_KEY, req);
		Message msg = mSendHandler.obtainMessage(HANDLER_SEND_CMD);
		msg.setData(b);

		msg.sendToTarget();
	}

	/**
	 * read full http ACE header
	 * 
	 * @throws IOException
	 */
	private Map<String, String> readHeader() throws IOException {
		if (bin == null)
			return null;

		BufferedReader reader = new BufferedReader(new InputStreamReader(bin));
		Map<String, String> map = new HashMap<String, String>();

		String statusLine = reader.readLine();
		if (statusLine == null)
			return null;
		else
			map.put(HEADER_STATUSLINE_KEY, statusLine);

		String tmp;
		while ((tmp = reader.readLine()) != null) {
			// 2xCRLF...
			if (tmp.equals("")) {
				break;
			}

			int pos = tmp.indexOf(": ");
			String key = tmp.substring(0, pos);
			String value = tmp.substring(pos + 2);
			map.put(key, value);
		}

		return map;
	}

	/**
	 * write integer to outputstrem in bigendian format
	 * 
	 * @param out
	 * @param val
	 * @throws IOException
	 */
	private void writeInteger(OutputStream out, int val) throws IOException {
		for (int i = 3; i >= 0; i--) {
			byte tmp = (byte) ((val >> i * 8) & 0xFF);
			out.write(tmp);
		}
	}

	/**
	 * read integer from inputstream in bigendian format
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	private int readInteger(InputStream in) throws IOException {
		int retval = 0;
		for (int i = 3; i >= 0; i--) {
			retval |= (((byte) in.read()) & 0xFF) << i * 8;
		}
		return retval;
	}

	/**
	 * send a new command aka write it to the stream, should not be used
	 * directly - use {@link SiriSessionImpl#addToSendQueue(SiriRequest)}
	 * instead
	 * 
	 * @param req
	 */
	private void sendCommand(SiriRequest req) {
		if (mClosed)
			return;

		try {
			CMD cmd = req.getCmd();

			switch (cmd) {
				case PING:
					// command id
					zout.write(req.getCmd().getId());
					// ping val
					writeInteger(zout, req.getPing());

					// MyLog.i(DEBUG_TAG, req.toString());
					break;
				case PLIST:
					addToRequests(req);

					// command id
					zout.write(req.getCmd().getId());

					// write length to server
					writeInteger(zout, req.getPlistBytes().length);
					// write contents
					zout.write(req.getPlistBytes());
					MyLog.i(DEBUG_TAG, req.toString());
					break;
				case PONG:
					throw new SiriProtocolException("trying to send PONG?!");
				default:
					throw new SiriProtocolException("unknown command: " + cmd);
			}

			zout.flush();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			close();
			MyLog.w(DEBUG_TAG, e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			close();
			MyLog.w(DEBUG_TAG, e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			close();
			String msg = e.getMessage();
			MyLog.w(DEBUG_TAG, (msg != null) ? msg : "");
		}
	}

	/**
	 * add a new andwer to the list; informs possible listeners
	 * 
	 * @param res
	 */
	private void addAnswer(SiriResponse res) {
		String refId = PropertyListUtils.getRefId(res.getPlist());

		if (mRequests.containsKey(refId)) {
			SiriRequest r = mRequests.get(refId);
			r.notifyListeners(res);
		} else {
			MyLog.i(DEBUG_TAG, "unreferenced response: " + res.toString());
		}

		if (mResponses.containsKey(refId)) {
			mResponses.get(refId).add(res);
		} else {
			List<SiriResponse> list = new ArrayList<SiriResponse>();
			list.add(res);
			mResponses.put(refId, list);
		}
	}

	/**
	 * add a new request to the list
	 * 
	 * @param req
	 */
	private void addToRequests(SiriRequest req) {
		String aceId = PropertyListUtils.getAceId(req.getPlist());
		mRequests.put(aceId, req);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSessionI#getRequests()
	 */
	public Map<String, SiriRequest> getRequests() {
		return mRequests;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSessionI#getResponses()
	 */
	public Map<String, List<SiriResponse>> getResponses() {
		return mResponses;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSessionI#getmResponseFactory()
	 */
	public SiriResponse.factory getmResponseFactory() {
		return mResponseFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSessionI#getmRequestFactory()
	 */
	public SiriRequest.factory getmRequestFactory() {
		return mRequestFactory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSession#getAssistantId()
	 */
	public String getAssistantId() {
		return ASSISTANT_ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSession#isClosed()
	 */
	public boolean isClosed() {
		return mClosed;
	}

	/**
	 * @param refId
	 */
	private void setAssistantData(String refId) {
		MyPreferencesManager mgr = new MyPreferencesManager(callback);
		String forename = mgr.getStringPref(R.string.preferences_userinfo_forename_key, R.string.preferences_userinfo_forename_default);
		String lastname = mgr.getStringPref(R.string.preferences_userinfo_lastname_key, R.string.preferences_userinfo_lastname_default);
		String nickname = mgr.getStringPref(R.string.preferences_userinfo_nickname_key, R.string.preferences_userinfo_nickname_default);
		String lang = mgr.getStringPref(R.string.preferences_language_lang_key, R.string.preferences_language_lang_default);
		String locale = lang.replace("-", "_");

		Person me = new Person("id", null, null, null, null, nickname, null, true, forename + " " + lastname, lastname, "", null, forename, null, null, null, null);
		List<Person> meList = new ArrayList<Person>();
		meList.add(me);

		SetAssistantData req2 = new SetAssistantData(UUID.randomUUID().toString(), refId, false, "Europe/Berlin", lang, locale, meList, null);
		try {
			addToSendQueue(SiriRequest.factory.getInstance().create(req2));
		} catch (PropertyListException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSession#getServerAddress()
	 */
	public String getServerAddress() {
		return HOST + ":" + PORT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.helge.siri.siriclient.net.SiriSession#getClosedReason()
	 */
	public String getClosedReason() {
		return mClosedReason;
	}

}
