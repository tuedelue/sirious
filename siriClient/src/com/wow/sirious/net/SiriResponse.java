package com.wow.sirious.net;

import com.dd.plist.PropertyListException;

/**
 * siri response message
 * 
 * @author helge
 * 
 */
public class SiriResponse extends SiriMessage {
	private static final long	serialVersionUID	= 2435549010355028014L;
	/**
	 * pong sequence number
	 */
	private int					pingpong;

	/**
	 * @param pingpong
	 *            pong seq
	 */
	private SiriResponse(int pingpong) {
		this.cmd = CMD.PONG;
		this.pingpong = pingpong;
	}

	/**
	 * @param plist
	 * @throws PropertyListException
	 */
	private SiriResponse(byte[] plist) throws PropertyListException {
		this.cmd = CMD.PLIST;
		this.pingpong = -1;
		setObjectFromPlistBytes(plist);
	}

	/**
	 * message facotry
	 * 
	 * @author helge
	 * 
	 */
	public static class factory {
		/**
		 * create pong
		 * 
		 * @param num
		 * @return
		 */
		public SiriResponse createPong(int num) {
			return new SiriResponse(num);
		}

		/**
		 * create response from binary
		 * 
		 * @param plist
		 * @return response
		 * @throws PropertyListException
		 */
		public SiriResponse create(byte[] plist) throws PropertyListException {
			return new SiriResponse(plist);
		}

		/**
		 * get factory instance
		 * 
		 * @return responsefactory
		 */
		public static factory getInstance() {
			return new factory();
		}
	}

	public String toString() {
		switch (this.cmd) {
			case PONG:
				return "(" + getTimestampFormatted() + ") PONG seq=" + pingpong;
			case PLIST:
				String name = null;
				if (getObject() != null) {
					name = getObject().getClass().getSimpleName();
				}
				return "(" + getTimestampFormatted() + ") RES " + name + " PLIST=" + getPlist() + "";
			default:
				throw new NullPointerException("wrong cmd: " + cmd);
		}
	}
}
