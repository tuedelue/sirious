package com.wow.siriobjects.email;

import java.util.Date;

import com.wow.siriobjects.ClientBoundCommand;


public class EmailSearch extends ClientBoundCommand {
	String toEmail;
	String timeZoneId;
	String subject;
	int status;
	Date startDate;
	String fromEmail;
	Date endDate;
	String targetAppId;

	public EmailSearch(String refId, String toEmail, String timeZoneId,
			String subject, int status, Date startDate, String fromEmail,
			Date endDate, String targetAppId) {
		super("Search", "com.apple.ace.email", null, refId, null);
		this.toEmail = toEmail;
		this.timeZoneId = timeZoneId;
		this.subject = subject;
		this.status = status;
		this.startDate = startDate;
		this.fromEmail = fromEmail;
		this.endDate = endDate;
		this.targetAppId = targetAppId;
	}

}
