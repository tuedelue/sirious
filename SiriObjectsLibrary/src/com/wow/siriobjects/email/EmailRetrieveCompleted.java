package com.wow.siriobjects.email;

import java.util.List;

import com.wow.siriobjects.ServerBoundCommand;


public class EmailRetrieveCompleted extends ServerBoundCommand {
	List<EmailEmail> results;

	public EmailRetrieveCompleted(String refId, List<EmailEmail> results) {
		super(null, "com.apple.ace.email", null, refId);
		this.results = results;
	}

}
