package com.wow.siriobjects.email;

import java.util.Date;
import java.util.List;

import com.wow.siriobjects.system.DomainObject;
import com.wow.siriobjects.system.Person;


public class EmailEmail extends DomainObject {
	String			type;
	String			timeZoneId;
	String			subject;
	String			referenceId;
	List<String>	recipientsTo;
	List<String>	recipientsCc;
	List<String>	recipientsBcc;
	List<String>	receivingAddresses;
	int				outgoing;
	String			message;
	Person			fromEmail;
	Date			dateSent;

	public EmailEmail(String type, String timeZoneId, String subject, String referenceId, List<String> recipientsTo, List<String> recipientsCc, List<String> recipientsBcc,
			List<String> receivingAddresses, int outgoing, String message, Person fromEmail, Date dateSent) {
		super(null, "com.apple.ace.email", null);
		this.type = type;
		this.timeZoneId = timeZoneId;
		this.subject = subject;
		this.referenceId = referenceId;
		this.recipientsTo = recipientsTo;
		this.recipientsCc = recipientsCc;
		this.recipientsBcc = recipientsBcc;
		this.receivingAddresses = receivingAddresses;
		this.outgoing = outgoing;
		this.message = message;
		this.fromEmail = fromEmail;
		this.dateSent = dateSent;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
