package com.wow.siriobjects.email;

import java.util.List;

import com.wow.siriobjects.ui.Snippet;


public class EmailSnippet extends Snippet {
	List<EmailEmail>	emails;

	public EmailSnippet(List<EmailEmail> emails) {
		super("com.apple.ace.email", null, null, false, null, null);
		this.emails = emails;
	}

}
