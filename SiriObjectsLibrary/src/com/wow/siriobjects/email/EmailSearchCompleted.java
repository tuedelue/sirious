package com.wow.siriobjects.email;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.ServerBoundCommand;


public class EmailSearchCompleted extends ServerBoundCommand {
	List<EmailEmail> results;

	public EmailSearchCompleted(String refId, List<EmailEmail> results) {
		super("SearchCompleted", "com.apple.ace.email", null, refId);
		this.results = (results != null) ? results
				: new ArrayList<EmailEmail>();
	}
}
