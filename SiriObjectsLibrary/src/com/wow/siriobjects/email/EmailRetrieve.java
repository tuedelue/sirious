package com.wow.siriobjects.email;

import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;


public class EmailRetrieve extends ClientBoundCommand {
	List<String> requestHeaders;
	List<String> identifiers;
	String targetAppId;

	public EmailRetrieve(String refId, List<String> requestHeaders,
			List<String> identifiers, String targetAppId) {
		super("Retrieve", "com.apple.ace.email", null, refId, null);
		this.requestHeaders = requestHeaders;
		this.identifiers = identifiers;
		this.targetAppId = targetAppId;
	}

}
