package com.wow.siriobjects.email;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.DomainObject;


public class Email extends DomainObject {
	private static final long	serialVersionUID	= -7244521395979611507L;
	public static final String	clazz				= "Email";
	public static final String	group				= "com.apple.ace.email";

	String						label;
	String						favoriteFacetime;
	String						emailAddress;

	public Email() {
		super(clazz, group);

	}

	public Email(String identifier, String label, String favoriteFacetime, String emailAddress) {
		super(clazz, group, identifier);
		this.label = label;
		this.favoriteFacetime = favoriteFacetime;
		this.emailAddress = emailAddress;
	}

	public NSDictionary toPlist() {
		if (label != null)
			properties.put("label", new NSString(label));
		if (favoriteFacetime != null)
			properties.put("favoriteFacetime", new NSString(favoriteFacetime));
		if (emailAddress != null)
			properties.put("emailAddress", new NSString(emailAddress));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static Email fromPlist(NSDictionary plist) {
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");
		String label = PropertyListUtils.getStringProperty(plist, "label");
		String favoriteFacetime = PropertyListUtils.getStringProperty(plist, "favoriteFacetime");
		String emailAddress = PropertyListUtils.getStringProperty(plist, "emailAddress");

		return new Email(identifier, label, favoriteFacetime, emailAddress);
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the favoriteFacetime
	 */
	public String getFavoriteFacetime() {
		return favoriteFacetime;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
