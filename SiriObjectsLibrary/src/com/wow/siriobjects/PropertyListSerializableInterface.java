package com.wow.siriobjects;

import java.io.Serializable;

import com.dd.plist.NSDictionary;

public interface PropertyListSerializableInterface extends Serializable {
	public NSDictionary toPlist();
}
