package com.wow.siriobjects;

import java.util.HashMap;
import java.util.Map;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;

public abstract class AceObject extends PropertyListSerializable implements PropertyListSerializableInterface {
	private static final long		serialVersionUID	= 4542702486077676413L;
	String							clazz;
	String							group;
	public Map<String, NSObject>	properties			= new HashMap<String, NSObject>();
	public Map<String, NSObject>	plist				= new HashMap<String, NSObject>();

	protected AceObject() {
	}

	public AceObject(String clazz, String group) {
		super();
		this.clazz = clazz;
		this.group = group;
	}

	public NSDictionary toPlist() {
		plist.put("class", new NSString(clazz));
		plist.put("group", new NSString(group));

		NSDictionary outplist = new NSDictionary();
		outplist.putAll(plist);

		if (properties.size() > 0) {
			NSDictionary properties = new NSDictionary();
			properties.putAll(this.properties);
			outplist.put("properties", properties);
		}

		return outplist;
	}

	public String toString() {
		return "class=" + clazz + " groupId=" + group + " properties=" + properties;
	}

	protected void setClazzAndGroup(String clazz, String group) {
		this.clazz = clazz;
		this.group = group;
	}
}
