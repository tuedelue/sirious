package com.wow.siriobjects.phone;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.PersonAttribute;


public class Call extends ClientBoundCommand {
	public static final String	clazz	= "Call";
	public static final String	group	= "com.apple.ace.phone";

	boolean						faceTime;
	PersonAttribute				callRecipient;
	String						targetAppId;
	String						recipient;

	public Call() {

	}

	public Call(String aceId, String refId, List<ClientBoundCommand> callbacks, boolean faceTime, PersonAttribute callRecipient, String targetAppId, String recipient) {
		super(clazz, group, aceId, refId, callbacks);
		this.faceTime = faceTime;
		this.callRecipient = callRecipient;
		this.targetAppId = targetAppId;
		this.recipient = recipient;
	}

	public NSDictionary toPlist() {
		properties.put("faceTime", new NSNumber(faceTime));
		if (callRecipient != null)
			properties.put("callRecipient", callRecipient.toPlist());
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));
		if (recipient != null)
			properties.put("recipient", new NSString(recipient));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static Call fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		boolean faceTime = PropertyListUtils.getBooleanProperty(plist, "faceTime");
		String recipient = PropertyListUtils.getStringProperty(plist, "recipient");
		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		PersonAttribute callRecipient = PropertyListUtils.getObject(plist, "callRecipient");

		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);
		return new Call(aceId, refId, callbacks, faceTime, callRecipient, targetAppId, recipient);
	}

	/**
	 * @return the faceTime
	 */
	public boolean isFaceTime() {
		return faceTime;
	}

	/**
	 * @return the callRecipient
	 */
	public PersonAttribute getCallRecipient() {
		return callRecipient;
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

	/**
	 * @return the recipient
	 */
	public String getRecipient() {
		return recipient;
	}

}
