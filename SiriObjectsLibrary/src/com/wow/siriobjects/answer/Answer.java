package com.wow.siriobjects.answer;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class Answer extends AceObject {
	public static final String	clazz	= "Object";
	public static final String	group	= "com.apple.ace.answer";

	String						title;
	List<AnswerLine>		lines;

	public Answer() {

	}

	public Answer(String title, List<AnswerLine> lines) {
		super(clazz, group);
		this.title = title;
		this.lines = (lines != null) ? lines : new ArrayList<AnswerLine>();
	}

	public static Answer fromPlist(NSDictionary plist) {
		String title = PropertyListUtils.getStringProperty(plist, "title");
		List<AnswerLine> lines = PropertyListUtils.getListObjects(plist, "lines");

		return new Answer(title, lines);
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the lines
	 */
	public List<AnswerLine> getLines() {
		return lines;
	}

	public NSDictionary toPlist() {
		NSArray lines = PropertyListUtils.convertPlistSerializableListToNSArray(this.lines);
		if (lines != null)
			properties.put("lines", lines);
		if (title != null)
			properties.put("title", new NSString(title));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
