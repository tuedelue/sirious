package com.wow.siriobjects.answer;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.siriobjects.ui.Snippet;


public class AnswerSnippet extends Snippet {
	public static final String	clazz	= "Snippet";
	public static final String	group	= "com.apple.ace.answer";
	List<Answer>			answers;
	ConfirmationOptions			confirmationOptions;

	public AnswerSnippet() {
	}

	public AnswerSnippet(List<Answer> answers, ConfirmationOptions confirmationOptions) {
		super(group, null, null, false, null, confirmationOptions);
		this.answers = (answers != null) ? answers : new ArrayList<Answer>();
	}

	public static AnswerSnippet fromPlist(NSDictionary plist) {
		List<Answer> answers = PropertyListUtils.getListObjects(plist, "answers");

		return new AnswerSnippet(answers, null);
	}

	public NSDictionary toPlist() {
		if (confirmationOptions != null)
			properties.put("confirmationOptions", null);
		if (answers != null)
			properties.put("answers", PropertyListUtils.convertPlistSerializableListToNSArray(answers));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the answers
	 */
	public List<Answer> getAnswers() {
		return answers;
	}

	/**
	 * @return the confirmationOptions
	 */
	public ConfirmationOptions getConfirmationOptions() {
		return confirmationOptions;
	}

}
