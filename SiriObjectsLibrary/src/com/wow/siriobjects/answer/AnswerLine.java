package com.wow.siriobjects.answer;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class AnswerLine extends AceObject {
	public static final String	clazz	= "ObjectLine";
	public static final String	group	= "com.apple.ace.answer";

	String						text;
	String						image;

	public AnswerLine() {

	}

	public AnswerLine(String text, String image) {
		super(clazz, group);
		this.text = text;
		this.image = image;
	}

	public static AnswerLine fromPlist(NSDictionary plist) {
		String text = PropertyListUtils.getStringProperty(plist, "text");
		String image = PropertyListUtils.getStringProperty(plist, "image");

		return new AnswerLine(text, image);
	}

	public NSDictionary toPlist() {
		if (image != null)
			properties.put("image", new NSString(image));
		if (text != null)
			properties.put("text", new NSString(text));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

}
