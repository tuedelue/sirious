package com.wow.siriobjects;

import java.util.List;
import java.util.UUID;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;

public abstract class ClientBoundCommand extends AceObject {
	String						aceId;
	String						refId;
	List<ClientBoundCommand>	callbacks;

	protected ClientBoundCommand() {
	}

	public ClientBoundCommand(String encodedClassName, String groupId, String aceId, String refId, List<ClientBoundCommand> callbacks) {
		super(encodedClassName, groupId);
		this.aceId = (aceId != null) ? aceId : UUID.randomUUID().toString();
		this.refId = (refId != null) ? refId : null;
		this.callbacks = callbacks;
	}

	public NSDictionary toPlist() {
		if (aceId != null)
			plist.put("aceId", new NSString(aceId));
		if (refId != null)
			plist.put("refId", new NSString(refId));

		if (callbacks != null) {
			NSArray callbacks = new NSArray(this.callbacks.size());
			int i = 0;
			for (PropertyListSerializable o : this.callbacks) {
				callbacks.setValue(i++, o.toPlist());
			}

			properties.put("callbacks", callbacks);
		}
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString() + " aceId=" + aceId + " refId=" + refId;
	}

	/**
	 * @return the callbacks
	 */
	public List<ClientBoundCommand> getCallbacks() {
		return callbacks;
	}

	/**
	 * @return the aceId
	 */
	public String getAceId() {
		return aceId;
	}

	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}

}
