package com.wow.siriobjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.util.Log;

import com.dd.plist.NSArray;
import com.dd.plist.NSData;
import com.dd.plist.NSDate;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;

public class PropertyListUtils {
	public static NSArray convertStringListToNSArray(List<String> l) {
		List<NSObject> nsstringlist = new ArrayList<NSObject>();

		for (String s : l) {
			nsstringlist.add(new NSString(s));
		}

		return new NSArray((NSObject[]) nsstringlist.toArray(new NSObject[nsstringlist.size()]));
	}

	public static List<ClientBoundCommand> getCallbacks(NSDictionary plist) {
		NSArray callbacks = (NSArray) PropertyListUtils.getNSObjectProperty(plist, "callbacks");
		List<ClientBoundCommand> clearCallbacks = null;
		if (callbacks != null) {
			clearCallbacks = new ArrayList<ClientBoundCommand>();
			for (NSObject o : callbacks) {
				NSDictionary callback = (NSDictionary) o;
				ClientBoundCommand clearCallback = (ClientBoundCommand) ObjectCreator.getInstance().createInstance(callback);
				if (clearCallback != null)
					clearCallbacks.add(clearCallback);
			}
		}

		return clearCallbacks;
	}

	public static <T extends PropertyListSerializable> List<T> getListObjects(NSDictionary plist, String key) {
		NSArray nsarray = (NSArray) PropertyListUtils.getNSObjectProperty(plist, key);
		List<T> clearList = null;
		if (nsarray != null) {
			clearList = new ArrayList<T>();
			for (NSObject o : nsarray) {
				if (!(o instanceof NSDictionary)) {
					Log.w("PropertyListUtils: getListObjects", "nondict:" + o.toString());
					continue;
				}
				NSDictionary obj = (NSDictionary) o;
				T clearObj = (T) ObjectCreator.getInstance().createInstance(obj);
				if (clearObj != null)
					clearList.add(clearObj);
			}
		}

		return clearList;
	}

	public static NSArray convertPlistSerializableListToNSArray(List l) {
		List<NSObject> nsobjlist = new ArrayList<NSObject>();

		for (Object o : l) {
			nsobjlist.add(((PropertyListSerializable) o).toPlist());
		}

		return new NSArray((NSObject[]) nsobjlist.toArray(new NSObject[nsobjlist.size()]));
	}

	public static List<String> convertToStringList(NSArray a) {
		List<String> stringlist = new ArrayList<String>();

		for (NSObject o : a.getArray()) {
			stringlist.add(((NSString) o).toString());
		}

		return stringlist;
	}

	public static String getRefId(NSDictionary plist) {
		NSString str = (NSString) plist.objectForKey("refId");
		return (str != null) ? str.toString() : null;
	}

	public static String getAceId(NSDictionary plist) {
		NSString str = (NSString) plist.objectForKey("aceId");
		return (str != null) ? str.toString() : null;
	}

	public static String getStringProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSString obj = (NSString) properties.objectForKey(key);

			if (obj != null) {
				return obj.toString();
			}
		}

		return null;
	}

	public static int getIntProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSObject obj = properties.objectForKey(key);

			if (obj instanceof NSString)
				return getIntFromNSStringFallBack((NSString) obj);

			if (obj == null || !(obj instanceof NSNumber))
				return 0;

			NSNumber num = (NSNumber) obj;
			return num.intValue();
		}

		return 0;
	}

	public static NSObject getNSObjectProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSObject obj = properties.objectForKey(key);

			if (obj != null) {
				return obj;
			}
		}

		return null;
	}

	public static List<String> getStringList(NSDictionary plist, String key) {
		NSArray arr = (NSArray) getNSObjectProperty(plist, key);
		if (arr == null)
			return null;

		List<String> list = new ArrayList<String>();
		for (NSObject o : arr.getArray()) {
			list.add(((NSString) o).toString());
		}
		return list;
	}

	public static long getLongProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSObject obj = properties.objectForKey(key);

			if (obj == null)
				return 0;

			if (obj instanceof NSString)
				return getLongFromNSStringFallBack((NSString) obj);

			if (obj instanceof NSNumber)
				return ((NSNumber) obj).longValue();
		}

		return 0l;
	}

	public static boolean getBooleanProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSObject obj = properties.objectForKey(key);

			if (obj == null)
				return false;

			if (obj instanceof NSString)
				return getBooleanFromNSStringFallBack((NSString) obj);

			// this may be a number or a string!
			if (obj instanceof NSNumber)
				return ((NSNumber) obj).boolValue();
		}

		return false;
	}

	public static Double getDoubleProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSObject obj = properties.objectForKey(key);

			if (obj instanceof NSString)
				return getDoubleFromNSStringFallBack((NSString) obj);

			if (obj == null || !(obj instanceof NSNumber))
				return 0d;

			NSNumber num = (NSNumber) obj;
			return num.doubleValue();
		}

		return 0d;
	}

	public static Date getDateProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSObject obj = properties.objectForKey(key);

			if (obj == null || !(obj instanceof NSDate))
				return null;

			NSDate date = (NSDate) obj;
			return date.getDate();
		}

		return null;
	}

	public static <T extends PropertyListSerializable> T getObject(NSDictionary plist, String key) {
		NSObject o = (NSObject) PropertyListUtils.getNSObjectProperty(plist, key);
		if (o == null)
			return null;

		if (!(o instanceof NSDictionary)) {
			Log.w("PropertyListUtils: getListObjects", "nondict:" + o.toString());
			return null;
		}
		NSDictionary obj = (NSDictionary) o;
		T clearObj = (T) ObjectCreator.getInstance().createInstance(obj);
		return clearObj;
	}

	public static byte[] getDataProperty(NSDictionary plist, String key) {
		NSDictionary properties = (NSDictionary) plist.objectForKey("properties");
		if (properties != null) {
			NSData obj = (NSData) properties.objectForKey(key);

			if (obj != null) {
				return obj.bytes();
			}
		}

		return null;
	}

	private static int getIntFromNSStringFallBack(NSString s) {
		try {
			return Integer.parseInt(s.toString());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	private static long getLongFromNSStringFallBack(NSString s) {
		try {
			return Long.parseLong(s.toString());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	private static float getFloatFromNSStringFallBack(NSString s) {
		try {
			return Float.parseFloat(s.toString());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	private static boolean getBooleanFromNSStringFallBack(NSString s) {
		try {
			return Boolean.parseBoolean(s.toString());
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static double getDoubleFromNSStringFallBack(NSString s) {
		try {
			return Double.parseDouble(s.toString());
		} catch (NumberFormatException e) {
			return 0;
		}
	}

}
