package com.wow.siriobjects.ui;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;


public class AssistantUtteranceView extends AceView {
	public static final String	clazz				= "AssistantUtteranceView";
	public static final String	group				= "com.apple.ace.assistant";

	String						text;
	String						dialogIdentifier	= "Misc#ident";

	public AssistantUtteranceView() {
	}

	public AssistantUtteranceView(String viewId, String text, String speakableText, String dialogIdentifier, boolean listenAfterSpeaking) {
		super(null, speakableText, listenAfterSpeaking);
		this.text = text;
		this.dialogIdentifier = dialogIdentifier;
	}

	public NSDictionary toPlist() {
		if (text != null)
			properties.put("text", new NSString(text));
		if (dialogIdentifier != null)
			properties.put("dialogIdentifier", new NSString(dialogIdentifier));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static AssistantUtteranceView fromPlist(NSDictionary plist) {
		String text = PropertyListUtils.getStringProperty(plist, "text");
		String speakableText = PropertyListUtils.getStringProperty(plist, "speakableText");
		String dialogIdentifier = PropertyListUtils.getStringProperty(plist, "dialogIdentifier");
		String viewId = PropertyListUtils.getStringProperty(plist, "viewId");
		boolean listenAfterSpeaking = PropertyListUtils.getBooleanProperty(plist, "listenAfterSpeaking");

		return new AssistantUtteranceView(viewId, text, speakableText, dialogIdentifier, listenAfterSpeaking);
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the dialogIdentifier
	 */
	public String getDialogIdentifier() {
		return dialogIdentifier;
	}

	/**
	 * @return the listenAfterSpeaking
	 */
	public boolean isListenAfterSpeaking() {
		return listenAfterSpeaking;
	}

	/**
	 * @return the speakableText
	 */
	public String getSpeakableText() {
		return speakableText;
	}

}
