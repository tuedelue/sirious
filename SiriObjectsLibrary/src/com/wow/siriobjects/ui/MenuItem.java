package com.wow.siriobjects.ui;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.AceObject;


public class MenuItem extends AceObject {
	String title = "";
	String subtitle = "";
	String ref = "";
	String icon = "";
	List<Object> commands;

	public MenuItem(String title, String subtitle, String ref, String icon,
			List<Object> commands) {
		super("MenuItem", "com.apple.ace.assistant");
		this.title = title;
		this.subtitle = subtitle;
		this.ref = ref;
		this.icon = icon;
		this.commands = (commands != null) ? commands : new ArrayList<Object>();
	}

}
