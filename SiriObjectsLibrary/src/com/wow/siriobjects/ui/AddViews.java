package com.wow.siriobjects.ui;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListSerializable;
import com.wow.siriobjects.PropertyListUtils;


public class AddViews extends ClientBoundCommand {
	public static final String	clazz		= "AddViews";
	public static final String	group		= "com.apple.ace.assistant";

	boolean						scrollToTop	= false;
	boolean						temporary	= false;
	String						dialogPhase	= "Completion";
	List<ViewObjectInterface>	views;

	public AddViews() {
	}

	public AddViews(String aceId, String refId, List<ClientBoundCommand> callbacks, boolean scrollToTop, boolean temporary, String dialogPhase, List<ViewObjectInterface> views) {
		super(clazz, group, aceId, refId, callbacks);
		this.scrollToTop = scrollToTop;
		this.temporary = temporary;
		this.dialogPhase = dialogPhase;
		this.views = (views != null) ? views : new ArrayList<ViewObjectInterface>();
	}

	public NSDictionary toPlist() {
		if (dialogPhase != null)
			properties.put("dialogphase", new NSString(dialogPhase));
		properties.put("scrollToTop", new NSNumber(scrollToTop));
		properties.put("temporary", new NSNumber(temporary));
		if (views != null)
			properties.put("views", PropertyListUtils.convertPlistSerializableListToNSArray(views));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static AddViews fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> clearCallbacks = PropertyListUtils.getCallbacks(plist);

		boolean scrollToTop = PropertyListUtils.getBooleanProperty(plist, "scrollToTop");
		boolean temporary = PropertyListUtils.getBooleanProperty(plist, "temporary");
		String dialogPhase = PropertyListUtils.getStringProperty(plist, "dialogPhase");

		List<PropertyListSerializable> clearViewsTmp = PropertyListUtils.getListObjects(plist, "views");
		List<ViewObjectInterface> clearViews = null;
		if (clearViewsTmp != null) {
			clearViews = new ArrayList<ViewObjectInterface>();
			for (PropertyListSerializable o : clearViewsTmp) {
				clearViews.add((ViewObjectInterface) o);
			}
		}

		return new AddViews(aceId, refId, clearCallbacks, scrollToTop, temporary, dialogPhase, clearViews);
	}

	/**
	 * @return the scrollToTop
	 */
	public boolean isScrollToTop() {
		return scrollToTop;
	}

	/**
	 * @return the temporary
	 */
	public boolean isTemporary() {
		return temporary;
	}

	/**
	 * @return the dialogPhase
	 */
	public String getDialogPhase() {
		return dialogPhase;
	}

	/**
	 * @return the views
	 */
	public List<ViewObjectInterface> getViews() {
		return views;
	}

}
