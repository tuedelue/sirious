package com.wow.siriobjects.ui;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.SendCommands;


public class Button extends AceObject implements ViewObjectInterface {
	public static final String	clazz	= "Button";
	public static final String	group	= "com.apple.ace.assistant";

	String						text;
	List<SendCommands>			commands;

	public Button() {
	}

	public Button(String text, List<SendCommands> commands) {
		super(clazz, group);
		this.text = text;
		this.commands = (commands != null) ? commands : new ArrayList<SendCommands>();
	}

	public NSDictionary toPlist() {
		if (text != null)
			properties.put("text", new NSString(text));
		if (commands != null)
			properties.put("commands", PropertyListUtils.convertPlistSerializableListToNSArray(commands));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static Button fromPlist(NSDictionary plist) {
		String text = PropertyListUtils.getStringProperty(plist, "text");

		NSArray commands = (NSArray) PropertyListUtils.getNSObjectProperty(plist, "commands");
		List<SendCommands> clearCommands = null;
		if (commands != null) {
			clearCommands = new ArrayList<SendCommands>();
			for (NSObject o : commands) {
				NSDictionary command = (NSDictionary) o;
				SendCommands clearCommand = SendCommands.fromPlist(command);
				clearCommands.add(clearCommand);
			}
		}

		return new Button(text, clearCommands);
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the commands
	 */
	public List<SendCommands> getCommands() {
		return commands;
	}

}
