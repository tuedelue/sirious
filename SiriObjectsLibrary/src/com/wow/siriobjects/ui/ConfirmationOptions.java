package com.wow.siriobjects.ui;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class ConfirmationOptions extends AceObject {
	private static final long	serialVersionUID	= -8417523674612934976L;
	public static final String	clazz				= "ConfirmationOptions";
	public static final String	group				= "com.apple.ace.assistant";

	List<ClientBoundCommand>	denyCommands;
	List<ClientBoundCommand>	submitCommands;
	String						confirmText			= "Confirm";
	String						denyText			= "Cancel";
	List<ClientBoundCommand>	cancelCommands;
	String						cancelLabel			= "Cancel";
	String						submitLabel			= "Confirm";
	List<ClientBoundCommand>	confirmCommands;
	String						cancelTrigger		= "Deny";

	public ConfirmationOptions() {
		super(clazz, group);
	}

	public ConfirmationOptions(List<ClientBoundCommand> denyCommands, List<ClientBoundCommand> submitCommands, String confirmText, String denyText, List<ClientBoundCommand> cancelCommands,
			String cancelLabel, String submitLabel, List<ClientBoundCommand> confirmCommands, String cancelTrigger) {
		super(clazz, group);
		this.denyCommands = denyCommands;
		this.submitCommands = submitCommands;
		this.confirmText = confirmText;
		this.denyText = denyText;
		this.cancelCommands = cancelCommands;
		this.cancelLabel = cancelLabel;
		this.submitLabel = submitLabel;
		this.confirmCommands = confirmCommands;
		this.cancelTrigger = cancelTrigger;
	}

	public NSDictionary toPlist() {
		properties.put("cancelLabel", new NSString(cancelLabel));
		properties.put("cancelTrigger", new NSString(cancelTrigger));
		properties.put("confirmText", new NSString(confirmText));
		properties.put("denyText", new NSString(denyText));
		properties.put("submitLabel", new NSString(submitLabel));

		properties.put("cancelCommands", PropertyListUtils.convertPlistSerializableListToNSArray(cancelCommands));
		properties.put("confirmCommands", PropertyListUtils.convertPlistSerializableListToNSArray(confirmCommands));
		properties.put("denyCommands", PropertyListUtils.convertPlistSerializableListToNSArray(denyCommands));
		properties.put("submitCommands", PropertyListUtils.convertPlistSerializableListToNSArray(submitCommands));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static ConfirmationOptions fromPlist(NSDictionary plist) {
		List<ClientBoundCommand> cancelCommands = PropertyListUtils.getListObjects(plist, "cancelCommands");
		List<ClientBoundCommand> confirmCommands = PropertyListUtils.getListObjects(plist, "confirmCommands");
		List<ClientBoundCommand> denyCommands = PropertyListUtils.getListObjects(plist, "denyCommands");
		List<ClientBoundCommand> submitCommands = PropertyListUtils.getListObjects(plist, "submitCommands");
		String cancelLabel = PropertyListUtils.getStringProperty(plist, "cancelLabel");
		String cancelTrigger = PropertyListUtils.getStringProperty(plist, "cancelTrigger");
		String confirmText = PropertyListUtils.getStringProperty(plist, "confirmText");
		String denyText = PropertyListUtils.getStringProperty(plist, "denyText");
		String submitLabel = PropertyListUtils.getStringProperty(plist, "submitLabel");

		return new ConfirmationOptions(denyCommands, submitCommands, confirmText, denyText, cancelCommands, cancelLabel, submitLabel, confirmCommands, cancelTrigger);
	}

	/**
	 * @return the denyCommands
	 */
	public List<ClientBoundCommand> getDenyCommands() {
		return denyCommands;
	}

	/**
	 * @return the submitCommands
	 */
	public List<ClientBoundCommand> getSubmitCommands() {
		return submitCommands;
	}

	/**
	 * @return the confirmText
	 */
	public String getConfirmText() {
		return confirmText;
	}

	/**
	 * @return the denyText
	 */
	public String getDenyText() {
		return denyText;
	}

	/**
	 * @return the cancelCommands
	 */
	public List<ClientBoundCommand> getCancelCommands() {
		return cancelCommands;
	}

	/**
	 * @return the cancelLabel
	 */
	public String getCancelLabel() {
		return cancelLabel;
	}

	/**
	 * @return the submitLabel
	 */
	public String getSubmitLabel() {
		return submitLabel;
	}

	/**
	 * @return the confirmCommands
	 */
	public List<ClientBoundCommand> getConfirmCommands() {
		return confirmCommands;
	}

	/**
	 * @return the cancelTrigger
	 */
	public String getCancelTrigger() {
		return cancelTrigger;
	}
}
