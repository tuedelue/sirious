package com.wow.siriobjects.ui;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;


public class Snippet extends AceView implements ViewObjectInterface {
	private static final long	serialVersionUID	= 1024822628398088194L;
	public static final String	clazz				= "Snippet";
	public static final String	group				= "com.apple.ace.assistant";

	Object						otherOptions;
	ConfirmationOptions			confirmationOptions;

	public Snippet() {
	}

	public Snippet(String group, String viewId, String speakableText, boolean listenAfterSpeaking, Object otherOptions, ConfirmationOptions confirmationOptions) {
		super(viewId, speakableText, listenAfterSpeaking);
		this.otherOptions = otherOptions;
		this.confirmationOptions = confirmationOptions;
	}

	/**
	 * @return the otherOptions
	 */
	public Object getOtherOptions() {
		return otherOptions;
	}

	/**
	 * @return the confirmationOptions
	 */
	public ConfirmationOptions getConfirmationOptions() {
		return confirmationOptions;
	}

	public static Snippet fromPlist(NSDictionary plist) {
		AceView a = AceView.fromPlist(plist);
		ConfirmationOptions confirmationOptions = PropertyListUtils.getObject(plist, "confirmationOptions");

		return new Snippet(group, a.getViewId(), a.getSpeakableText(), a.isListenAfterSpeaking(), null, confirmationOptions);
	}

}
