package com.wow.siriobjects.ui;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class AceView extends AceObject implements ViewObjectInterface {
	public static final String	clazz	= "AceView";
	public static final String	group	= "com.apple.ace.assistant";

	String						viewId;
	String						speakableText;
	boolean						listenAfterSpeaking;

	public AceView() {
	}

	public AceView(String viewId, String speakableText, boolean listenAfterSpeaking) {
		super(clazz, group);
		this.viewId = viewId;
		this.speakableText = speakableText;
		this.listenAfterSpeaking = listenAfterSpeaking;
	}

	public static AceView fromPlist(NSDictionary plist) {
		String viewId = PropertyListUtils.getStringProperty(plist, "viewId");
		String speakableText = PropertyListUtils.getStringProperty(plist, "speakableText");
		boolean listenAfterSpeaking = PropertyListUtils.getBooleanProperty(plist, "listenAfterSpeaking");

		return new AceView(viewId, speakableText, listenAfterSpeaking);
	}

	/**
	 * @return the viewId
	 */
	public String getViewId() {
		return viewId;
	}

	/**
	 * @return the speakableText
	 */
	public String getSpeakableText() {
		return speakableText;
	}

	/**
	 * @return the listenAfterSpeaking
	 */
	public boolean isListenAfterSpeaking() {
		return listenAfterSpeaking;
	}

}
