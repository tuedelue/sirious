package com.wow.siriobjects.ui;

import com.wow.siriobjects.AceObject;

public class HtmlView extends AceObject {
	String html = "";

	public HtmlView(String html) {
		super("HtmlView", "com.apple.ace.assistant");
		this.html = html;
	}

}
