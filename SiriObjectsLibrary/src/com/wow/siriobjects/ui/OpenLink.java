package com.wow.siriobjects.ui;

import com.wow.siriobjects.AceObject;

public class OpenLink extends AceObject {
	String ref = "";

	public OpenLink(String ref) {
		super("OpenLink", "com.apple.ace.assistant");
		this.ref = ref;
	}

}
