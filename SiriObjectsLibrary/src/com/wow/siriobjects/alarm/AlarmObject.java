package com.wow.siriobjects.alarm;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.system.DomainObject;


public class AlarmObject extends DomainObject {
	int				relativeOffsetMinutes;
	String			label;
	int				hour;
	List<Object>	frequency;
	boolean			enabled;

	public AlarmObject(String identifier, int relativeOffsetMinutes, String label, int hour, List<Object> frequency, boolean enabled) {
		super(null, "com.apple.ace.alarm", identifier);
		this.relativeOffsetMinutes = relativeOffsetMinutes;
		this.label = label;
		this.hour = hour;
		this.frequency = (frequency != null) ? frequency : new ArrayList<Object>();
		this.enabled = enabled;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
