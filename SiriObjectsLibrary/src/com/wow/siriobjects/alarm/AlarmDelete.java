package com.wow.siriobjects.alarm;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;


public class AlarmDelete extends ClientBoundCommand {
	List<String> alarmIds;
	String targetAppId;

	public AlarmDelete(String refId, List<String> alarmIds, String targetAppId) {
		super("Delete", "com.apple.ace.alarm", null, refId, null);
		this.alarmIds = (alarmIds != null) ? alarmIds : new ArrayList<String>();
		this.targetAppId = targetAppId;
	}

}
