package com.wow.siriobjects.alarm;

import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;


public class AlarmSearch extends ClientBoundCommand {
	int minute;
	String label;
	String identifier;
	int hour;
	List<Object> frequency;
	boolean enabled;
	String targetAppId;

	public AlarmSearch(String refId, int minute, String label,
			String identifier, int hour, List<Object> frequency,
			boolean enabled, String targetAppId) {
		super("Search", "com.apple.alarm", null, refId, null);
		this.minute = minute;
		this.label = label;
		this.identifier = identifier;
		this.hour = hour;
		this.frequency = frequency;
		this.enabled = enabled;
		this.targetAppId = targetAppId;
	}

}
