package com.wow.siriobjects.alarm;

import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;


public class AlarmUpdate extends ClientBoundCommand {
	List<Object> removedFrequency;
	int minute;
	String label;
	int hour;
	boolean enabled;
	String alarmId;
	List<Object> addedFrequency;
	String targetAppId;

	public AlarmUpdate(String aceId, String refId,
			List<Object> removedFrequency, int minute, String label, int hour,
			boolean enabled, String alarmId, List<Object> addedFrequency,
			String targetAppId) {
		super("Update", "com.apple.ace.alarm", null, refId, null);
		this.removedFrequency = removedFrequency;
		this.minute = minute;
		this.label = label;
		this.hour = hour;
		this.enabled = enabled;
		this.alarmId = alarmId;
		this.addedFrequency = addedFrequency;
		this.targetAppId = targetAppId;
	}

}
