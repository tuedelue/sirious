package com.wow.siriobjects.alarm;

import com.wow.siriobjects.ClientBoundCommand;

public class AlarmCreate extends ClientBoundCommand {
	AlarmObject	alarmToCreate;
	String		targetAppId;

	public AlarmCreate(String refId, AlarmObject alarmToCreate, String targetAppId) {
		super("Create", "com.apple.ace.alarm", null, refId, null);
		this.alarmToCreate = alarmToCreate;
		this.targetAppId = targetAppId;
	}

}
