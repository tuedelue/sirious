package com.wow.siriobjects.alarm;

import com.wow.siriobjects.ServerBoundCommand;

public class AlarmUpdateCompleted extends ServerBoundCommand {

	public AlarmUpdateCompleted() {
		super("UpdateCompleted", "com.apple.ace.alarm", null, null);
	}

}
