package com.wow.siriobjects.alarm;

import java.util.List;

import com.wow.siriobjects.ui.Snippet;


public class AlarmSnippet extends Snippet {
	List<AlarmObject>	alarms;

	public AlarmSnippet(List<AlarmObject> alarms) {
		super("com.apple.ace.alarm", null, null, false, null, null);
		this.alarms = alarms;
	}

}
