package com.wow.siriobjects.alarm;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.ServerBoundCommand;


public class AlarmSearchCompleted extends ServerBoundCommand {
	List<AlarmObject> results;

	public AlarmSearchCompleted(List<AlarmObject> results) {
		super("SearchCompleted", "com.apple.ace.alarm", null, null);
		this.results = (results != null) ? results
				: new ArrayList<AlarmObject>();
	}

}
