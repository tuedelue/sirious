package com.wow.siriobjects.alarm;

import com.wow.siriobjects.ServerBoundCommand;

public class AlarmDeleteCompleted extends ServerBoundCommand {

	public AlarmDeleteCompleted(String refId) {
		super("DeleteCompleted", "com.apple.alarm", null, refId);
	}

}
