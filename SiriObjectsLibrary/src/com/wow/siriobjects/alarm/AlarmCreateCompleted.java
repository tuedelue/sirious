package com.wow.siriobjects.alarm;

import com.wow.siriobjects.ServerBoundCommand;

public class AlarmCreateCompleted extends ServerBoundCommand {
	String alarmId;

	public AlarmCreateCompleted(String encodedClassName, String groupId,
			String alarmId) {
		super("CreateCompleted", "com.apple.alarm", null, null);
		this.alarmId = alarmId;
	}

}
