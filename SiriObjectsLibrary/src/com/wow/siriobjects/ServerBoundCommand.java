package com.wow.siriobjects;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;

public abstract class ServerBoundCommand extends AceObject {
	String	aceId;
	String	refId;

	protected ServerBoundCommand() {

	}

	public ServerBoundCommand(String encodedClassName, String groupId, String aceId, String refId) {
		super(encodedClassName, groupId);
		this.aceId = aceId;
		this.refId = refId;
	}

	public NSDictionary toPlist() {
		if (aceId != null)
			plist.put("aceId", new NSString(aceId));
		if (refId != null)
			plist.put("refId", new NSString(refId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString() + " aceId=" + aceId + " refId=" + refId;
	}

	/**
	 * @return the aceId
	 */
	public String getAceId() {
		return aceId;
	}

	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}

	/**
	 * @param aceId
	 *            the aceId to set
	 */
	public void setAceId(String aceId) {
		this.aceId = aceId;
	}

	/**
	 * @param refId
	 *            the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}
}
