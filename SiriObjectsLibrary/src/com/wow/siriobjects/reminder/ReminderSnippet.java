package com.wow.siriobjects.reminder;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.AceObject;


public class ReminderSnippet extends AceObject {
	List<ReminderObject> reminders;
	boolean temporary;
	String dialogPhase;

	public ReminderSnippet(List<ReminderObject> reminders, boolean temporary,
			String dialogPhase) {
		super("Snippet", "com.apple.ace.reminder");
		this.reminders = (reminders != null) ? reminders
				: new ArrayList<ReminderObject>();
		this.temporary = temporary;
		this.dialogPhase = dialogPhase;
	}

}
