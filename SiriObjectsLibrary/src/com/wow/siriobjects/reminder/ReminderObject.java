package com.wow.siriobjects.reminder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.wow.siriobjects.AceObject;


public class ReminderObject extends AceObject {
	String dueDateTimneZoneId;
	Date dueDate;
	boolean completed;
	List<ListObject> lists;
	List<DateTimeTrigger> trigger;
	String subject;
	boolean important;
	String identifier;

	public ReminderObject(String dueDateTimneZoneId, Date dueDate,
			boolean completed, List<ListObject> lists,
			List<DateTimeTrigger> trigger, String subject, boolean important,
			String identifier) {
		super("Object", "com.apple.ace.reminder");
		this.dueDateTimneZoneId = dueDateTimneZoneId;
		this.dueDate = dueDate;
		this.completed = completed;
		this.lists = (lists != null) ? lists : new ArrayList<ListObject>();
		this.trigger = (trigger != null) ? trigger
				: new ArrayList<DateTimeTrigger>();
		this.subject = subject;
		this.important = important;
		this.identifier = identifier;
	}

}
