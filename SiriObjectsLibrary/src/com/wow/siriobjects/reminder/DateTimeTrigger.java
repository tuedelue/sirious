package com.wow.siriobjects.reminder;

import java.util.Date;

import com.wow.siriobjects.AceObject;


public class DateTimeTrigger extends AceObject {
	Date date;

	public DateTimeTrigger(Date date) {
		super("DateTimeTrigger", "com.apple.ace.reminder");
		this.date = date;
	}

}
