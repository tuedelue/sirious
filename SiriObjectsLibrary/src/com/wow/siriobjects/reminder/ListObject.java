package com.wow.siriobjects.reminder;

import com.wow.siriobjects.AceObject;

public class ListObject extends AceObject {
	String name;

	public ListObject(String name) {
		super("ListObject", "com.apple.ace.reminder");
		this.name = name;
	}
}
