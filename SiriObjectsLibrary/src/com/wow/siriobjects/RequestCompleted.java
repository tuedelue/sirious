package com.wow.siriobjects;

import java.util.List;

import com.dd.plist.NSDictionary;

public class RequestCompleted extends ClientBoundCommand {
	public static final String	clazz	= "RequestCompleted";
	public static final String	group	= "com.apple.ace.system";

	public RequestCompleted() {
	}

	public RequestCompleted(String aceId, String refId, List<ClientBoundCommand> callbacks) {
		super(clazz, group, aceId, refId, callbacks);
	}

	public static RequestCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		return new RequestCompleted(aceId, refId, callbacks);
	}
}
