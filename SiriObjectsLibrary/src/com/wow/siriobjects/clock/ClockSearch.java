package com.wow.siriobjects.clock;

import com.wow.siriobjects.ClientBoundCommand;

public class ClockSearch extends ClientBoundCommand {
	String unlocalizedCountryName;
	String unlocalizedCityName;
	String identifier;
	String countryCode;
	String alCityId;
	String targetAppId;

	public ClockSearch(String refId, String unlocalizedCountryName,
			String unlocalizedCityName, String identifier, String countryCode,
			String alCityId, String targetAppId) {
		super("Search", "com.apple.ace.clock", null, refId, null);
		this.unlocalizedCountryName = unlocalizedCountryName;
		this.unlocalizedCityName = unlocalizedCityName;
		this.identifier = identifier;
		this.countryCode = countryCode;
		this.alCityId = alCityId;
		this.targetAppId = targetAppId;
	}

}
