package com.wow.siriobjects.clock;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.Snippet;


public class ClockSnippet extends Snippet {
	public static final String	clazz	= "Snippet";
	public static final String	group	= "com.apple.ace.clock";

	List<ClockObject>			clocks;

	public ClockSnippet() {
	}

	public ClockSnippet(List<ClockObject> clocks) {
		super(group, null, null, false, null, null);
		this.clocks = clocks;
	}

	public static ClockSnippet fromPlist(NSDictionary plist) {
		List<ClockObject> clearClocks = PropertyListUtils.getListObjects(plist, "clocks");

		return new ClockSnippet(clearClocks);
	}

	public NSDictionary toPlist() {
		if (clocks != null)
			properties.put("clocks", PropertyListUtils.convertPlistSerializableListToNSArray(clocks));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the clocks
	 */
	public List<ClockObject> getClocks() {
		return clocks;
	}

}
