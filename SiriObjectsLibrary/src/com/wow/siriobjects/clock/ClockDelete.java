package com.wow.siriobjects.clock;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.ClientBoundCommand;


public class ClockDelete extends ClientBoundCommand {
	List<String> clockIds;
	String targetAppId;

	public ClockDelete(String refId, List<String> clockIds, String targetAppId) {
		super("Delete", "com.apple.ace.clock", null, refId, null);
		this.clockIds = (clockIds != null) ? clockIds : new ArrayList<String>();
		this.targetAppId = targetAppId;
	}

}
