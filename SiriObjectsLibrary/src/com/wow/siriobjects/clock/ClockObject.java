package com.wow.siriobjects.clock;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.DomainObject;


public class ClockObject extends DomainObject {
	public static final String	clazz	= "Object";
	public static final String	group	= "com.apple.ace.clock";

	String						unlocalizedCountryName;
	String						unlocalizedCityName;
	String						timezoneId;
	String						countryName;
	String						countryCode;
	String						cityName;
	String						alCityId;

	public ClockObject() {
		super(clazz, group);
	}

	public ClockObject(String unlocalizedCountryName, String unlocalizedCityName, String timezoneId, String countryName, String countryCode, String cityName, String alCityId) {
		super(clazz, group, null);
		this.unlocalizedCountryName = unlocalizedCountryName;
		this.unlocalizedCityName = unlocalizedCityName;
		this.timezoneId = timezoneId;
		this.countryName = countryName;
		this.countryCode = countryCode;
		this.cityName = cityName;
		this.alCityId = alCityId;
	}

	public static ClockObject fromPlist(NSDictionary plist) {
		String unlocalizedCountryName = PropertyListUtils.getStringProperty(plist, "unlocalizedCountryName");
		String unlocalizedCityName = PropertyListUtils.getStringProperty(plist, "unlocalizedCityName");
		String timeZoneId = PropertyListUtils.getStringProperty(plist, "timezoneId");
		String countryName = PropertyListUtils.getStringProperty(plist, "countryName");
		String cityName = PropertyListUtils.getStringProperty(plist, "cityName");
		String countryCode = PropertyListUtils.getStringProperty(plist, "countryCode");
		String alCityId = PropertyListUtils.getStringProperty(plist, "alCityId");

		return new ClockObject(unlocalizedCountryName, unlocalizedCityName, timeZoneId, countryName, countryCode, cityName, alCityId);
	}

	public NSDictionary toPlist() {
		if (alCityId != null)
			properties.put("alCityId", new NSString(alCityId));
		if (cityName != null)
			properties.put("cityName", new NSString(cityName));
		if (countryCode != null)
			properties.put("countryCode", new NSString(countryCode));
		if (countryName != null)
			properties.put("countryName", new NSString(countryName));
		if (timezoneId != null)
			properties.put("timezoneId", new NSString(timezoneId));
		if (unlocalizedCityName != null)
			properties.put("unlocalizedCityName", new NSString(unlocalizedCityName));
		if (unlocalizedCountryName != null)
			properties.put("unlocalizedCountryName", new NSString(unlocalizedCountryName));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the unlocalizedCountryName
	 */
	public String getUnlocalizedCountryName() {
		return unlocalizedCountryName;
	}

	/**
	 * @return the unlocalizedCityName
	 */
	public String getUnlocalizedCityName() {
		return unlocalizedCityName;
	}

	/**
	 * @return the timeZoneId
	 */
	public String getTimezoneId() {
		return timezoneId;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @return the alCityId
	 */
	public String getAlCityId() {
		return alCityId;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
