package com.wow.siriobjects.clock;

import com.wow.siriobjects.ClientBoundCommand;

public class ClockAdd extends ClientBoundCommand {
	ClockObject clockToAdd;
	String targetAppId;

	public ClockAdd(String refId, ClockObject clockToAdd, String targetAppId) {
		super("Add", "com.apple.ace.clock", null, refId, null);
		this.clockToAdd = clockToAdd;
		this.targetAppId = targetAppId;
	}

}
