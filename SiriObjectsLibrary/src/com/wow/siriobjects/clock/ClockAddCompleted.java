package com.wow.siriobjects.clock;

import com.wow.siriobjects.ServerBoundCommand;

public class ClockAddCompleted extends ServerBoundCommand {
	String worldClockId;
	boolean alreadyExists;

	public ClockAddCompleted(String refId, String worldClockId,
			boolean alreadyExists) {
		super("AddCompleted", "com.apple.ace.clock", null, refId);
		this.worldClockId = worldClockId;
		this.alreadyExists = alreadyExists;
	}
}
