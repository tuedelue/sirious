package com.wow.siriobjects.clock;

import java.util.List;

import com.wow.siriobjects.ServerBoundCommand;


public class ClockSearchCompleted extends ServerBoundCommand {
	List<ClockObject> results;

	public ClockSearchCompleted(List<ClockObject> results) {
		super("SearchCompleted", "com.apple.ace.clock", null, null);
		this.results = results;
	}
}
