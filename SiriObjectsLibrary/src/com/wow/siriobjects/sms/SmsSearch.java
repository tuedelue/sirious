package com.wow.siriobjects.sms;

import java.util.Date;
import java.util.List;

import com.dd.plist.NSDate;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.PersonAttribute;


public class SmsSearch extends ClientBoundCommand {
	private static final long	serialVersionUID	= -2521840573768137865L;
	public static final String	clazz				= "Search";
	public static final String	group				= "com.apple.ace.sms";

	String						targetAppId;
	Date						end;
	String						message;
	PersonAttribute				recipient;
	PersonAttribute				sender;
	Date						start;
	int							status;
	String						timeZoneId;

	public SmsSearch() {

	}

	public SmsSearch(String aceId, String refId, List<ClientBoundCommand> callbacks, String targetAppId, Date end, String message, PersonAttribute recipient, PersonAttribute sender, Date start,
			int status, String timeZoneId) {
		super(clazz, group, aceId, refId, callbacks);
		this.targetAppId = targetAppId;
		this.end = end;
		this.message = message;
		this.recipient = recipient;
		this.sender = sender;
		this.start = start;
		this.status = status;
		this.timeZoneId = timeZoneId;
	}

	public NSDictionary toPlist() {
		properties.put("end", new NSDate(end));
		properties.put("message", new NSString(message));
		properties.put("recipient", recipient.toPlist());
		properties.put("sender", sender.toPlist());
		properties.put("start", new NSDate(start));
		properties.put("status", new NSNumber(status));
		properties.put("targetAppId", new NSString(targetAppId));
		properties.put("timeZoneId", new NSString(timeZoneId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SmsSearch fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);
		Date end = PropertyListUtils.getDateProperty(plist, "end");
		PersonAttribute recipient = PropertyListUtils.getObject(plist, "recipient");
		PersonAttribute sender = PropertyListUtils.getObject(plist, "sender");
		Date start = PropertyListUtils.getDateProperty(plist, "start");
		int status = PropertyListUtils.getIntProperty(plist, "status");
		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		String timeZoneId = PropertyListUtils.getStringProperty(plist, "timeZoneId");
		String message = PropertyListUtils.getStringProperty(plist, "message");

		return new SmsSearch(aceId, refId, callbacks, targetAppId, end, message, recipient, sender, start, status, timeZoneId);
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

	/**
	 * @return the end
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the recipient
	 */
	public PersonAttribute getRecipient() {
		return recipient;
	}

	/**
	 * @return the sender
	 */
	public PersonAttribute getSender() {
		return sender;
	}

	/**
	 * @return the start
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @return the timeZoneId
	 */
	public String getTimeZoneId() {
		return timeZoneId;
	}

}
