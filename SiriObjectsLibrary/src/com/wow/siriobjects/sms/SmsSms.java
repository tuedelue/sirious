package com.wow.siriobjects.sms;

import java.util.Date;
import java.util.List;

import com.dd.plist.NSDate;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.DomainObject;
import com.wow.siriobjects.system.PersonAttribute;


public class SmsSms extends DomainObject {
	private static final long	serialVersionUID	= -7180781563963941319L;
	public static final String	clazz				= "Sms";
	public static final String	group				= "com.apple.ace.sms";

	String						attachment;
	Date						dateSent;
	String						message;
	List<PersonAttribute>		msgRecipients;
	PersonAttribute				msgSender;
	boolean						outgoing;
	List<String>				recipients;
	String						sender;
	String						subject;
	String						timezoneId;

	public SmsSms() {
		super(clazz, group);
	}

	public SmsSms(String identifier, String attachment, Date dateSent, String message, List<PersonAttribute> msgRecipients, PersonAttribute msgSender, boolean outgoing, List<String> recipients,
			String sender, String subject, String timezoneId) {
		super(clazz, group, identifier);
		this.attachment = attachment;
		this.dateSent = dateSent;
		this.message = message;
		this.msgRecipients = msgRecipients;
		this.msgSender = msgSender;
		this.outgoing = outgoing;
		this.recipients = recipients;
		this.sender = sender;
		this.subject = subject;
		this.timezoneId = timezoneId;
	}

	public NSDictionary toPlist() {
		if (attachment != null)
			properties.put("attachment", new NSString(attachment));
		if (dateSent != null)
			properties.put("dateSend", new NSDate(dateSent));
		if (message != null)
			properties.put("message", new NSString(message));
		if (msgRecipients != null)
			properties.put("msgRecipients", PropertyListUtils.convertPlistSerializableListToNSArray(msgRecipients));
		if (msgSender != null)
			properties.put("msgSender", msgSender.toPlist());
		properties.put("outgoing", new NSNumber(outgoing));
		if (recipients != null)
			properties.put("recipients", PropertyListUtils.convertStringListToNSArray(recipients));
		if (sender != null)
			properties.put("sender", new NSString(sender));
		if (subject != null)
			properties.put("subject", new NSString(subject));
		if (timezoneId != null)
			properties.put("timezoneId", new NSString(timezoneId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SmsSms fromPlist(NSDictionary plist) {
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");
		String attachment = PropertyListUtils.getStringProperty(plist, "attachment");
		Date dateSent = PropertyListUtils.getDateProperty(plist, "dateSent");
		String message = PropertyListUtils.getStringProperty(plist, "message");
		List<PersonAttribute> msgRecipients = PropertyListUtils.getListObjects(plist, "msgRecipients");
		PersonAttribute msgSender = PropertyListUtils.getObject(plist, "msdSender");
		boolean outgoing = PropertyListUtils.getBooleanProperty(plist, "outgoing");
		List<String> recipients = PropertyListUtils.getStringList(plist, "recipients");
		String sender = PropertyListUtils.getStringProperty(plist, "sender");
		String subject = PropertyListUtils.getStringProperty(plist, "subject");
		String timezoneId = PropertyListUtils.getStringProperty(plist, "timezoneId");

		return new SmsSms(identifier, attachment, dateSent, message, msgRecipients, msgSender, outgoing, recipients, sender, subject, timezoneId);
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the attachment
	 */
	public String getAttachment() {
		return attachment;
	}

	/**
	 * @return the dateSent
	 */
	public Date getDateSent() {
		return dateSent;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the msgRecipients
	 */
	public List<PersonAttribute> getMsgRecipients() {
		return msgRecipients;
	}

	/**
	 * @return the msgSender
	 */
	public PersonAttribute getMsgSender() {
		return msgSender;
	}

	/**
	 * @return the outgoing
	 */
	public boolean isOutgoing() {
		return outgoing;
	}

	/**
	 * @return the recipients
	 */
	public List<String> getRecipients() {
		return recipients;
	}

	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @return the timezoneId
	 */
	public String getTimezoneId() {
		return timezoneId;
	}

	/**
	 * @param attachment
	 *            the attachment to set
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * @param dateSent
	 *            the dateSent to set
	 */
	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param msgRecipients
	 *            the msgRecipients to set
	 */
	public void setMsgRecipients(List<PersonAttribute> msgRecipients) {
		this.msgRecipients = msgRecipients;
	}

	/**
	 * @param msgSender
	 *            the msgSender to set
	 */
	public void setMsgSender(PersonAttribute msgSender) {
		this.msgSender = msgSender;
	}

	/**
	 * @param outgoing
	 *            the outgoing to set
	 */
	public void setOutgoing(boolean outgoing) {
		this.outgoing = outgoing;
	}

	/**
	 * @param recipients
	 *            the recipients to set
	 */
	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	/**
	 * @param sender
	 *            the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @param timezoneId
	 *            the timezoneId to set
	 */
	public void setTimezoneId(String timezoneId) {
		this.timezoneId = timezoneId;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		SmsSms sadd = (SmsSms) add;
		SmsSms sremove = (SmsSms) remove;
		SmsSms sset = (SmsSms) set;

		// add
		if (sadd != null)
			setVars(sadd);

		// set
		if (sset != null)
			setVars(sset);

		// remove
		if (sremove == null)
			return;

		if (sremove.getAttachment() != null) {
			attachment = null;
		}
		if (sremove.getDateSent() != null) {
			dateSent = null;
		}
		if (sremove.getMessage() != null) {
			message = null;
		}
		if (sremove.getMsgRecipients() != null) {
			msgRecipients = null;
		}
		if (sremove.getMsgSender() != null) {
			msgSender = null;
		}
		if (sremove.getRecipients() != null) {
			recipients = null;
		}
		if (sremove.getSender() != null) {
			sender = null;
		}
		if (sremove.getSubject() != null) {
			subject = null;
		}
		if (sremove.getTimezoneId() != null) {
			timezoneId = null;
		}

		notifyListeners();
	}

	private void setVars(SmsSms obj) {
		if (obj.getAttachment() != null) {
			attachment = obj.getAttachment();
		}
		if (obj.getDateSent() != null) {
			dateSent = obj.getDateSent();
		}
		if (obj.getMessage() != null) {
			message = obj.getMessage();
		}
		if (obj.getMsgRecipients() != null) {
			msgRecipients = obj.getMsgRecipients();
		}
		if (obj.getMsgSender() != null) {
			msgSender = obj.getMsgSender();
		}
		if (obj.getRecipients() != null) {
			recipients = obj.getRecipients();
		}
		if (obj.getSender() != null) {
			sender = obj.getSender();
		}
		if (obj.getSubject() != null) {
			subject = obj.getSubject();
		}
		if (obj.getTimezoneId() != null) {
			timezoneId = obj.getTimezoneId();
		}
	}
}
