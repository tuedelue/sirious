package com.wow.siriobjects.sms;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.siriobjects.ui.Snippet;


public class SmsSnippet extends Snippet {
	public static final String	clazz	= "Snippet";
	public static final String	group	= "com.apple.ace.sms";

	List<SmsSms>				smss;

	public SmsSnippet() {

	}

	public SmsSnippet(String viewId, String speakableText, boolean listenAfterSpeaking, Object otherOptions, ConfirmationOptions confirmationOptions, List<SmsSms> smss) {
		super(group, viewId, speakableText, listenAfterSpeaking, otherOptions, confirmationOptions);
		this.smss = smss;
	}

	public NSDictionary toPlist() {
		properties.put("smss", PropertyListUtils.convertPlistSerializableListToNSArray(smss));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SmsSnippet fromPlist(NSDictionary plist) {
		String speakableText = PropertyListUtils.getStringProperty(plist, "speakableText");
		String viewId = PropertyListUtils.getStringProperty(plist, "viewId");
		boolean listenAfterSpeaking = PropertyListUtils.getBooleanProperty(plist, "listenAfterSpeaking");
		ConfirmationOptions confirmationOptions = PropertyListUtils.getObject(plist, "confirmationOptions");

		List<SmsSms> smss = PropertyListUtils.getListObjects(plist, "smss");

		return new SmsSnippet(viewId, speakableText, listenAfterSpeaking, null, confirmationOptions, smss);
	}

	/**
	 * @return the smss
	 */
	public List<SmsSms> getSmss() {
		return smss;
	}

}
