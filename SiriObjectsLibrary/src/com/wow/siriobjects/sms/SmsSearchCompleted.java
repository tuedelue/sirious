package com.wow.siriobjects.sms;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class SmsSearchCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= 4985622948169984487L;
	public static final String	clazz				= "SearchCompleted";
	public static final String	group				= "com.apple.ace.sms";

	List<SmsSms>				results;

	public SmsSearchCompleted() {
	}

	public SmsSearchCompleted(String aceId, String refId, List<SmsSms> results) {
		super(clazz, group, aceId, refId);
		this.results = results;
	}

	public NSDictionary toPlist() {
		properties.put("results", PropertyListUtils.convertPlistSerializableListToNSArray(results));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SmsSearchCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		List<SmsSms> results = PropertyListUtils.getListObjects(plist, "results");

		return new SmsSearchCompleted(aceId, refId, results);
	}

	/**
	 * @return the results
	 */
	public List<SmsSms> getResults() {
		return results;
	}

}
