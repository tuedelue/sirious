package com.wow.siriobjects.sms;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;
import com.wow.siriobjects.system.Person;


public class SmsRecipientSearchCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= -3630782088207577908L;
	public static final String	clazz				= "RecipientSearchCompleted";
	public static final String	group				= "com.apple.ace.sms";

	Person						recipient;
	List<Person>				recipients;

	public SmsRecipientSearchCompleted() {

	}

	public SmsRecipientSearchCompleted(String aceId, String refId, Person recipient, List<Person> recipients) {
		super(clazz, group, aceId, refId);
		this.recipient = recipient;
		this.recipients = recipients;
	}

	public NSDictionary toPlist() {
		properties.put("recipient", recipient.toPlist());
		properties.put("recipients", PropertyListUtils.convertPlistSerializableListToNSArray(recipients));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SmsRecipientSearchCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		Person recipient = PropertyListUtils.getObject(plist, "recipient");
		List<Person> recipients = PropertyListUtils.getListObjects(plist, "recipients");

		return new SmsRecipientSearchCompleted(aceId, refId, recipient, recipients);
	}

	/**
	 * @return the recipient
	 */
	public Person getRecipient() {
		return recipient;
	}

	/**
	 * @return the recipients
	 */
	public List<Person> getRecipients() {
		return recipients;
	}

}
