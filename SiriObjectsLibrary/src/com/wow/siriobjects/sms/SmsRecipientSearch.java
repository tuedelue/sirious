package com.wow.siriobjects.sms;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.Person;


public class SmsRecipientSearch extends ClientBoundCommand {
	private static final long	serialVersionUID	= -7643195806454232430L;
	public static final String	clazz				= "RecipientSearch";
	public static final String	group				= "com.apple.ace.sms";

	String						targetAppId;
	Person						recipient;
	List<Person>				recipients;

	public SmsRecipientSearch() {

	}

	public SmsRecipientSearch(String aceId, String refId, List<ClientBoundCommand> callbacks, String targetAppId, Person recipient, List<Person> recipients) {
		super(clazz, group, aceId, refId, callbacks);
		this.targetAppId = targetAppId;
		this.recipient = recipient;
		this.recipients = recipients;
	}

	public NSDictionary toPlist() {
		properties.put("targetAppId", new NSString(targetAppId));
		properties.put("recipient", recipient.toPlist());
		properties.put("recipients", PropertyListUtils.convertPlistSerializableListToNSArray(recipients));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SmsRecipientSearch fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		Person recipient = PropertyListUtils.getObject(plist, "recipient");
		List<Person> recipients = PropertyListUtils.getListObjects(plist, "recipients");

		return new SmsRecipientSearch(aceId, refId, callbacks, targetAppId, recipient, recipients);
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

	/**
	 * @return the recipient
	 */
	public Person getRecipient() {
		return recipient;
	}

	/**
	 * @return the recipients
	 */
	public List<Person> getRecipients() {
		return recipients;
	}

}
