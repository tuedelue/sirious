package com.wow.siriobjects.localsearch;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.Snippet;


public class MapItemSnippet extends Snippet {
	public static final String	clazz				= "MapItemSnippet";
	public static final String	group				= "com.apple.ace.localsearch";

	boolean						userCurrentLocation	= true;
	List<MapItem>				items;

	public MapItemSnippet() {

	}

	public MapItemSnippet(boolean userCurrentLocation, List<MapItem> items) {
		super(group, null, null, false, null, null);
		this.userCurrentLocation = userCurrentLocation;
		this.items = (items != null) ? items : new ArrayList<MapItem>();
	}

	public NSDictionary toPlist() {
		if (items != null)
			properties.put("items", PropertyListUtils.convertPlistSerializableListToNSArray(items));
		properties.put("userCurrentLocation", new NSNumber(userCurrentLocation));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static MapItemSnippet fromPlist(NSDictionary plist) {
		boolean userCurrentLocation = PropertyListUtils.getBooleanProperty(plist, "userCurrentLocation");
		List<MapItem> items = PropertyListUtils.getListObjects(plist, "items");
		return new MapItemSnippet(userCurrentLocation, items);
	}

	/**
	 * @return the userCurrentLocation
	 */
	public boolean isUserCurrentLocation() {
		return userCurrentLocation;
	}

	/**
	 * @return the items
	 */
	public List<MapItem> getItems() {
		return items;
	}

}
