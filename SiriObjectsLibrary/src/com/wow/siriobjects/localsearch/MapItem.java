package com.wow.siriobjects.localsearch;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.DomainObject;
import com.wow.siriobjects.system.Location;


public class MapItem extends DomainObject {
	private static final long	serialVersionUID	= -655083674724459664L;
	public static final String	clazz				= "MapItem";
	public static final String	group				= "com.apple.ace.localsearch";

	String						label;
	String						detailType;
	Location					location;

	public MapItem() {
		super(clazz, group);
	}

	public MapItem(String label, String detailType, Location location) {
		super(clazz, group, null);
		this.label = label;
		this.detailType = detailType;
		this.location = location;
	}

	public NSDictionary toPlist() {
		if (label != null)
			properties.put("label", new NSString(label));
		if (detailType != null)
			properties.put("detailType", new NSString(detailType));
		if (location != null)
			properties.put("location", location.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static MapItem fromPlist(NSDictionary plist) {
		String label = PropertyListUtils.getStringProperty(plist, "label");
		String detailType = PropertyListUtils.getStringProperty(plist, "detailType");
		Location location = PropertyListUtils.getObject(plist, "location");

		return new MapItem(label, detailType, location);
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the detailType
	 */
	public String getDetailType() {
		return detailType;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
