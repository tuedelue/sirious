package com.wow.siriobjects.contact;

import java.util.Date;
import java.util.List;

import com.dd.plist.NSDate;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.email.Email;
import com.wow.siriobjects.system.Location;
import com.wow.siriobjects.system.RelatedName;


public class PersonSearch extends ClientBoundCommand {
	public static final String	clazz	= "PersonSearch";
	public static final String	group	= "com.apple.ace.contact";

	String						scope;
	List<RelatedName>			relatedNames;
	String						phone;
	String						name;
	boolean						me;
	Email						email;
	String						company;
	Date						birthday;
	Location					address;
	String						accountIdentifier;
	String						targetAppId;

	public PersonSearch() {
	}

	public PersonSearch(String aceId, String refId, String scope, List<RelatedName> relatedNames, String phone, String name, boolean me, Email email, String company, Date birthday, Location address,
			String accountIdentifier, String targetAppId) {
		super(clazz, group, aceId, refId, null);
		this.scope = scope;
		this.relatedNames = relatedNames;
		this.phone = phone;
		this.name = name;
		this.me = me;
		this.email = email;
		this.company = company;
		this.birthday = birthday;
		this.address = address;
		this.accountIdentifier = accountIdentifier;
		this.targetAppId = targetAppId;
	}

	public NSDictionary toPlist() {
		if (accountIdentifier != null)
			properties.put("accountIdentifier)", new NSString(accountIdentifier));
		if (birthday != null)
			properties.put("birthday", new NSDate(birthday));
		if (company != null)
			properties.put("company", new NSString(company));
		if (email != null)
			properties.put("email", email.toPlist());
		properties.put("me", new NSNumber(me));
		if (phone != null)
			properties.put("phone", new NSString(phone));
		if (relatedNames != null)
			properties.put("relationship", PropertyListUtils.convertPlistSerializableListToNSArray(relatedNames));
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));
		if (name != null)
			properties.put("name", new NSString(name));
		if (scope != null)
			properties.put("scope", new NSString(scope));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static PersonSearch fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		String name = PropertyListUtils.getStringProperty(plist, "name");
		String scope = PropertyListUtils.getStringProperty(plist, "scope");
		String accountIdentifier = PropertyListUtils.getStringProperty(plist, "accountIdentifier");
		Email email = PropertyListUtils.getObject(plist, "email");
		boolean me = PropertyListUtils.getBooleanProperty(plist, "me");
		String phone = PropertyListUtils.getStringProperty(plist, "phone");
		List<RelatedName> relatedNames = PropertyListUtils.getListObjects(plist, "relatedNames");
		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		String company = PropertyListUtils.getStringProperty(plist, "company");
		Date birthday = PropertyListUtils.getDateProperty(plist, "birthday");
		Location address = PropertyListUtils.getObject(plist, "address");

		return new PersonSearch(aceId, refId, scope, relatedNames, phone, name, me, email, company, birthday, address, accountIdentifier, targetAppId);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * @return the relationship
	 */
	public List<RelatedName> getRelatedNames() {
		return relatedNames;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @return the me
	 */
	public boolean isMe() {
		return me;
	}

	/**
	 * @return the email
	 */
	public Email getEmail() {
		return email;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @return the address
	 */
	public Location getAddress() {
		return address;
	}

	/**
	 * @return the accountIdentifier
	 */
	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

}
