package com.wow.siriobjects.contact;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.DomainObject;
import com.wow.siriobjects.system.Source;


public class ContactGroup extends DomainObject {
	public static final String	clazz	= "ContactGroup";
	public static final String	group	= "com.apple.ace.contact";

	Source						groupSource;
	String						groupName;

	private ContactGroup(Source groupSource, String groupName) {
		super(clazz, group);
		this.groupSource = groupSource;
		this.groupName = groupName;
	}

	public NSDictionary toPlist() {
		properties.put("groupname", new NSString(groupName));
		properties.put("groupSource", groupSource.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static ContactGroup fromPlist(NSDictionary plist) {
		String groupName = PropertyListUtils.getStringProperty(plist, "groupName");
		Source groupSource = PropertyListUtils.getObject(plist, "groupSource");

		return new ContactGroup(groupSource, groupName);
	}

	/**
	 * @return the groupSource
	 */
	public Source getGroupSource() {
		return groupSource;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
