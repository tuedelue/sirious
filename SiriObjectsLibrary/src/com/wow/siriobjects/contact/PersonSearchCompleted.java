package com.wow.siriobjects.contact;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;
import com.wow.siriobjects.system.Person;


public class PersonSearchCompleted extends ServerBoundCommand {
	public static final String	clazz	= "PersonSearchCompleted";
	public static final String	group	= "com.apple.ace.contact";

	List<Person>				results;

	public PersonSearchCompleted(String aceId, String refId, List<Person> results) {
		super(clazz, group, aceId, refId);
		this.results = results;
	}

	public NSDictionary toPlist() {
		properties.put("results", PropertyListUtils.convertPlistSerializableListToNSArray(results));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
