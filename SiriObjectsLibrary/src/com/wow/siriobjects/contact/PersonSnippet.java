package com.wow.siriobjects.contact;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.Person;
import com.wow.siriobjects.ui.Snippet;


public class PersonSnippet extends Snippet {
	public static final String	clazz	= "PersonSnippet";
	public static final String	group	= "com.apple.ace.contact";
	List<Person>				persons;
	List<String>				displayProperties;

	public PersonSnippet() {

	}

	public PersonSnippet(List<Person> persons, List<String> displayProperties) {
		super(group, null, null, false, null, null);
		this.persons = persons;
		this.displayProperties = displayProperties;
	}

	public NSDictionary toPlist() {
		properties.put("persons", PropertyListUtils.convertPlistSerializableListToNSArray(persons));
		properties.put("displayProperties", PropertyListUtils.convertStringListToNSArray(displayProperties));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static PersonSnippet fromPlist(NSDictionary plist) {
		List<Person> person = PropertyListUtils.getListObjects(plist, "persons");
		List<String> displayProperties = PropertyListUtils.getStringList(plist, "displayProperties");

		return new PersonSnippet(person, displayProperties);
	}

	/**
	 * @return the person
	 */
	public List<Person> getPersons() {
		return persons;
	}

	/**
	 * @return the displayProperties
	 */
	public List<String> getDisplayProperties() {
		return displayProperties;
	}

}
