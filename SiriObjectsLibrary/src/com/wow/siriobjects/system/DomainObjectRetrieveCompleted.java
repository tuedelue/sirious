package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class DomainObjectRetrieveCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= -223892333815355013L;
	public static final String	clazz				= "DomainObjectRetrieveCompleted";
	public static final String	group				= "com.apple.ace.system";

	List<DomainObject>			objects;

	public DomainObjectRetrieveCompleted() {
	}

	public DomainObjectRetrieveCompleted(String aceId, String refId, List<DomainObject> objects) {
		super(clazz, group, aceId, refId);
		this.objects = objects;
	}

	public NSDictionary toPlist() {
		if (objects != null)
			properties.put("objects", PropertyListUtils.convertPlistSerializableListToNSArray(objects));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the objects
	 */
	public List<DomainObject> getObjects() {
		return objects;
	}
}
