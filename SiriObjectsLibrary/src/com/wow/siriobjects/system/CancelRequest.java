package com.wow.siriobjects.system;

import com.wow.siriobjects.ServerBoundCommand;

public class CancelRequest extends ServerBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8072124414599854391L;
	public static final String	clazz				= "CancelRequest";
	public static final String	group				= "com.apple.ace.system";

	public CancelRequest(String aceId, String refId) {
		super(clazz, group, aceId, refId);
	}

}
