package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListSerializable;
import com.wow.siriobjects.PropertyListUtils;


public class ListItem extends DomainObject {
	private static final long	serialVersionUID	= 1735892174021689162L;
	public static final String	clazz				= "ListItem";
	public static final String	group				= "com.apple.ace.assistant";

	String						selectionText;
	PropertyListSerializable	object;
	List<ClientBoundCommand>	commands;
	String						speakableText;
	String						title;

	public ListItem() {
		super(clazz, group);
	}

	public ListItem(String identifier, String selectionText, PropertyListSerializable object, List<ClientBoundCommand> commands, String speakableText, String title) {
		super(clazz, group, identifier);
		this.selectionText = selectionText;
		this.object = object;
		this.commands = commands;
		this.speakableText = speakableText;
		this.title = title;
	}

	public NSDictionary toPlist() {
		if (speakableText != null)
			properties.put("speakableText", new NSString(speakableText));
		if (selectionText != null)
			properties.put("selectionText", new NSString(selectionText));
		if (title != null)
			properties.put("title", new NSString(title));
		if (commands != null)
			properties.put("commands", PropertyListUtils.convertPlistSerializableListToNSArray(commands));
		if (object != null)
			properties.put("object", object.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static ListItem fromPlist(NSDictionary plist) {
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");
		String selectionText = PropertyListUtils.getStringProperty(plist, "selectionText");
		String speakableText = PropertyListUtils.getStringProperty(plist, "speakableText");
		String title = PropertyListUtils.getStringProperty(plist, "title");
		List<ClientBoundCommand> commands = PropertyListUtils.getListObjects(plist, "commands");
		PropertyListSerializable object = PropertyListUtils.getObject(plist, "object");

		return new ListItem(identifier, selectionText, object, commands, speakableText, title);
	}

	/**
	 * @return the selectionText
	 */
	public String getSelectionText() {
		return selectionText;
	}

	/**
	 * @return the object
	 */
	public PropertyListSerializable getObject() {
		return object;
	}

	/**
	 * @return the commands
	 */
	public List<ClientBoundCommand> getCommands() {
		return commands;
	}

	/**
	 * @return the speakableText
	 */
	public String getSpeakableText() {
		return speakableText;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
