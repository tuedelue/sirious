package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class DomainObjectRetrieve extends ClientBoundCommand {
	private static final long	serialVersionUID	= 8524902749050461221L;
	public static final String	clazz				= "DomainObjectRetrieve";
	public static final String	group				= "com.apple.ace.system";

	List<DomainObject>			identifiers;

	public DomainObjectRetrieve() {
	}

	public DomainObjectRetrieve(String aceId, String refId, List<ClientBoundCommand> callbacks, List<DomainObject> identifiers) {
		super(clazz, group, aceId, refId, callbacks);
		this.identifiers = identifiers;
	}

	public NSDictionary toPlist() {
		if (identifiers != null)
			properties.put("identifiers", PropertyListUtils.convertPlistSerializableListToNSArray(identifiers));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectRetrieve fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		List<DomainObject> identifiers = PropertyListUtils.getListObjects(plist, "identifiers");

		return new DomainObjectRetrieve(aceId, refId, callbacks, identifiers);
	}

	/**
	 * @return the identifiers
	 */
	public List<DomainObject> getIdentifiers() {
		return identifiers;
	}

}
