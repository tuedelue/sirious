package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class AssistantCreated extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1887475791776486464L;
	public static final String	clazz				= "AssistantCreated";
	public static final String	group				= "com.apple.ace.system";

	String						assistantId;
	String						speechId;

	public AssistantCreated() {

	}

	public AssistantCreated(String aceId, String refId, String assistantId, String speechId) {
		super(clazz, group, aceId, refId, null);
		this.assistantId = assistantId;
		this.speechId = speechId;
	}

	public NSDictionary toPlist() {
		if (assistantId != null)
			properties.put("assistantId", new NSString(assistantId));
		if (speechId != null)
			properties.put("speechId", new NSString(speechId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static AssistantCreated fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		String assistantId = PropertyListUtils.getStringProperty(plist, "assistantId");
		String speechId = PropertyListUtils.getStringProperty(plist, "speechId");

		return new AssistantCreated(aceId, refId, assistantId, speechId);
	}
}
