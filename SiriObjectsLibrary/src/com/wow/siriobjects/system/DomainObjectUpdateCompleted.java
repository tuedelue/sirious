package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class DomainObjectUpdateCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= 9169559964650252371L;
	public static final String	clazz				= "DomainObjectUpdateCompleted";
	public static final String	group				= "com.apple.ace.system";

	DomainObject				identifier;

	public DomainObjectUpdateCompleted() {
	}

	public DomainObjectUpdateCompleted(String aceId, String refId, DomainObject identifier) {
		super(clazz, group, aceId, refId);
		this.identifier = identifier;
	}

	public NSDictionary toPlist() {
		if (identifier != null)
			properties.put("identifier", identifier.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectUpdateCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		DomainObject identifier = PropertyListUtils.getObject(plist, "identifier");

		return new DomainObjectUpdateCompleted(aceId, refId, identifier);
	}

	/**
	 * @return the identifier
	 */
	public DomainObject getIdentifier() {
		return identifier;
	}
}
