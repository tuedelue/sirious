package com.wow.siriobjects.system;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;


public abstract class DomainObject extends AceObject implements DomainObjectInterface {
	private static final long					serialVersionUID	= 6786659441568771557L;
	String										identifier;

	private List<OnDomainObjectChangedListener>	listeners			= new ArrayList<OnDomainObjectChangedListener>();

	public void addListener(OnDomainObjectChangedListener listener) {
		listeners.add(listener);
	}

	public boolean removeListener(OnDomainObjectChangedListener listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
			return true;
		}
		return false;
	}

	public void removeAllListeners() {
		listeners.clear();
	}

	public void notifyListeners() {
		for (OnDomainObjectChangedListener l : listeners) {
			l.handleChange(this);
		}
	}

	public DomainObject(String clazz, String group) {
		super(clazz, group);
	}

	public DomainObject(String clazz, String group, String identifier) {
		super(clazz, group);
		this.identifier = identifier;
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier
	 *            the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public NSDictionary toPlist() {
		if (identifier != null) {
			properties.put("identifier", new NSString(identifier));
		}

		return super.toPlist();
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		notifyListeners();
	}

}
