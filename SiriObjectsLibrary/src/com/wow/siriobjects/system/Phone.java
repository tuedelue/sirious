package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;


public class Phone extends DomainObject {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -9006487974458680416L;
	public static final String	clazz				= "Phone";
	public static final String	group				= "com.apple.ace.system";

	String						number;
	String						label;
	int							favoriteVoice;
	int							favoriteFacetime;

	public Phone() {
		super(clazz, group);
	}

	public Phone(String number, String label, int favoriteVoice, int favoriteFacetime) {
		super(clazz, group, null);
		this.number = number;
		this.label = label;
		this.favoriteVoice = favoriteVoice;
		this.favoriteFacetime = favoriteFacetime;
	}

	public NSDictionary toPlist() {
		if (number != null)
			properties.put("number", new NSString(number));
		if (label != null)
			properties.put("label", new NSString(label));
		properties.put("favoriteVoice", new NSNumber(favoriteVoice));
		properties.put("favoriteFacetime", new NSNumber(favoriteFacetime));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static Phone fromPlist(NSDictionary plist) {
		String number = PropertyListUtils.getStringProperty(plist, "number");
		String label = PropertyListUtils.getStringProperty(plist, "label");
		int favoriteVoice = PropertyListUtils.getIntProperty(plist, "favoriteVoice");
		int favoriteFacetime = PropertyListUtils.getIntProperty(plist, "favoriteFacetime");

		return new Phone(number, label, favoriteVoice, favoriteFacetime);
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the favoriteVoice
	 */
	public int getFavoriteVoice() {
		return favoriteVoice;
	}

	/**
	 * @return the favoriteFacetime
	 */
	public int getFavoriteFacetime() {
		return favoriteFacetime;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
