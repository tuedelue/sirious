package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ServerBoundCommand;


public class DestroyAssistant extends ServerBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7455185112406582981L;
	public static final String	clazz				= "DestroyAssistant";
	public static final String	group				= "com.apple.ace.system";

	String						assistantId;

	public DestroyAssistant(String aceId, String refId, String assistantId) {
		super(clazz, group, aceId, refId);
		this.assistantId = assistantId;
	}

	public NSDictionary toPlist() {
		properties.put("assistantId", new NSString(assistantId));
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
