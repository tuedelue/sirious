package com.wow.siriobjects.system;

import com.wow.siriobjects.ClientBoundCommand;

public class CancelSucceeded extends ClientBoundCommand {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6747987896297683458L;

	public CancelSucceeded(String aceId, String refId) {
		super("CancelSucceeded", "com.apple.ace.system", aceId, refId, null);
	}

}
