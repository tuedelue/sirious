package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class DomainObjectCancel extends ClientBoundCommand {
	private static final long	serialVersionUID	= 3974193714434458271L;
	public static final String	clazz				= "DomainObjectCancel";
	public static final String	group				= "com.apple.ace.system";

	DomainObject				identifier;

	public DomainObjectCancel() {
	}

	public DomainObjectCancel(String aceId, String refId, List<ClientBoundCommand> callbacks, DomainObject identifier) {
		super(clazz, group, aceId, refId, callbacks);
		this.identifier = identifier;
	}

	public NSDictionary toPlist() {
		if (identifier != null)
			properties.put("identifier", identifier.toPlist());
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectCancel fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);
		DomainObject identifier = PropertyListUtils.getObject(plist, "identifier");

		return new DomainObjectCancel(aceId, refId, callbacks, identifier);
	}

	/**
	 * @return the identifier
	 */
	public DomainObject getIdentifier() {
		return identifier;
	}

}
