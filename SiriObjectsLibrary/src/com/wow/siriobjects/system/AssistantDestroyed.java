package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class AssistantDestroyed extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2784980480790774929L;
	public static final String	clazz				= "AssistantDestroyed";
	public static final String	group				= "com.apple.ace.system";

	String						assistantId;

	public AssistantDestroyed() {

	}

	public AssistantDestroyed(String aceId, String refId, String assistantId) {
		super(clazz, group, aceId, refId, null);
		this.assistantId = assistantId;
	}

	public NSDictionary toPlist() {
		if (assistantId != null)
			properties.put("assistantId", new NSString(assistantId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static AssistantDestroyed fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		String assistantId = PropertyListUtils.getStringProperty(plist, "assistantId");

		return new AssistantDestroyed(aceId, refId, assistantId);
	}

}
