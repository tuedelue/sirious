package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class SetAssistantData extends ServerBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 491863765693750648L;
	public static final String	clazz				= "SetAssistantData";
	public static final String	group				= "com.apple.ace.system";

	boolean						censorSpeech;
	String						timeZoneId;
	String						language;
	String						region;
	List<Person>				meCards;
	List<Source>				abSources;

	public SetAssistantData(String aceId, String refId, boolean censorSpeech, String timeZoneId, String language, String region, List<Person> meCards, List<Source> abSources) {
		super(clazz, group, aceId, refId);
		this.censorSpeech = censorSpeech;
		this.timeZoneId = timeZoneId;
		this.language = language;
		this.region = region;
		this.meCards = meCards;
		this.abSources = abSources;
	}

	public NSDictionary toPlist() {
		properties.put("censorSpeech", new NSNumber(censorSpeech));
		if (timeZoneId != null)
			properties.put("timeZoneId", new NSString(timeZoneId));
		if (language != null)
			properties.put("language", new NSString(language));
		if (region != null)
			properties.put("region", new NSString(region));

		if (meCards != null) {
			properties.put("meCards", PropertyListUtils.convertPlistSerializableListToNSArray(meCards));
		}
		if (abSources != null) {
			properties.put("abSources", PropertyListUtils.convertPlistSerializableListToNSArray(abSources));
		}

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
