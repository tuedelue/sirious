package com.wow.siriobjects.system;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class SendCommands extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5221869282028756163L;
	public static final String	clazz				= "SendCommands";
	public static final String	group				= "com.apple.ace.system";

	List<StartRequest>			commands;

	public SendCommands() {

	}

	public SendCommands(String aceId, String refId, List<StartRequest> commands) {
		super(clazz, group, aceId, refId, null);
		this.commands = (commands != null) ? commands : new ArrayList<StartRequest>();
	}

	public NSDictionary toPlist() {
		if (commands != null)
			properties.put("commands", PropertyListUtils.convertPlistSerializableListToNSArray(commands));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SendCommands fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		List<StartRequest> commands = PropertyListUtils.getListObjects(plist, "commands");

		return new SendCommands(aceId, refId, commands);
	}

	/**
	 * @return the commands
	 */
	public List<StartRequest> getCommands() {
		return commands;
	}

}
