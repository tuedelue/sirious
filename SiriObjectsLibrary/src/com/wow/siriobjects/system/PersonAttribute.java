package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class PersonAttribute extends AceObject {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1628934887239065217L;
	public static final String	clazz				= "PersonAttribute";
	public static final String	group				= "com.apple.ace.system";

	DomainObject				object;
	String						displayText;
	String						data;

	public PersonAttribute() {

	}

	public PersonAttribute(DomainObject object, String displayText, String data) {
		super("PersonAttribute", "com.apple.ace.system");
		this.object = object;
		this.displayText = displayText;
		this.data = data;
	}

	public NSDictionary toPlist() {
		if (object != null)
			properties.put("object", object.toPlist());
		if (displayText != null)
			properties.put("displayText", new NSString(displayText));
		if (data != null)
			properties.put("data", new NSString(data));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static PersonAttribute fromPlist(NSDictionary plist) {
		String displayText = PropertyListUtils.getStringProperty(plist, "displayText");
		String data = PropertyListUtils.getStringProperty(plist, "data");
		DomainObject object = PropertyListUtils.getObject(plist, "object");

		return new PersonAttribute(object, displayText, data);
	}

	/**
	 * @return the object
	 */
	public DomainObject getObject() {
		return object;
	}

	/**
	 * @return the displayText
	 */
	public String getDisplayText() {
		return displayText;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

}
