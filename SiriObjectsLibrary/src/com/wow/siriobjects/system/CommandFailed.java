package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class CommandFailed extends ClientBoundCommand {
	private static final long	serialVersionUID	= -4856020252086574256L;
	public static final String	clazz				= "CommandFailed";
	public static final String	group				= "com.apple.ace.system";

	String						reason;
	int							errorCode;

	public CommandFailed() {

	}

	public CommandFailed(String aceId, String refId, String reason, int errorCode) {
		super(clazz, group, aceId, refId, null);
		this.reason = reason;
		this.errorCode = errorCode;
	}

	public NSDictionary toPlist() {
		if (reason != null)
			properties.put("reason", new NSString(reason));
		properties.put("errorCode", new NSNumber(errorCode));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static CommandFailed fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		String reason = PropertyListUtils.getStringProperty(plist, "reason");
		int errorCode = PropertyListUtils.getIntProperty(plist, "errorCode");

		return new CommandFailed(aceId, refId, reason, errorCode);
	}

}
