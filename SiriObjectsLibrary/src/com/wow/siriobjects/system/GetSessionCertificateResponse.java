package com.wow.siriobjects.system;

import com.dd.plist.NSData;
import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class GetSessionCertificateResponse extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5363796341010582682L;
	public static final String	clazz				= "GetSessionCertificateResponse";
	public static final String	group				= "com.apple.ace.system";

	byte[]						certificate;
	byte[]						caCert;
	byte[]						sessionCert;

	public GetSessionCertificateResponse() {
	}

	public GetSessionCertificateResponse(String aceId, String refId, byte[] certificate, byte[] caCert, byte[] sessionCert) {
		super(clazz, group, aceId, refId, null);
		this.certificate = certificate;
		this.caCert = caCert;
		this.sessionCert = sessionCert;
	}

	public NSDictionary toPlist() {
		if (certificate != null)
			properties.put("certificate", new NSData(certificate));
		if (caCert != null)
			properties.put("caCert", new NSData(caCert));
		if (sessionCert != null)
			properties.put("sessionCert", new NSData(sessionCert));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static GetSessionCertificateResponse fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		NSData dataObj;
		dataObj = (NSData) PropertyListUtils.getNSObjectProperty(plist, "certificate");
		byte[] certificate = (dataObj != null) ? dataObj.bytes() : null;

		dataObj = (NSData) PropertyListUtils.getNSObjectProperty(plist, "caCert");
		byte[] caCert = (dataObj != null) ? dataObj.bytes() : null;

		dataObj = (NSData) PropertyListUtils.getNSObjectProperty(plist, "sessionCert");
		byte[] sessionCert = (dataObj != null) ? dataObj.bytes() : null;

		return new GetSessionCertificateResponse(aceId, refId, certificate, caCert, sessionCert);
	}
}
