package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class StartRequest extends ServerBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2224805165346101319L;
	public static final String	clazz				= "StartRequest";
	public static final String	group				= "com.apple.ace.system";

	boolean						handsFree;
	String						utterance;

	public StartRequest() {

	}

	public StartRequest(String aceId, String refId, boolean handsFree, String utterance) {
		super(clazz, group, aceId, refId);
		this.handsFree = handsFree;
		this.utterance = utterance;
	}

	public static StartRequest fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		boolean handsFree = PropertyListUtils.getBooleanProperty(plist, "handsFree");
		String utterance = PropertyListUtils.getStringProperty(plist, "utterance");

		return new StartRequest(aceId, refId, handsFree, utterance);
	}

	public NSDictionary toPlist() {
		properties.put("handsFree", new NSNumber(handsFree));
		properties.put("utterance", new NSString(utterance));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the handsFree
	 */
	public boolean isHandsFree() {
		return handsFree;
	}

	/**
	 * @return the utterance
	 */
	public String getUtterance() {
		return utterance;
	}

}
