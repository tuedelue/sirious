package com.wow.siriobjects.system;

public interface OnDomainObjectChangedListener {
	public void handleChange(DomainObject obj);
}
