package com.wow.siriobjects.system;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class ResultCallback extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1089642065553385476L;
	public static final String	clazz				= "ResultCallback";
	public static final String	group				= "com.apple.ace.system";

	List<ClientBoundCommand>	commands;
	int							code				= 0;

	public ResultCallback() {
	}

	public ResultCallback(String aceId, String refId, List<ClientBoundCommand> commands, List<ClientBoundCommand> callbacks, int code) {
		super(clazz, group, aceId, refId, callbacks);
		this.commands = (commands != null) ? commands : new ArrayList<ClientBoundCommand>();
		this.code = code;
	}

	public NSDictionary toPlist() {
		if (commands != null)
			properties.put("commands", PropertyListUtils.convertPlistSerializableListToNSArray(commands));
		properties.put("code", new NSNumber(code));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static ResultCallback fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		int code = PropertyListUtils.getIntProperty(plist, "code");

		List<ClientBoundCommand> commands = PropertyListUtils.getListObjects(plist, "commands");
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		return new ResultCallback(aceId, refId, commands, callbacks, code);
	}

	/**
	 * @return the commands
	 */
	public List<ClientBoundCommand> getCommands() {
		return commands;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

}
