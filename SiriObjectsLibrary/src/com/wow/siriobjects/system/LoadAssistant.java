package com.wow.siriobjects.system;

import com.dd.plist.NSData;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ServerBoundCommand;


public class LoadAssistant extends ServerBoundCommand {
	public static final String	clazz	= "LoadAssistant";
	public static final String	group	= "com.apple.ace.system";

	String						assistantId;
	String						speechId;
	byte[]						sessionValidationData;

	public LoadAssistant(String aceId, String assistantId, String speechId, byte[] sessionValidationData) {
		super(clazz, group, aceId, null);
		this.assistantId = assistantId;
		this.speechId = speechId;
		this.sessionValidationData = sessionValidationData;
	}

	public NSDictionary toPlist() {
		if (sessionValidationData != null)
			properties.put("sessionValidationData", new NSData(sessionValidationData));
		if (assistantId != null)
			properties.put("assistantId", new NSString(assistantId));
		if (speechId != null)
			properties.put("speechId", new NSString(speechId));
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
