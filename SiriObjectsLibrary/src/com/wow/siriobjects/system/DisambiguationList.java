package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.AceView;


public class DisambiguationList extends AceView {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 856261803089256272L;
	public static final String	clazz				= "DisambiguationList";
	public static final String	group				= "com.apple.ace.assistant";

	public DisambiguationList() {

	}

	List<ListItem>	items;
	String			speakableSelectionResponse;
	String			speakableFinalDelimitter;
	String			speakableDelimitter;
	String			selectionResponse;

	public DisambiguationList(String viewId, String speakableText, boolean listenAfterSpeaking, List<ListItem> items, String speakableSelectionResponse, String speakableFinalDelimitter,
			String speakableDelimitter, String selectionResponse) {
		super(viewId, speakableText, listenAfterSpeaking);
		this.items = items;
		this.speakableSelectionResponse = speakableSelectionResponse;
		this.speakableFinalDelimitter = speakableFinalDelimitter;
		this.speakableDelimitter = speakableDelimitter;
		this.selectionResponse = selectionResponse;
	}

	public NSDictionary toPlist() {
		if (items != null)
			properties.put("items", PropertyListUtils.convertPlistSerializableListToNSArray(items));
		if (speakableDelimitter != null)
			properties.put("speakableDelimitter", new NSString(speakableDelimitter));
		if (speakableSelectionResponse != null)
			properties.put("speakableSelectionResponse", new NSString(speakableSelectionResponse));
		if (speakableDelimitter != null)
			properties.put("speakableFinalDemitter", new NSString(speakableFinalDelimitter));
		if (selectionResponse != null)
			properties.put("selectionResponse", new NSString(selectionResponse));

		return super.toPlist();
	}

	public static DisambiguationList fromPlist(NSDictionary plist) {
		boolean listenAfterSpeaking = PropertyListUtils.getBooleanProperty(plist, "listenAfterSpeaking");
		String speakableText = PropertyListUtils.getStringProperty(plist, "speakableText");
		String viewId = PropertyListUtils.getStringProperty(plist, "viewId");
		List<ListItem> items = PropertyListUtils.getListObjects(plist, "items");
		String selectionResponse = PropertyListUtils.getStringProperty(plist, "selectionResponse");
		String speakableDelimitter = PropertyListUtils.getStringProperty(plist, "speakableDelimitter");
		String speakableFinalDelimitter = PropertyListUtils.getStringProperty(plist, "speakableFinalDelimitter");
		String speakableSelectionResponse = PropertyListUtils.getStringProperty(plist, "speakableSelectionResponse");

		return new DisambiguationList(viewId, speakableText, listenAfterSpeaking, items, speakableSelectionResponse, speakableFinalDelimitter, speakableDelimitter, speakableSelectionResponse);
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the items
	 */
	public List<ListItem> getItems() {
		return items;
	}

	/**
	 * @return the speakableSelectionResponse
	 */
	public String getSpeakableSelectionResponse() {
		return speakableSelectionResponse;
	}

	/**
	 * @return the speakableFinalDemitter
	 */
	public String getSpeakableFinalDelimitter() {
		return speakableFinalDelimitter;
	}

	/**
	 * @return the speakableDemitter
	 */
	public String getSpeakableDelimitter() {
		return speakableDelimitter;
	}

	/**
	 * @return the selectionResponse
	 */
	public String getSelectionResponse() {
		return selectionResponse;
	}

}
