package com.wow.siriobjects.system;

import java.util.Date;
import java.util.List;

import com.dd.plist.NSDate;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.email.Email;


public class Person extends DomainObject {
	private static final long	serialVersionUID	= 7101894998296557576L;
	public static final String	clazz				= "Person";
	public static final String	group				= "com.apple.ace.system";

	String						suffix;
	List<RelatedName>			relatedNames;
	String						prefix;
	List<Phone>					phones;
	String						nickName;
	String						middleName;
	boolean						me;
	String						lastNamePhonetic;
	String						lastName;
	String						fullName;
	String						forstNamePhonetic;
	String						firstName;
	List<Email>					emails;
	String						company;
	Date						birthday;
	List<Location>				addresses;

	/**
	 * @param identifier
	 * @param suffix
	 * @param relatedNames
	 * @param prefix
	 * @param phones
	 * @param nickName
	 * @param middleName
	 * @param me
	 * @param lastNamePhonetic
	 * @param lastName
	 * @param fullName
	 * @param forstNamePhonetic
	 * @param firstName
	 * @param emails
	 * @param company
	 * @param birthday
	 * @param addresses
	 */
	public Person(String identifier, String suffix, List<RelatedName> relatedNames, String prefix, List<Phone> phones, String nickName, String middleName, boolean me, String lastNamePhonetic,
			String lastName, String fullName, String forstNamePhonetic, String firstName, List<Email> emails, String company, Date birthday, List<Location> addresses) {
		super(clazz, group, identifier);
		this.suffix = suffix;
		this.relatedNames = relatedNames;
		this.prefix = prefix;
		this.phones = phones;
		this.nickName = nickName;
		this.middleName = middleName;
		this.me = me;
		this.lastNamePhonetic = lastNamePhonetic;
		this.lastName = lastName;
		this.fullName = fullName;
		this.forstNamePhonetic = forstNamePhonetic;
		this.firstName = firstName;
		this.emails = emails;
		this.company = company;
		this.birthday = birthday;
		this.addresses = addresses;
	}

	public Person() {
		super(clazz, group);
	}

	public NSDictionary toPlist() {
		if (addresses != null)
			properties.put("addresses", PropertyListUtils.convertPlistSerializableListToNSArray(addresses));
		if (birthday != null)
			properties.put("birthday", new NSDate(birthday));
		if (company != null)
			properties.put("company", new NSString(company));
		if (emails != null)
			properties.put("emails", PropertyListUtils.convertPlistSerializableListToNSArray(emails));
		if (firstName != null)
			properties.put("firstName", new NSString(firstName));
		if (forstNamePhonetic != null)
			properties.put("forstNamePhentic", new NSString(forstNamePhonetic));
		if (fullName != null)
			properties.put("fullName", new NSString(fullName));
		if (lastName != null)
			properties.put("lastName", new NSString(lastName));
		if (lastNamePhonetic != null)
			properties.put("lastNamePhonetic", new NSString(lastNamePhonetic));
		properties.put("me", new NSNumber(me));
		if (middleName != null)
			properties.put("middleName", new NSString(middleName));
		if (nickName != null)
			properties.put("nickName", new NSString(nickName));
		if (phones != null)
			properties.put("phones", PropertyListUtils.convertPlistSerializableListToNSArray(phones));
		if (prefix != null)
			properties.put("prefix", new NSString(prefix));
		if (relatedNames != null)
			properties.put("relatedNames", PropertyListUtils.convertPlistSerializableListToNSArray(relatedNames));
		if (suffix != null)
			properties.put("suffix", new NSString(suffix));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static Person fromPlist(NSDictionary plist) {
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");

		List<Location> addresses = PropertyListUtils.getListObjects(plist, "addresses");
		Date birthday = PropertyListUtils.getDateProperty(plist, "birthday");
		String company = PropertyListUtils.getStringProperty(plist, "company");
		List<Email> emails = PropertyListUtils.getListObjects(plist, "emails");
		String firstName = PropertyListUtils.getStringProperty(plist, "firstName");
		String forstNamePhonetic = PropertyListUtils.getStringProperty(plist, "forstNamePhonetic");
		String fullName = PropertyListUtils.getStringProperty(plist, "fullName");
		String lastName = PropertyListUtils.getStringProperty(plist, "lastName");
		String lastNamePhonetic = PropertyListUtils.getStringProperty(plist, "lastNamePhonetic");
		boolean me = PropertyListUtils.getBooleanProperty(plist, "me");
		String middleName = PropertyListUtils.getStringProperty(plist, "middleName");
		String nickName = PropertyListUtils.getStringProperty(plist, "nickName");
		List<Phone> phones = PropertyListUtils.getListObjects(plist, "phones");
		String prefix = PropertyListUtils.getStringProperty(plist, "prefix");
		List<RelatedName> relatedNames = PropertyListUtils.getListObjects(plist, "relatedNames");
		String suffix = PropertyListUtils.getStringProperty(plist, "suffix");

		return new Person(identifier, suffix, relatedNames, prefix, phones, nickName, middleName, me, lastNamePhonetic, lastName, fullName, forstNamePhonetic, firstName, emails, company, birthday,
				addresses);
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @return the relatedNames
	 */
	public List<RelatedName> getRelatedNames() {
		return relatedNames;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @return the phones
	 */
	public List<Phone> getPhones() {
		return phones;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return the me
	 */
	public boolean isMe() {
		return me;
	}

	/**
	 * @return the lastNamePhonetic
	 */
	public String getLastNamePhonetic() {
		return lastNamePhonetic;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @return the forstNamePhonetic
	 */
	public String getForstNamePhonetic() {
		return forstNamePhonetic;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the emails
	 */
	public List<Email> getEmails() {
		return emails;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @return the addresses
	 */
	public List<Location> getAddresses() {
		return addresses;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}
}
