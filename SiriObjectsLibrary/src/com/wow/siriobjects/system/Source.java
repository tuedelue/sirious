package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;


public class Source extends DomainObject {
	private static final long	serialVersionUID	= 6668299441584361041L;
	public static final String	clazz				= "Source";
	public static final String	group				= "com.apple.ace.system";

	String						accountIdentifier;
	String						accountName;
	String						domainIdentifier	= "com.apple.ace.contact";
	boolean						remote;

	public Source() {
		super(clazz, group);
	}

	public Source(String identifier, String accountIdentifier, String accountName, String domainIdentifier, boolean remote) {
		super(clazz, group, identifier);
		this.accountIdentifier = accountIdentifier;
		this.domainIdentifier = domainIdentifier;
		this.accountName = accountName;
		this.remote = remote;
	}

	public static Source fromPlist(NSDictionary plist) {
		String accountIdentifier = PropertyListUtils.getStringProperty(plist, "accountIdentifier");
		String accountName = PropertyListUtils.getStringProperty(plist, "accountName");
		boolean remote = PropertyListUtils.getBooleanProperty(plist, "remote");
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");

		return new Source(identifier, accountIdentifier, accountName, null, remote);
	}

	public NSDictionary toPlist() {
		if (accountIdentifier != null)
			properties.put("accountIdentifier", new NSString(accountIdentifier));
		if (accountName != null)
			properties.put("accountName", new NSString(accountName));
		if (domainIdentifier != null)
			properties.put("domainIdentifier", new NSString(domainIdentifier));
		properties.put("remote", new NSNumber(remote));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}
}
