package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;


public class RelatedName extends DomainObject {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3412699246805649789L;
	public static final String	clazz				= "RelatedName";
	public static final String	group				= "com.apple.ace.system";
	String						name;
	String						label;

	public RelatedName() {
		super(clazz, group);

	}

	public RelatedName(String name, String label) {
		super(clazz, group, null);
		this.name = name;
		this.label = label;
	}

	public NSDictionary toPlist() {
		NSDictionary plist = new NSDictionary();

		if (name != null)
			plist.put("name", new NSString(name));
		if (label != null)
			plist.put("label", new NSString(label));

		return plist;
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static RelatedName fromPlist(NSDictionary plist) {
		String name = PropertyListUtils.getStringProperty(plist, "name");
		String label = PropertyListUtils.getStringProperty(plist, "label");

		return new RelatedName(name, label);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
