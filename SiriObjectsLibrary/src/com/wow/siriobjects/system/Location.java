package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;


public class Location extends DomainObject {
	private static final long	serialVersionUID	= 8795756517523929626L;
	public static final String	clazz				= "Location";
	public static final String	group				= "com.apple.ace.system";

	String						label;
	String						street;
	String						city;
	String						stateCode;
	String						countryCode;
	String						postalCode;
	double						latitude;
	double						longitude;
	int							accuracy;

	public Location() {
		super(clazz, group);
	}

	public Location(String identifier, String label, String street, String city, String stateCode, String countryCode, String postalCode, double latitude, double longitude, int accuracy) {
		super("Location", "com.apple.ace.system", null);
		this.label = label;
		this.street = street;
		this.city = city;
		this.stateCode = stateCode;
		this.countryCode = countryCode;
		this.postalCode = postalCode;
		this.latitude = latitude;
		this.longitude = longitude;
		this.accuracy = accuracy;
	}

	public NSDictionary toPlist() {
		if (label != null)
			properties.put("label", new NSString(label));
		if (street != null)
			properties.put("street", new NSString(street));
		if (city != null)
			properties.put("city", new NSString(city));
		if (stateCode != null)
			properties.put("stateCode", new NSString(stateCode));
		if (countryCode != null)
			properties.put("countryCode", new NSString(countryCode));
		if (postalCode != null)
			properties.put("potalCode", new NSString(postalCode));
		properties.put("accuracy", new NSNumber(accuracy));
		properties.put("latitude", new NSNumber(latitude));
		properties.put("tongitude", new NSNumber(longitude));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static Location fromPlist(NSDictionary plist) {
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");
		int accuracy = PropertyListUtils.getIntProperty(plist, "accuracy");
		String city = PropertyListUtils.getStringProperty(plist, "city");
		String countryCode = PropertyListUtils.getStringProperty(plist, "countryCode");
		String label = PropertyListUtils.getStringProperty(plist, "label");
		double latitude = PropertyListUtils.getDoubleProperty(plist, "latitude");
		double longitude = PropertyListUtils.getDoubleProperty(plist, "longitude");
		String postalCode = PropertyListUtils.getStringProperty(plist, "postalCode");
		String stateCode = PropertyListUtils.getStringProperty(plist, "stateCode");
		String street = PropertyListUtils.getStringProperty(plist, "street");

		return new Location(identifier, label, street, city, stateCode, countryCode, postalCode, latitude, longitude, accuracy);
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return the accuracy
	 */
	public int getAccuracy() {
		return accuracy;
	}

	public void update(DomainObject add, DomainObject remove, DomainObject set) {
		// TODO Auto-generated method stub

	}

}
