package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class AssistantNotFound extends ClientBoundCommand {
	private static final long	serialVersionUID	= -7792757918184548510L;
	public static final String	clazz				= "AssistantNotFound";
	public static final String	group				= "com.apple.ace.system";

	public AssistantNotFound() {
	}

	public AssistantNotFound(String aceId, String refId) {
		super(clazz, group, aceId, refId, null);
	}

	public static AssistantNotFound fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		return new AssistantNotFound(aceId, refId);
	}
}
