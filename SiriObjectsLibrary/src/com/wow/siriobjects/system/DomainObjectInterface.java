package com.wow.siriobjects.system;

public interface DomainObjectInterface {
	public void update(DomainObject add, DomainObject remove, DomainObject set);

	public void setIdentifier(String id);
}
