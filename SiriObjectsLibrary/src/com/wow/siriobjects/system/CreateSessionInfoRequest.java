package com.wow.siriobjects.system;

import com.dd.plist.NSData;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ServerBoundCommand;


public class CreateSessionInfoRequest extends ServerBoundCommand {
	private static final long	serialVersionUID	= -5774489604555981751L;
	public static final String	clazz				= "CreateSessionInfoRequest";
	public static final String	group				= "com.apple.ace.system";

	byte[]						sessionInfo;
	int							validityDuration;
	String						squirrelAuth;

	public CreateSessionInfoRequest() {

	}

	public CreateSessionInfoRequest(String aceId, String refId, byte[] sessionInfo, int validityDuration, String squirrelAuth) {
		super(clazz, group, aceId, refId);
		this.sessionInfo = sessionInfo;
		this.validityDuration = validityDuration;
		this.squirrelAuth = squirrelAuth;
	}

	public NSDictionary toPlist() {
		if (sessionInfo != null)
			properties.put("sessionInfo", new NSData(sessionInfo));
		properties.put("validityDuration", new NSNumber(validityDuration));
		if (squirrelAuth != null)
			properties.put("squirrelAuth", new NSString(squirrelAuth));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the sessionInfo
	 */
	public byte[] getSessionInfo() {
		return sessionInfo;
	}

	/**
	 * @return the validityDuration
	 */
	public int getValidityDuration() {
		return validityDuration;
	}

	/**
	 * @return the squirrelAuth
	 */
	public String getSquirrelAuth() {
		return squirrelAuth;
	}
}
