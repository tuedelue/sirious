package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSData;
import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class SetActivationToken extends ClientBoundCommand {
	private static final long	serialVersionUID	= -647581289822514219L;
	public static final String	clazz				= "SetActivationToken";
	public static final String	group				= "com.apple.ace.system";

	byte[]						activationToken;

	public SetActivationToken() {
	}

	public SetActivationToken(String aceId, String refId, List<ClientBoundCommand> callbacks, byte[] activationToken) {
		super(clazz, group, aceId, refId, callbacks);
		this.activationToken = activationToken;
	}

	public NSDictionary toPlist() {
		if (activationToken != null)
			properties.put("activationToken", new NSData(activationToken));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static SetActivationToken fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		byte[] activationToken = PropertyListUtils.getDataProperty(plist, "activationToken");

		return new SetActivationToken(aceId, refId, callbacks, activationToken);
	}

	/**
	 * @return the activationToken
	 */
	public byte[] getActivationToken() {
		return activationToken;
	}

}
