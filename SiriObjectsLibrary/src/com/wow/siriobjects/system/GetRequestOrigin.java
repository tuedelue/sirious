package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class GetRequestOrigin extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID				= -7446636389057329439L;
	public static final String	group							= "com.apple.ace.system";
	public static final String	clazz							= "GetRequestOrigin";

	final String				desiredAccuracyThreeKilometers	= "ThreeKilometers";
	final String				desiredAccuracyKilometer		= "Kilometer";
	final String				desiredAccuracyHundredMeters	= "HundredMeters";
	final String				desiredAccuracyNearestTenMeters	= "NearestTenMeters";
	final String				desiredAccuracyBest				= "Best";

	String						desiredAccuracy					= desiredAccuracyHundredMeters;
	int							searchTimeout;
	int							maxAge;

	public GetRequestOrigin() {

	}

	public GetRequestOrigin(String aceId, String refId, String desiredAccuracy, int searchTimeout, int maxAge) {
		super(clazz, group, aceId, refId, null);
		this.desiredAccuracy = desiredAccuracy;
		this.searchTimeout = searchTimeout;
		this.maxAge = maxAge;
	}

	public NSDictionary toPlist() {
		if (desiredAccuracy != null)
			properties.put("desiredAccuracy", new NSString(desiredAccuracy));
		properties.put("searchTimeout", new NSNumber(searchTimeout));
		properties.put("maxAge", new NSNumber(maxAge));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static GetRequestOrigin fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		String desiredAccuracy = PropertyListUtils.getStringProperty(plist, "desiredAccuracy");
		int maxAge = PropertyListUtils.getIntProperty(plist, "maxAge");
		int searchTimeout = PropertyListUtils.getIntProperty(plist, "searchTimeout");

		return new GetRequestOrigin(aceId, refId, desiredAccuracy, searchTimeout, maxAge);
	}

	/**
	 * @return the desiredAccuracyThreeKilometers
	 */
	public String getDesiredAccuracyThreeKilometers() {
		return desiredAccuracyThreeKilometers;
	}

	/**
	 * @return the desiredAccuracyKilometer
	 */
	public String getDesiredAccuracyKilometer() {
		return desiredAccuracyKilometer;
	}

	/**
	 * @return the desiredAccuracyHundredMeters
	 */
	public String getDesiredAccuracyHundredMeters() {
		return desiredAccuracyHundredMeters;
	}

	/**
	 * @return the desiredAccuracyNearestTenMeters
	 */
	public String getDesiredAccuracyNearestTenMeters() {
		return desiredAccuracyNearestTenMeters;
	}

	/**
	 * @return the desiredAccuracyBest
	 */
	public String getDesiredAccuracyBest() {
		return desiredAccuracyBest;
	}

	/**
	 * @return the desiredAccuracy
	 */
	public String getDesiredAccuracy() {
		return desiredAccuracy;
	}

	/**
	 * @return the searchTimeout
	 */
	public int getSearchTimeout() {
		return searchTimeout;
	}

	/**
	 * @return the maxAge
	 */
	public int getMaxAge() {
		return maxAge;
	}

}
