package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ServerBoundCommand;


public class SetRequestOrigin extends ServerBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6642429949721490093L;
	public static final String	clazz				= "SetRequestOrigin";
	public static final String	group				= "com.apple.ace.system";

	final static String			statusValid			= "Valid";
	final static String			statusTimeout		= "Timeout";
	final static String			statusUnknown		= "Unknown";
	final static String			statusDenied		= "Denied";
	final static String			statusDisabled		= "Disabled";

	long						timestamp;
	String						status;
	double						speed;
	double						direction;
	String						desiredAccuracy;
	double						altitude;
	long						age;
	String						horizontalAccuracy;
	String						verticalAccuracy;
	double						longitude;
	double						latitude;

	public SetRequestOrigin(String aceId, String refId, long timestamp, String status, double speed, double direction, String desiredAccuracy, double altitude, long age, String horizontalAccuracy,
			String verticalAccuracy, double longitude, double latitude) {
		super(clazz, group, aceId, refId);
		this.timestamp = timestamp;
		this.status = status;
		this.speed = speed;
		this.direction = direction;
		this.desiredAccuracy = desiredAccuracy;
		this.altitude = altitude;
		this.age = age;
		this.horizontalAccuracy = horizontalAccuracy;
		this.verticalAccuracy = verticalAccuracy;
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public NSDictionary toPlist() {
		if (desiredAccuracy != null)
			properties.put("desiredAccuracy", new NSString(desiredAccuracy));
		if (status != null)
			properties.put("status", new NSString(status));
		if (horizontalAccuracy != null)
			properties.put("horizontalAccuracy", new NSString(horizontalAccuracy));
		if (verticalAccuracy != null)
			properties.put("verticalAccuracy", new NSString(verticalAccuracy));
		properties.put("longitude", new NSNumber(longitude));
		properties.put("latitude", new NSNumber(latitude));
		properties.put("altitude", new NSNumber(altitude));
		properties.put("direction", new NSNumber(direction));
		properties.put("speed", new NSNumber(speed));
		properties.put("age", new NSNumber(age));
		properties.put("timestamp", new NSNumber(timestamp));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	/**
	 * @return the statusValid
	 */
	public static String getStatusValid() {
		return statusValid;
	}

	/**
	 * @return the statusTimeout
	 */
	public static String getStatusTimeout() {
		return statusTimeout;
	}

	/**
	 * @return the statusUnknown
	 */
	public static String getStatusUnknown() {
		return statusUnknown;
	}

	/**
	 * @return the statusDenied
	 */
	public static String getStatusDenied() {
		return statusDenied;
	}

	/**
	 * @return the statusDisabled
	 */
	public static String getStatusDisabled() {
		return statusDisabled;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @return the speed
	 */
	public double getSpeed() {
		return speed;
	}

	/**
	 * @return the direction
	 */
	public double getDirection() {
		return direction;
	}

	/**
	 * @return the desiredAccuracy
	 */
	public String getDesiredAccuracy() {
		return desiredAccuracy;
	}

	/**
	 * @return the altitude
	 */
	public double getAltitude() {
		return altitude;
	}

	/**
	 * @return the age
	 */
	public long getAge() {
		return age;
	}

	/**
	 * @return the horizontalAccuracy
	 */
	public String getHorizontalAccuracy() {
		return horizontalAccuracy;
	}

	/**
	 * @return the verticalAccuracy
	 */
	public String getVerticalAccuracy() {
		return verticalAccuracy;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

}
