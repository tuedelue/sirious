package com.wow.siriobjects.system;

import com.dd.plist.NSData;
import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ServerBoundCommand;


public class CreateAssistant extends ServerBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4730345654715117597L;
	public static final String	clazz				= "CreateAssistant";
	public static final String	group				= "com.apple.ace.system";

	byte[]						validationData;

	public CreateAssistant(String aceId, byte[] validationData) {
		super(clazz, group, aceId, null);
		this.validationData = validationData;
	}

	public NSDictionary toPlist() {
		if (validationData != null)
			properties.put("validationData", new NSData(validationData));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
