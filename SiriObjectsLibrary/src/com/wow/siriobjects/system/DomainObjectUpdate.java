package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class DomainObjectUpdate extends ClientBoundCommand {
	private static final long	serialVersionUID	= -8726962689440917600L;
	public static final String	clazz				= "DomainObjectUpdate";
	public static final String	group				= "com.apple.ace.system";

	DomainObject				identifier;
	DomainObject				addFields;
	DomainObject				setFields;
	DomainObject				removeFields;

	public DomainObjectUpdate() {
	}

	public DomainObjectUpdate(String aceId, String refId, List<ClientBoundCommand> callbacks, DomainObject identifier, DomainObject addFields, DomainObject setFields, DomainObject removeFields) {
		super(clazz, group, aceId, refId, callbacks);

		this.identifier = identifier;

		this.addFields = addFields;
		this.setFields = setFields;
		this.removeFields = removeFields;
	}

	public NSDictionary toPlist() {
		if (addFields != null)
			properties.put("addFields", addFields.toPlist());
		if (removeFields != null)
			properties.put("removeFields", removeFields.toPlist());
		if (setFields != null)
			properties.put("setFields", setFields.toPlist());
		if (identifier != null)
			properties.put("identifier", identifier.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectUpdate fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);
		DomainObject addFields = PropertyListUtils.getObject(plist, "addFields");
		DomainObject setFields = PropertyListUtils.getObject(plist, "setFields");
		DomainObject removeFields = PropertyListUtils.getObject(plist, "removeFields");
		DomainObject identifier = PropertyListUtils.getObject(plist, "identifier");

		return new DomainObjectUpdate(aceId, refId, callbacks, identifier, addFields, setFields, removeFields);
	}

	/**
	 * @return the identifier
	 */
	public DomainObject getIdentifier() {
		return identifier;
	}

	/**
	 * @return the addFields
	 */
	public DomainObject getAddFields() {
		return addFields;
	}

	/**
	 * @return the setFields
	 */
	public DomainObject getSetFields() {
		return setFields;
	}

	/**
	 * @return the removeFields
	 */
	public DomainObject getRemoveFields() {
		return removeFields;
	}
}
