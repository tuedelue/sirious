package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ServerBoundCommand;


public class DomainObjectCancelCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= -1439610932836643892L;
	public static final String	clazz				= "DomainObjectCancelCompleted";
	public static final String	group				= "com.apple.ace.system";

	String						identifier;

	public DomainObjectCancelCompleted() {
	}

	public DomainObjectCancelCompleted(String aceId, String refId, String identifier) {
		super(clazz, group, aceId, refId);
		this.identifier = identifier;
	}

	public NSDictionary toPlist() {
		properties.put("identifier", new NSString(identifier));
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

}
