package com.wow.siriobjects.system;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class DomainObjectCreate extends ClientBoundCommand {
	private static final long	serialVersionUID	= -8800954946218611801L;
	public static final String	clazz				= "DomainObjectCreate";
	public static final String	group				= "com.apple.ace.system";

	DomainObject				object;

	public DomainObjectCreate() {

	}

	public DomainObjectCreate(String aceId, String refId, List<ClientBoundCommand> callbacks, DomainObject object) {
		super(clazz, group, aceId, refId, callbacks);
		this.object = object;
	}

	public NSDictionary toPlist() {
		if (object != null)
			properties.put("object", object.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectCreate fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		DomainObject object = PropertyListUtils.getObject(plist, "object");

		return new DomainObjectCreate(aceId, refId, callbacks, object);
	}

	/**
	 * @return the object
	 */
	public DomainObject getObject() {
		return object;
	}

}
