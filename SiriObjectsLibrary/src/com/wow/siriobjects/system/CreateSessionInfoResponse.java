package com.wow.siriobjects.system;

import com.dd.plist.NSData;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class CreateSessionInfoResponse extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -4659583776154327326L;
	public static final String	clazz				= "CreateSessionInfoResponse";
	public static final String	group				= "com.apple.ace.system";

	int							validityDuration;
	byte[]						sessionInfo;

	public CreateSessionInfoResponse() {

	}

	public CreateSessionInfoResponse(String aceId, String refId, int validityDuration, byte[] sessionInfo) {
		super(clazz, group, aceId, refId, null);
		this.validityDuration = validityDuration;
		this.sessionInfo = sessionInfo;
	}

	public NSDictionary toPlist() {
		if (sessionInfo != null)
			properties.put("sessionInfo", new NSData(sessionInfo));
		properties.put("validityDuration", new NSNumber(validityDuration));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static CreateSessionInfoResponse fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		int validityDuration = PropertyListUtils.getIntProperty(plist, "validityDuration");
		byte[] sessionInfo = PropertyListUtils.getDataProperty(plist, "sessionInfo");

		return new CreateSessionInfoResponse(aceId, refId, validityDuration, sessionInfo);
	}

}
