package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class DomainObjectCommitCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= 7252282640319185026L;
	public static final String	clazz				= "DomainObjectCommitCompleted";
	public static final String	group				= "com.apple.ace.system";

	String						identifier;

	public DomainObjectCommitCompleted() {
	}

	public DomainObjectCommitCompleted(String aceId, String refId, String identifier) {
		super(clazz, group, aceId, refId);
		this.identifier = identifier;
	}

	public NSDictionary toPlist() {
		properties.put("identifier", new NSString(identifier));
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectCommitCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");

		return new DomainObjectCommitCompleted(aceId, refId, identifier);
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

}
