package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class DomainObjectCreateCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= -5064380984538832776L;
	public static final String	clazz				= "DomainObjectCreateCompleted";
	public static final String	group				= "com.apple.ace.system";

	String						identifier;

	public DomainObjectCreateCompleted() {

	}

	public DomainObjectCreateCompleted(String aceId, String refId, String identifier) {
		super(clazz, group, aceId, refId);
		this.identifier = identifier;
	}

	public NSDictionary toPlist() {
		if (identifier != null)
			properties.put("identifier", new NSString(identifier));
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static DomainObjectCreateCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);

		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");

		return new DomainObjectCreateCompleted(aceId, refId, identifier);
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

}
