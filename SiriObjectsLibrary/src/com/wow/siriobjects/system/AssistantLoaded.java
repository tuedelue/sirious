package com.wow.siriobjects.system;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class AssistantLoaded extends ClientBoundCommand {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2257612622955058990L;
	public static final String	clazz				= "AssistantLoaded";
	public static final String	group				= "com.apple.ace.system";

	String						version;
	boolean						requestSync;
	String						dataAnchor;

	public AssistantLoaded() {
	}

	public AssistantLoaded(String aceId, String refId, String version, boolean requestSync, String dataAnchor) {
		super(clazz, group, aceId, refId, null);
		this.version = version;
		this.requestSync = requestSync;
		this.dataAnchor = dataAnchor;
	}

	public NSDictionary toPlist() {
		if (version != null)
			properties.put("version", new NSString(version));
		properties.put("requestSync", new NSNumber(requestSync));
		if (dataAnchor != null)
			properties.put("dataAnchor", new NSString(dataAnchor));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static AssistantLoaded fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		String version = PropertyListUtils.getStringProperty(plist, "version");
		boolean requestSync = PropertyListUtils.getBooleanProperty(plist, "requestSync");
		String dataAnchor = PropertyListUtils.getStringProperty(plist, "dataAnchor");

		return new AssistantLoaded(aceId, refId, version, requestSync, dataAnchor);
	}
}
