package com.wow.siriobjects.system;

import com.wow.siriobjects.ServerBoundCommand;

public class GetSessionCertificate extends ServerBoundCommand {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -224357289040756089L;
	public static final String	group				= "com.apple.ace.system";
	public static final String	clazz				= "GetSessionCertificate";

	public GetSessionCertificate(String aceId) {
		super(clazz, group, aceId, null);
	}

}
