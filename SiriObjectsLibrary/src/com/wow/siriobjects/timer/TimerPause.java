package com.wow.siriobjects.timer;

import com.wow.siriobjects.ClientBoundCommand;

public class TimerPause extends ClientBoundCommand {

	public TimerPause(String refId) {
		super("Pause", "com.apple.ace.timer", null, refId, null);
	}

}
