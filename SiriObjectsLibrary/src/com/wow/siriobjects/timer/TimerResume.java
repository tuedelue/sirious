package com.wow.siriobjects.timer;

import com.wow.siriobjects.ClientBoundCommand;

public class TimerResume extends ClientBoundCommand {

	public TimerResume(String refId) {
		super("Resume", "com.apple.ace.timer", null, refId, null);
	}

}
