package com.wow.siriobjects.timer;

import com.wow.siriobjects.ClientBoundCommand;

public class TimerCancel extends ClientBoundCommand {

	public TimerCancel(String refId) {
		super("Cancel", "com.apple.ace.timer", null, refId, null);
	}

}
