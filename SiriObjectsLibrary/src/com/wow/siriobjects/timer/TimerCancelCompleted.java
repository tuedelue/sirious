package com.wow.siriobjects.timer;

import com.wow.siriobjects.ServerBoundCommand;

public class TimerCancelCompleted extends ServerBoundCommand {
	TimerObject timer;

	public TimerCancelCompleted(TimerObject timer) {
		super("CancelCompleted", "com.apple.ace.timer", null, null);
		this.timer = timer;
	}

}
