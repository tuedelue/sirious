package com.wow.siriobjects.timer;

import com.wow.siriobjects.ClientBoundCommand;

public class TimerGet extends ClientBoundCommand {

	public TimerGet(String refId) {
		super("Get", "com.apple.ace.timer", null, refId, null);
	}

}
