package com.wow.siriobjects.timer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.siriobjects.ui.Snippet;


public class TimerSnippet extends Snippet {
	public static final String	clazz	= "TimerSnippet";
	public static final String	group	= "com.apple.ace.timer";

	List<TimerObject>			timers;

	public TimerSnippet(ConfirmationOptions confirmationOptions, List<TimerObject> timers) {
		super(group, UUID.randomUUID().toString(), null, false, null, confirmationOptions);
		this.timers = (timers != null) ? timers : new ArrayList<TimerObject>();
	}

}
