package com.wow.siriobjects.timer;

import com.wow.siriobjects.ServerBoundCommand;

public class TimerGetCompleted extends ServerBoundCommand {
	TimerObject timer;

	public TimerGetCompleted(TimerObject timer) {
		super("GetCompleted", "com.apple.ace.timer", null, null);
		this.timer = timer;
	}

}
