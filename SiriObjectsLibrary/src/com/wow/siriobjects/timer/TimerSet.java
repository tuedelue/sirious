package com.wow.siriobjects.timer;

import com.wow.siriobjects.ClientBoundCommand;

public class TimerSet extends ClientBoundCommand {
	TimerObject timer;

	public TimerSet(String refId, TimerObject timer) {
		super("Set", "com.apple.ace.timer", null, refId, null);
		this.timer = timer;
	}

}
