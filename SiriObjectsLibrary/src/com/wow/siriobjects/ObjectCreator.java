package com.wow.siriobjects;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;

public class ObjectCreator {
	public static final String							CLASS_FIELD_NAME		= "clazz";
	public static final Class<?>[]						FROM_PLIST_ARGS			= { NSDictionary.class };
	public static final String							FROM_PLIST_METHODNAME	= "fromPlist";
	public static final String							GROUP_FIELD_NAME		= "group";
	private static ObjectCreator						instance				= null;
	private static Map<String, Map<String, Class<?>>>	map						= new HashMap<String, Map<String, Class<?>>>();

	private ObjectCreator() {
		try {
			addToMapping(com.wow.siriobjects.RequestCompleted.class);

			addToMapping(com.wow.siriobjects.answer.AnswerSnippet.class);
			addToMapping(com.wow.siriobjects.answer.Answer.class);
			addToMapping(com.wow.siriobjects.answer.AnswerLine.class);

			addToMapping(com.wow.siriobjects.clock.ClockObject.class);
			addToMapping(com.wow.siriobjects.clock.ClockSnippet.class);

			addToMapping(com.wow.siriobjects.contact.PersonSnippet.class);
			addToMapping(com.wow.siriobjects.contact.PersonSearch.class);
			addToMapping(com.wow.siriobjects.contact.ContactGroup.class);
			addToMapping(com.wow.siriobjects.contact.PersonSearchCompleted.class);

			addToMapping(com.wow.siriobjects.email.Email.class);

			addToMapping(com.wow.siriobjects.localsearch.MapItem.class);
			addToMapping(com.wow.siriobjects.localsearch.MapItemSnippet.class);

			addToMapping(com.wow.siriobjects.phone.Call.class);

			addToMapping(com.wow.siriobjects.sms.SmsRecipientSearch.class);
			addToMapping(com.wow.siriobjects.sms.SmsRecipientSearchCompleted.class);
			addToMapping(com.wow.siriobjects.sms.SmsSearch.class);
			addToMapping(com.wow.siriobjects.sms.SmsSearchCompleted.class);
			addToMapping(com.wow.siriobjects.sms.SmsSms.class);
			addToMapping(com.wow.siriobjects.sms.SmsSnippet.class);

			addToMapping(com.wow.siriobjects.speech.SpeechRecognized.class);

			addToMapping(com.wow.siriobjects.system.DisambiguationList.class);
			addToMapping(com.wow.siriobjects.system.ListItem.class);

			addToMapping(com.wow.siriobjects.system.SetActivationToken.class);
			addToMapping(com.wow.siriobjects.system.Person.class);
			addToMapping(com.wow.siriobjects.system.CommandFailed.class);
			addToMapping(com.wow.siriobjects.system.GetSessionCertificateResponse.class);
			addToMapping(com.wow.siriobjects.system.CreateSessionInfoResponse.class);
			addToMapping(com.wow.siriobjects.system.AssistantCreated.class);
			addToMapping(com.wow.siriobjects.system.AssistantLoaded.class);
			addToMapping(com.wow.siriobjects.system.AssistantNotFound.class);
			addToMapping(com.wow.siriobjects.system.AssistantDestroyed.class);
			addToMapping(com.wow.siriobjects.system.ResultCallback.class);
			addToMapping(com.wow.siriobjects.system.DomainObjectCancel.class);
			addToMapping(com.wow.siriobjects.system.DomainObjectCommit.class);
			addToMapping(com.wow.siriobjects.system.DomainObjectRetrieve.class);
			addToMapping(com.wow.siriobjects.system.DomainObjectCreate.class);
			addToMapping(com.wow.siriobjects.system.DomainObjectUpdate.class);
			addToMapping(com.wow.siriobjects.system.Location.class);
			addToMapping(com.wow.siriobjects.system.GetRequestOrigin.class);
			addToMapping(com.wow.siriobjects.system.RelatedName.class);
			addToMapping(com.wow.siriobjects.system.Phone.class);
			addToMapping(com.wow.siriobjects.system.SendCommands.class);
			addToMapping(com.wow.siriobjects.system.StartRequest.class);
			addToMapping(com.wow.siriobjects.system.PersonAttribute.class);

			addToMapping(com.wow.siriobjects.weather.WeatherWindSpeed.class);
			addToMapping(com.wow.siriobjects.weather.WeatherBarometricPressure.class);
			addToMapping(com.wow.siriobjects.weather.WeatherCondition.class);
			addToMapping(com.wow.siriobjects.weather.WeatherCurrentConditions.class);
			addToMapping(com.wow.siriobjects.weather.WeatherDailyForecast.class);
			addToMapping(com.wow.siriobjects.weather.WeatherForecastSnippet.class);
			addToMapping(com.wow.siriobjects.weather.WeatherHourlyForecast.class);
			addToMapping(com.wow.siriobjects.weather.WeatherLocation.class);
			addToMapping(com.wow.siriobjects.weather.WeatherLocationAdd.class);
			addToMapping(com.wow.siriobjects.weather.WeatherLocationDelete.class);
			addToMapping(com.wow.siriobjects.weather.WeatherLocationSearch.class);
			addToMapping(com.wow.siriobjects.weather.WeatherLocationSnippet.class);
			addToMapping(com.wow.siriobjects.weather.WeatherObject.class);
			addToMapping(com.wow.siriobjects.weather.WeatherShowWeatherLocations.class);
			addToMapping(com.wow.siriobjects.weather.WeatherUnits.class);

			addToMapping(com.wow.siriobjects.ui.AddViews.class);
			addToMapping(com.wow.siriobjects.ui.AssistantUtteranceView.class);
			addToMapping(com.wow.siriobjects.ui.Button.class);
			addToMapping(com.wow.siriobjects.ui.ConfirmationOptions.class);

			addToMapping(com.wow.siriobjects.websearch.WebSearch.class);
		} catch (IllegalArgumentException e) {
			Log.w(getClass().getSimpleName(), "IllegalArgumentException: " + e.getMessage());
		} catch (SecurityException e) {
			Log.w(getClass().getSimpleName(), "SecurityException: " + e.getMessage());
		} catch (IllegalAccessException e) {
			Log.w(getClass().getSimpleName(), "IllegalAccessException: " + e.getMessage());
		} catch (NoSuchFieldException e) {
			Log.w(getClass().getSimpleName(), "NoSuchFieldException: " + e.getMessage());
		}
	}

	private void addToMapping(Class<?> class1) throws IllegalArgumentException, SecurityException, IllegalAccessException, NoSuchFieldException {
		String clazz = (String) class1.getDeclaredField(CLASS_FIELD_NAME).get(String.class);
		String group = (String) class1.getDeclaredField(GROUP_FIELD_NAME).get(String.class);

		// see if group exists
		Map<String, Class<?>> groupMap;
		if (!map.containsKey(group)) {
			groupMap = new HashMap<String, Class<?>>();
			map.put(group, groupMap);
		} else {
			groupMap = map.get(group);
		}

		groupMap.put(clazz, class1);
	}

	public static ObjectCreator getInstance() {
		if (instance == null) {
			instance = new ObjectCreator();
		}

		return instance;
	}

	public PropertyListSerializable createInstance(NSDictionary plist) {
		String group = ((NSString) plist.objectForKey("group")).toString();
		if (group == null)
			return null;
		String clazz = ((NSString) plist.objectForKey("class")).toString();
		if (clazz == null)
			return null;

		if (!map.containsKey(group)) {
			// Log.i(getClass().getSimpleName(), "GROUP NOT FOUND: " + group);
			return null;
		}

		Map<String, Class<?>> groupMap = map.get(group);

		if (!groupMap.containsKey(clazz)) {
			// Log.i(getClass().getSimpleName(), "CLASS NOT FOUND: " + clazz);
			return null;
		}

		Class<?> class1 = groupMap.get(clazz);
		Object[] args = { plist };
		try {
			Method m;
			m = class1.getDeclaredMethod(FROM_PLIST_METHODNAME, FROM_PLIST_ARGS);
			Object instance;
			instance = class1.newInstance();
			return (PropertyListSerializable) m.invoke(instance, args);
		} catch (InstantiationException e) {
			e.printStackTrace();
			String msg = e.getMessage();
			if (msg == null)
				msg = "";
			Log.w(getClass().getSimpleName(), "InstantiationException: group=" + group + " class=" + clazz + " " + msg);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			String msg = e.getMessage();
			if (msg == null)
				msg = "";
			Log.w(getClass().getSimpleName(), "IllegalAccessException: group=" + group + " class=" + clazz + " " + msg);
		} catch (SecurityException e) {
			e.printStackTrace();
			String msg = e.getMessage();
			if (msg == null)
				msg = "";
			Log.w(getClass().getSimpleName(), "SecurityException: group=" + group + " class=" + clazz + " " + msg);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			String msg = e.getMessage();
			if (msg == null)
				msg = "";
			Log.w(getClass().getSimpleName(), "NoSuchMethodException: group=" + group + " class=" + clazz + " " + msg);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			String msg = e.getMessage();
			if (msg == null)
				msg = "";
			Log.w(getClass().getSimpleName(), "IllegalArgumentException: group=" + group + " class=" + clazz + " " + msg);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			String msg = e.getMessage();
			if (msg == null)
				msg = "";
			Log.w(getClass().getSimpleName(), "InvocationTargetException: group=" + group + " class=" + clazz + " " + msg);
		}

		return null;
	}
}
