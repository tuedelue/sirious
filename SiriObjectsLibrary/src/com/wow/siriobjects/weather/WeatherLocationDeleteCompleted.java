package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ServerBoundCommand;


public class WeatherLocationDeleteCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= 857830970900240336L;
	public static final String	clazz				= "LocationDeleteCompleted";
	public static final String	group				= "com.apple.ace.weather";

	public WeatherLocationDeleteCompleted() {
	}

	public WeatherLocationDeleteCompleted(String aceId, String refId) {
		super(clazz, group, aceId, refId);
	}

	public NSDictionary toPlist() {
		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
