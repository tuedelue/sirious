package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherWindSpeed extends AceObject {
	private static final long	serialVersionUID		= 1221698512219802847L;
	public static final String	clazz					= "WindSpeed";
	public static final String	group					= "com.apple.ace.weather";

	public static final String	DirectionNorthValue		= "North";
	public static final String	DirectionNorthEastValue	= "NorthEast";
	public static final String	DirectionEastValue		= "East";
	public static final String	DirectionSouthEastValue	= "SouthEast";
	public static final String	DirectionSouthValue		= "South";
	public static final String	DirectionSouthWestValue	= "SouthWest";
	public static final String	DirectionWestValue		= "West";
	public static final String	DirectionNorthWestValue	= "NorthWest";

	String						value;
	String						windDirection;
	double						windDirectionDegree;

	public WeatherWindSpeed() {
	}

	public WeatherWindSpeed(String value, String windDirection, double windDirectionDegree) {
		super(clazz, group);
		this.value = value;
		this.windDirection = windDirection;
		this.windDirectionDegree = windDirectionDegree;
	}

	public NSDictionary toPlist() {
		if (value != null)
			properties.put("value", new NSString(value));
		if (windDirection != null)
			properties.put("windDirection", new NSString(windDirection));
		properties.put("windDirectionDegree", new NSNumber(windDirectionDegree));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherWindSpeed fromPlist(NSDictionary plist) {
		String value = PropertyListUtils.getStringProperty(plist, "value");
		String windDirection = PropertyListUtils.getStringProperty(plist, "windDirection");
		double windDirectionDegree = PropertyListUtils.getDoubleProperty(plist, "windDirectionDegree");

		return new WeatherWindSpeed(value, windDirection, windDirectionDegree);
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return the windDirection
	 */
	public String getWindDirection() {
		return windDirection;
	}

	/**
	 * @return the windDirectionDegree
	 */
	public double getWindDirectionDegree() {
		return windDirectionDegree;
	}

}
