package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherBarometricPressure extends AceObject {
	private static final long	serialVersionUID	= 7443114369570067331L;
	public static final String	clazz				= "BarometricPressure";
	public static final String	group				= "com.apple.ace.weather";

	String						TrendRisingValue	= "Rising";
	String						TrendFallingValue	= "Falling";

	public WeatherBarometricPressure() {

	}

	public WeatherBarometricPressure(String trendRisingValue, String trendFallingValue) {
		super(clazz, group);
		TrendRisingValue = trendRisingValue;
		TrendFallingValue = trendFallingValue;
	}

	public NSDictionary toPlist() {
		if (TrendRisingValue != null)
			properties.put("TrendRisingValue", new NSString(TrendRisingValue));
		if (TrendFallingValue != null)
			properties.put("TrendFallingValue", new NSString(TrendFallingValue));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public WeatherBarometricPressure fromPlist(NSDictionary plist) {
		String TrendRisingValue = PropertyListUtils.getStringProperty(plist, "TrendRisingValue");
		String TrendFallingValue = PropertyListUtils.getStringProperty(plist, "TrendFallingValue");

		return new WeatherBarometricPressure(TrendRisingValue, TrendFallingValue);
	}

	/**
	 * @return the trendRisingValue
	 */
	public String getTrendRisingValue() {
		return TrendRisingValue;
	}

	/**
	 * @return the trendFallingValue
	 */
	public String getTrendFallingValue() {
		return TrendFallingValue;
	}

}
