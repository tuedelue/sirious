package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherUnits extends AceObject {
	private static final long	serialVersionUID				= 4981235517366495399L;
	public static final String	clazz							= "Units";
	public static final String	group							= "com.apple.ace.weather";

	public static final String	DistanceUnitsFeetValue			= "Feet";
	public static final String	DistanceUnitsMilesValue			= "Miles";
	public static final String	DistanceUnitsMetersValue		= "Meters";
	public static final String	DistanceUnitsKilometersValue	= "Kilometers";

	public static final String	PressureUnitsINValue			= "IN";
	public static final String	PressureUnitsMBValue			= "MB";

	public static final String	SpeedUnitsMPHValue				= "MPH";
	public static final String	SpeedUnitsKPHValue				= "KPH";

	public static final String	TemperatureUnitsCelsiusValue	= "Celsius";
	public static final String	TemperatureUnitsFahrenheitValue	= "Fahrenheit";

	String						distanceUnits;
	String						pressureUnits;
	String						speedUnits;
	String						temperatureUnits;

	public WeatherUnits() {
	}

	public WeatherUnits(String distanceUnits, String pressureUnits, String speedUnits, String temperatureUnits) {
		super(clazz, group);
		this.distanceUnits = distanceUnits;
		this.pressureUnits = pressureUnits;
		this.speedUnits = speedUnits;
		this.temperatureUnits = temperatureUnits;
	}

	public NSDictionary toPlist() {
		if (distanceUnits != null)
			properties.put("distanceUnits", new NSString(distanceUnits));
		if (pressureUnits != null)
			properties.put("pressureUnits", new NSString(pressureUnits));
		if (speedUnits != null)
			properties.put("speedUnits", new NSString(speedUnits));
		if (temperatureUnits != null)
			properties.put("temperatureUnits", new NSString(temperatureUnits));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherUnits fromPlist(NSDictionary plist) {
		String distanceUnits = PropertyListUtils.getStringProperty(plist, "distanceUnits");
		String pressureUnits = PropertyListUtils.getStringProperty(plist, "pressureUnits");
		String speedUnits = PropertyListUtils.getStringProperty(plist, "speedUnits");
		String temperatureUnits = PropertyListUtils.getStringProperty(plist, "temperatureUnits");

		return new WeatherUnits(distanceUnits, pressureUnits, speedUnits, temperatureUnits);
	}

	/**
	 * @return the distanceUnits
	 */
	public String getDistanceUnits() {
		return distanceUnits;
	}

	/**
	 * @return the pressureUnits
	 */
	public String getPressureUnits() {
		return pressureUnits;
	}

	/**
	 * @return the speedUnits
	 */
	public String getSpeedUnits() {
		return speedUnits;
	}

	/**
	 * @return the temperatureUnits
	 */
	public String getTemperatureUnits() {
		return temperatureUnits;
	}

}
