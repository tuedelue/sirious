package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherHourlyForecast extends WeatherForecast {
	private static final long	serialVersionUID	= -1181239380151958531L;
	public static final String	clazz				= "HourlyForecast";
	public static final String	group				= "com.apple.ace.weather";

	double						temperature;

	public WeatherHourlyForecast() {
	}

	public WeatherHourlyForecast(double chanceOfPerception, WeatherCondition condition, boolean isUserRequested, int timeIndex, double temperature) {
		super(chanceOfPerception, condition, isUserRequested, timeIndex);
		this.temperature = temperature;
	}

	public NSDictionary toPlist() {
		properties.put("temperature", new NSNumber(temperature));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherHourlyForecast fromPlist(NSDictionary plist) {
		WeatherForecast w = WeatherForecast.fromPlist(plist);
		double temperature = PropertyListUtils.getDoubleProperty(plist, "temperature");

		return new WeatherHourlyForecast(w.chanceOfPerception, w.condition, w.isUserRequested, w.timeIndex, temperature);
	}

	/**
	 * @return the temperature
	 */
	public double getTemperature() {
		return temperature;
	}

}
