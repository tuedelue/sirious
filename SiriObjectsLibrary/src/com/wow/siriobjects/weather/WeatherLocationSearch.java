package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherLocationSearch extends ClientBoundCommand {
	private static final long	serialVersionUID	= 6224149356765092282L;
	public static final String	clazz				= "LocationSearch";
	public static final String	group				= "com.apple.ace.weather";

	String						targetAppId;
	String						identifier;
	String						locationId;

	public WeatherLocationSearch() {
	}

	public WeatherLocationSearch(String aceId, String refId, List<ClientBoundCommand> callbacks, String targetAppId, String identifier, String locationId) {
		super(clazz, group, aceId, refId, callbacks);
		this.targetAppId = targetAppId;
		this.identifier = identifier;
		this.locationId = locationId;
	}

	public NSDictionary toPlist() {
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));
		if (identifier != null)
			properties.put("identifier", new NSString(identifier));
		if (locationId != null)
			properties.put("locationId", new NSString(locationId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherLocationSearch fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");
		String locationId = PropertyListUtils.getStringProperty(plist, "locationId");

		return new WeatherLocationSearch(aceId, refId, callbacks, targetAppId, identifier, locationId);
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

}
