package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherShowWeatherLocations extends ClientBoundCommand {
	private static final long	serialVersionUID	= -3966661592827433703L;
	public static final String	clazz				= "ShowWeatherLocations";
	public static final String	group				= "com.apple.ace.weather";

	String						targetAppId;

	public WeatherShowWeatherLocations() {
	}

	public WeatherShowWeatherLocations(String aceId, String refId, List<ClientBoundCommand> callbacks, String targetAppId) {
		super(clazz, group, aceId, refId, callbacks);
		this.targetAppId = targetAppId;
	}

	public NSDictionary toPlist() {
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherShowWeatherLocations fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");

		return new WeatherShowWeatherLocations(aceId, refId, callbacks, targetAppId);
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

}
