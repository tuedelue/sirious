package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.siriobjects.ui.Snippet;


public class WeatherForecastSnippet extends Snippet {
	private static final long	serialVersionUID	= -731865259947034459L;
	public static final String	clazz				= "ForecastSnippet";
	public static final String	group				= "com.apple.ace.weather";

	List<WeatherObject>			aceWeathers;

	public WeatherForecastSnippet() {
	}

	public WeatherForecastSnippet(String viewId, String speakableText, boolean listenAfterSpeaking, Object otherOptions, ConfirmationOptions confirmationOptions, List<WeatherObject> aceWeathers) {
		super(group, viewId, speakableText, listenAfterSpeaking, otherOptions, confirmationOptions);
		this.aceWeathers = aceWeathers;
	}

	public NSDictionary toPlist() {
		if (aceWeathers != null)
			properties.put("aceWeathers", PropertyListUtils.convertPlistSerializableListToNSArray(aceWeathers));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherForecastSnippet fromPlist(NSDictionary plist) {
		Snippet s = Snippet.fromPlist(plist);
		List<WeatherObject> aceWeathers = PropertyListUtils.getListObjects(plist, "aceWeathers");

		return new WeatherForecastSnippet(s.getViewId(), s.getSpeakableText(), s.isListenAfterSpeaking(), null, s.getConfirmationOptions(), aceWeathers);
	}

	/**
	 * @return the aceWeathers
	 */
	public List<WeatherObject> getAceWeathers() {
		return aceWeathers;
	}

}
