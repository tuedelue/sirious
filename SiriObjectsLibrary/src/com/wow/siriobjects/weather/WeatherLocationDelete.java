package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherLocationDelete extends ClientBoundCommand {
	private static final long	serialVersionUID	= 5483122499975741775L;
	public static final String	clazz				= "LocationDelete";
	public static final String	group				= "com.apple.ace.weather";

	String						targetAppId;
	String						weatherLocation;

	public WeatherLocationDelete() {
	}

	public WeatherLocationDelete(String aceId, String refId, List<ClientBoundCommand> callbacks, String targetAppId, String weatherLocation) {
		super(clazz, group, aceId, refId, callbacks);
		this.targetAppId = targetAppId;
		this.weatherLocation = weatherLocation;
	}

	public NSDictionary toPlist() {
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));
		if (weatherLocation != null)
			properties.put("weatherLocation", new NSString(weatherLocation));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherLocationDelete fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);
		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		String weatherLocation = PropertyListUtils.getStringProperty(plist, "weatherLocation");

		return new WeatherLocationDelete(aceId, refId, callbacks, targetAppId, weatherLocation);
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

	/**
	 * @return the weatherLocation
	 */
	public String getWeatherLocation() {
		return weatherLocation;
	}

}
