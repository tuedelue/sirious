package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherLocationAdd extends ClientBoundCommand {
	public static final String	clazz	= "LocationAdd";
	public static final String	group	= "com.apple.ace.weather";

	String						targetAppId;
	WeatherLocation				weatherLocation;

	public WeatherLocationAdd() {
	}

	public WeatherLocationAdd(String aceId, String refId, List<ClientBoundCommand> callbacks, String targetAppId, WeatherLocation weatherLocation) {
		super(clazz, group, aceId, refId, callbacks);
		this.targetAppId = targetAppId;
		this.weatherLocation = weatherLocation;
	}

	public NSDictionary toPlist() {
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));
		if (weatherLocation != null)
			properties.put("weatherLocation", weatherLocation.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherLocationAdd fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);
		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");
		WeatherLocation weatherLocation = PropertyListUtils.getObject(plist, "weatherLocation");

		return new WeatherLocationAdd(aceId, refId, callbacks, targetAppId, weatherLocation);
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

	/**
	 * @return the weatherLocation
	 */
	public WeatherLocation getWeatherLocation() {
		return weatherLocation;
	}

}
