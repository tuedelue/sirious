package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherDailyForecast extends WeatherForecast {
	private static final long	serialVersionUID	= -8720148885338577907L;
	public static final String	clazz				= "DailyForecast";
	public static final String	group				= "com.apple.ace.weather";

	double						highTemperature;
	double						lowTemperature;

	public WeatherDailyForecast() {
	}

	public WeatherDailyForecast(double chanceOfPerception, WeatherCondition condition, boolean isUserRequested, int timeIndex, double highTemperature, double lowTemperature) {
		super(chanceOfPerception, condition, isUserRequested, timeIndex);
		this.highTemperature = highTemperature;
		this.lowTemperature = lowTemperature;
	}

	public NSDictionary toPlist() {
		properties.put("highTemperature", new NSNumber(highTemperature));
		properties.put("lowTemperature", new NSNumber(lowTemperature));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherDailyForecast fromPlist(NSDictionary plist) {
		WeatherForecast fc = WeatherForecast.fromPlist(plist);
		double highTemperature = PropertyListUtils.getDoubleProperty(plist, "highTemperature");
		double lowTemperature = PropertyListUtils.getDoubleProperty(plist, "lowTemperature");

		return new WeatherDailyForecast(fc.chanceOfPerception, fc.condition, fc.isUserRequested, fc.timeIndex, highTemperature, lowTemperature);
	}

	/**
	 * @return the highTemperature
	 */
	public double getHighTemperature() {
		return highTemperature;
	}

	/**
	 * @return the lowTemperature
	 */
	public double getLowTemperature() {
		return lowTemperature;
	}

}
