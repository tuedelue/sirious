package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherCurrentConditions extends AceObject {
	private static final long	serialVersionUID				= 7511142595368973601L;
	public static final String	clazz							= "CurrentConditions";
	public static final String	group							= "com.apple.ace.weather";

	public static final String	MoonPhaseNEWValue				= "NEW";
	public static final String	MoonPhaseWAXING_CRESCENTValue	= "WAXING_CRESCENT";
	public static final String	MoonPhaseFIRST_QUARTERValue		= "FIRST_QUARTER";
	public static final String	MoonPhaseWAXING_GIBBOUSValue	= "WAXING_GIBBOUS";
	public static final String	MoonPhaseFULLValue				= "FULL";
	public static final String	MoonPhaseWANING_GIBBOUSValue	= "WANING_GIBBOUS";
	public static final String	MoonPhaseTHIRD_QUARTERValue		= "THIRD_QUARTER";
	public static final String	MoonPhaseWANING_CRESCENTValue	= "WANING_CRESCENT";

	WeatherBarometricPressure	barometricPressure;
	WeatherCondition			condition;
	int							dayOfWeek;
	String						dewPoint;
	String						feelsLike;
	String						heatIndex;
	String						moonPhase;
	String						percentHumidity;
	double						percentOfMoonFaceVisible;
	String						sunrise;
	String						sunset;
	String						temperature;
	String						timeOfObservation;
	String						timeZone;
	String						visibility;
	String						windChill;
	WeatherWindSpeed			windSpeed;

	public WeatherCurrentConditions() {
	}

	public WeatherCurrentConditions(WeatherBarometricPressure barometricPressure, WeatherCondition condition, int dayOfWeek, String dewPoint, String feelsLike, String heatIndex, String moonPhase,
			String percentHumidity, double percentOfMoonFaceVisible, String sunrise, String sunset, String temperature, String timeOfObservation, String timeZone, String visibility, String windChill,
			WeatherWindSpeed windSpeed) {
		super(clazz, group);
		this.barometricPressure = barometricPressure;
		this.condition = condition;
		this.dayOfWeek = dayOfWeek;
		this.dewPoint = dewPoint;
		this.feelsLike = feelsLike;
		this.heatIndex = heatIndex;
		this.moonPhase = moonPhase;
		this.percentHumidity = percentHumidity;
		this.percentOfMoonFaceVisible = percentOfMoonFaceVisible;
		this.sunrise = sunrise;
		this.sunset = sunset;
		this.temperature = temperature;
		this.timeOfObservation = timeOfObservation;
		this.timeZone = timeZone;
		this.visibility = visibility;
		this.windChill = windChill;
		this.windSpeed = windSpeed;
	}

	public NSDictionary toPlist() {
		properties.put("dayOfWeek", new NSNumber(dayOfWeek));
		if (barometricPressure != null)
			properties.put("barometricPressure", barometricPressure.toPlist());
		if (windSpeed != null)
			properties.put("windSpeed", windSpeed.toPlist());
		if (condition != null)
			properties.put("condition", condition.toPlist());
		properties.put("percentOfMoonFaceVisible", new NSNumber(percentOfMoonFaceVisible));

		if (dewPoint != null)
			properties.put("dewPoint", new NSString(dewPoint));
		if (feelsLike != null)
			properties.put("feelsLike", new NSString(feelsLike));
		if (heatIndex != null)
			properties.put("heatIndex", new NSString(heatIndex));
		if (moonPhase != null)
			properties.put("moonPhase", new NSString(moonPhase));
		if (percentHumidity != null)
			properties.put("percentHumidity", new NSString(percentHumidity));
		if (sunrise != null)
			properties.put("sunrise", new NSString(sunrise));
		if (sunset != null)
			properties.put("sunset", new NSString(sunset));
		if (temperature != null)
			properties.put("temperature", new NSString(temperature));
		if (timeOfObservation != null)
			properties.put("timeOfObservation", new NSString(timeOfObservation));
		if (timeZone != null)
			properties.put("timeZone", new NSString(timeZone));
		if (visibility != null)
			properties.put("visibility", new NSString(visibility));
		if (windChill != null)
			properties.put("windChill", new NSString(windChill));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherCurrentConditions fromPlist(NSDictionary plist) {
		WeatherBarometricPressure barometricPressure = PropertyListUtils.getObject(plist, "barometricPressure");
		WeatherCondition condition = PropertyListUtils.getObject(plist, "condition");
		WeatherWindSpeed windSpeed = PropertyListUtils.getObject(plist, "windSpeed");
		double percentOfMoonFaceVisible = PropertyListUtils.getDoubleProperty(plist, "percentOfMoonFaceVisible");
		int dayOfWeek = PropertyListUtils.getIntProperty(plist, "dayOfWeek");
		String dewPoint = PropertyListUtils.getStringProperty(plist, "dewPoint");
		String feelsLike = PropertyListUtils.getStringProperty(plist, "feelsLike");
		String heatIndex = PropertyListUtils.getStringProperty(plist, "heatIndex");
		String moonPhase = PropertyListUtils.getStringProperty(plist, "moonPhase");
		String percentHumidity = PropertyListUtils.getStringProperty(plist, "percentHumidity");
		String sunrise = PropertyListUtils.getStringProperty(plist, "sunrise");
		String sunset = PropertyListUtils.getStringProperty(plist, "sunset");
		String temperature = PropertyListUtils.getStringProperty(plist, "temperature");
		String timeOfObservation = PropertyListUtils.getStringProperty(plist, "timeOfObservation");
		String timeZone = PropertyListUtils.getStringProperty(plist, "timeZone");
		String visibility = PropertyListUtils.getStringProperty(plist, "visibility");
		String windChill = PropertyListUtils.getStringProperty(plist, "windChill");

		return new WeatherCurrentConditions(barometricPressure, condition, dayOfWeek, dewPoint, feelsLike, heatIndex, moonPhase, percentHumidity, percentOfMoonFaceVisible, sunrise, sunset,
				temperature, timeOfObservation, timeZone, visibility, windChill, windSpeed);
	}

	/**
	 * @return the barometricPressure
	 */
	public WeatherBarometricPressure getBarometricPressure() {
		return barometricPressure;
	}

	/**
	 * @return the condition
	 */
	public WeatherCondition getCondition() {
		return condition;
	}

	/**
	 * @return the dayOfWeek
	 */
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	/**
	 * @return the dewPoint
	 */
	public String getDewPoint() {
		return dewPoint;
	}

	/**
	 * @return the feelsLike
	 */
	public String getFeelsLike() {
		return feelsLike;
	}

	/**
	 * @return the heatIndex
	 */
	public String getHeatIndex() {
		return heatIndex;
	}

	/**
	 * @return the moonPhase
	 */
	public String getMoonPhase() {
		return moonPhase;
	}

	/**
	 * @return the percentHumidity
	 */
	public String getPercentHumidity() {
		return percentHumidity;
	}

	/**
	 * @return the percentOfMoonFaceVisible
	 */
	public double getPercentOfMoonFaceVisible() {
		return percentOfMoonFaceVisible;
	}

	/**
	 * @return the sunrise
	 */
	public String getSunrise() {
		return sunrise;
	}

	/**
	 * @return the sunset
	 */
	public String getSunset() {
		return sunset;
	}

	/**
	 * @return the temperature
	 */
	public String getTemperature() {
		return temperature;
	}

	/**
	 * @return the timeOfObservation
	 */
	public String getTimeOfObservation() {
		return timeOfObservation;
	}

	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @return the visibility
	 */
	public String getVisibility() {
		return visibility;
	}

	/**
	 * @return the windChill
	 */
	public String getWindChill() {
		return windChill;
	}

	/**
	 * @return the windSpeed
	 */
	public WeatherWindSpeed getWindSpeed() {
		return windSpeed;
	}

}
