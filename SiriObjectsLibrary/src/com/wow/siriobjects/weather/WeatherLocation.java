package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.Location;


public class WeatherLocation extends Location {
	private static final long	serialVersionUID	= 4610087200686557438L;
	public static final String	clazz				= "";
	public static final String	group				= "com.apple.ace.weather";

	String						locationId;

	public WeatherLocation() {
	}

	public WeatherLocation(String identifier, String label, String street, String city, String stateCode, String countryCode, String postalCode, double latitude, double longitude, int accuracy,
			String locationId) {
		super(identifier, label, street, city, stateCode, countryCode, postalCode, latitude, longitude, accuracy);
		this.locationId = locationId;
	}

	public NSDictionary toPlist() {
		if (locationId != null)
			properties.put("locationId", new NSString(locationId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherLocation fromPlist(NSDictionary plist) {
		Location l = Location.fromPlist(plist);
		String locationId = PropertyListUtils.getStringProperty(plist, "locationId");

		return new WeatherLocation(l.getIdentifier(), l.getLabel(), l.getStreet(), l.getCity(), l.getStateCode(), l.getCountryCode(), l.getPostalCode(), l.getLatitude(), l.getLongitude(),
				l.getAccuracy(), locationId);
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

}
