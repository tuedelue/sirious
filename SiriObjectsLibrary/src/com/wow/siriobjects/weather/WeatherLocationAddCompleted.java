package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class WeatherLocationAddCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID			= -6764908727915510199L;
	public static final String	clazz						= "LocationAddCompleted";
	public static final String	group						= "com.apple.ace.weather";

	public static final String	ErrorAlreadyExistsValue		= "AlreadyExists";
	public static final String	ErrorMaxNumberExceededValue	= "MaxNumberExceeded";

	String						error;
	String						weatherLocationId;

	public WeatherLocationAddCompleted() {
	}

	public WeatherLocationAddCompleted(String aceId, String refId, String error, String weatherLocationId) {
		super(clazz, group, aceId, refId);
		this.error = error;
		this.weatherLocationId = weatherLocationId;
	}

	public NSDictionary toPlist() {
		if (error != null)
			properties.put("error", new NSString(error));
		if (weatherLocationId != null)
			properties.put("weatherLocationId", new NSString(weatherLocationId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherLocationAddCompleted fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		String error = PropertyListUtils.getStringProperty(plist, "error");
		String weatherLocationId = PropertyListUtils.getStringProperty(plist, "weatherLocationId");
		return new WeatherLocationAddCompleted(aceId, refId, error, weatherLocationId);
	}

	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}

	/**
	 * @return the weatherLocationId
	 */
	public String getWeatherLocationId() {
		return weatherLocationId;
	}

}
