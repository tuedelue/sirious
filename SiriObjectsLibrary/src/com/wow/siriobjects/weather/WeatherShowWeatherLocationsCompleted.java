package com.wow.siriobjects.weather;

import com.wow.siriobjects.ServerBoundCommand;

public class WeatherShowWeatherLocationsCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= -4547253522862488285L;
	public static final String	clazz				= "ShowWeatherLocationsCompleted";
	public static final String	group				= "com.apple.ace.weather";

	public WeatherShowWeatherLocationsCompleted() {
	}

	public WeatherShowWeatherLocationsCompleted(String aceId, String refId) {
		super(clazz, group, aceId, refId);
	}
}
