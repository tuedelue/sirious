package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSString;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherCondition extends AceObject {
	private static final long	serialVersionUID							= 8963951721097674166L;
	public static final String	clazz										= "Condition";
	public static final String	group										= "com.apple.ace.weather";

	public static final String	ConditionCodeTornadoValue					= "Tornado";
	public static final String	ConditionCodeTropical_StormValue			= "Tropical_Storm";
	public static final String	ConditionCodeHurricaneValue					= "Hurricane";
	public static final String	ConditionCodeSevere_ThunderstormsValue		= "Severe_Thunderstorms";
	public static final String	ConditionCodeThunderstormsValue				= "Thunderstorms";
	public static final String	ConditionCodeMixedRainAndSnowValue			= "MixedRainAndSnow";
	public static final String	ConditionCodeMixedRainAndSleetValue			= "MixedRainAndSleet";
	public static final String	ConditionCodeMixedSnowAndSleetValue			= "MixedSnowAndSleet";
	public static final String	ConditionCodeFreezingDrizzleValue			= "FreezingDrizzle";
	public static final String	ConditionCodeDrizzleValue					= "Drizzle";
	public static final String	ConditionCodeFreezingRainValue				= "FreezingRain";
	public static final String	ConditionCodeShowersValue					= "Showers";
	public static final String	ConditionCodeShowers2Value					= "Showers2";
	public static final String	ConditionCodeSnowFlurriesValue				= "SnowFlurries";
	public static final String	ConditionCodeLightSnowShowersValue			= "LightSnowShowers";
	public static final String	ConditionCodeBlowingSnowValue				= "BlowingSnow";
	public static final String	ConditionCodeSnowValue						= "Snow";
	public static final String	ConditionCodeHailValue						= "Hail";
	public static final String	ConditionCodeSleetValue						= "Sleet";
	public static final String	ConditionCodeDustValue						= "Dust";
	public static final String	ConditionCodeFoggyValue						= "Foggy";
	public static final String	ConditionCodeHazeValue						= "Haze";
	public static final String	ConditionCodeSmokyValue						= "Smoky";
	public static final String	ConditionCodeBlusteryValue					= "Blustery";
	public static final String	ConditionCodeWindyValue						= "Windy";
	public static final String	ConditionCodeColdValue						= "Cold";
	public static final String	ConditionCodeCloudyValue					= "Cloudy";
	public static final String	ConditionCodeMostlyCloudyNightValue			= "MostlyCloudyNight";
	public static final String	ConditionCodeMostlyCloudyDayValue			= "MostlyCloudyDay";
	public static final String	ConditionCodePartlyCloudyNightValue			= "PartlyCloudyNight";
	public static final String	ConditionCodePartlyCloudyDayValue			= "PartlyCloudyDay";
	public static final String	ConditionCodeClearNightValue				= "ClearNight";
	public static final String	ConditionCodeSunnyValue						= "Sunny";
	public static final String	ConditionCodeFairNightValue					= "FairNight";
	public static final String	ConditionCodeFairDayValue					= "FairDay";
	public static final String	ConditionCodeMixedRainAndHailValue			= "MixedRainAndHail";
	public static final String	ConditionCodeHotValue						= "Hot";
	public static final String	ConditionCodeIsolatedThunderstormsValue		= "IsolatedThunderstorms";
	public static final String	ConditionCodeScatteredThunderstormsValue	= "ScatteredThunderstorms";
	public static final String	ConditionCodeScatteredThunderstorms2Value	= "ScatteredThunderstorms2";
	public static final String	ConditionCodeScatteredShowersValue			= "ScatteredShowers";
	public static final String	ConditionCodeHeavySnowValue					= "HeavySnow";
	public static final String	ConditionCodeScatteredSnowShowersValue		= "ScatteredSnowShowers";
	public static final String	ConditionCodeHeavySnow2Value				= "HeavySnow2";
	public static final String	ConditionCodePartlyCloudyValue				= "PartlyCloudy";
	public static final String	ConditionCodeThundershowersValue			= "Thundershowers";
	public static final String	ConditionCodeSnowShowersValue				= "SnowShowers";
	public static final String	ConditionCodeIsolatedThundershowersValue	= "IsolatedThundershowers";
	public static final String	ConditionCodeNotAvailableValue				= "NotAvailable";

	String						conditionCode;
	int							conditionCodeIndex;

	public WeatherCondition() {
	}

	public WeatherCondition(String conditionCode, int conditionCodeIndex) {
		super(clazz, group);
		this.conditionCode = conditionCode;
		this.conditionCodeIndex = conditionCodeIndex;
	}

	public NSDictionary toPlist() {
		if (conditionCode != null)
			properties.put("conditionCode", new NSString(conditionCode));
		properties.put("conditionCodeIndex", new NSNumber(conditionCodeIndex));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherCondition fromPlist(NSDictionary plist) {
		String conditionCode = PropertyListUtils.getStringProperty(plist, "conditionCode");
		int conditionCodeIndex = PropertyListUtils.getIntProperty(plist, "conditionCodeIndex");

		return new WeatherCondition(conditionCode, conditionCodeIndex);
	}

	/**
	 * @return the conditionCode
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * @return the conditionCodeIndex
	 */
	public int getConditionCodeIndex() {
		return conditionCodeIndex;
	}

}
