package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ui.ConfirmationOptions;
import com.wow.siriobjects.ui.Snippet;


public class WeatherLocationSnippet extends Snippet {
	private static final long	serialVersionUID	= -4908285995300468462L;
	public static final String	clazz				= "LocationSnippet";
	public static final String	group				= "com.apple.ace.weather";

	List<WeatherLocation>		weatherLocations;

	public WeatherLocationSnippet() {
	}

	public WeatherLocationSnippet(String viewId, String speakableText, boolean listenAfterSpeaking, Object otherOptions, ConfirmationOptions confirmationOptions, List<WeatherLocation> weatherLocations) {
		super(group, viewId, speakableText, listenAfterSpeaking, otherOptions, confirmationOptions);
		this.weatherLocations = weatherLocations;
	}

	public NSDictionary toPlist() {
		if (weatherLocations != null)
			properties.put("weatherLocations", PropertyListUtils.convertPlistSerializableListToNSArray(weatherLocations));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherLocationSnippet fromPlist(NSDictionary plist) {
		Snippet s = Snippet.fromPlist(plist);
		List<WeatherLocation> weatherLocations = PropertyListUtils.getListObjects(plist, "weatherLocations");

		return new WeatherLocationSnippet(s.getViewId(), s.getSpeakableText(), s.isListenAfterSpeaking(), null, s.getConfirmationOptions(), weatherLocations);
	}

	/**
	 * @return the weatherLocations
	 */
	public List<WeatherLocation> getWeatherLocations() {
		return weatherLocations;
	}

}
