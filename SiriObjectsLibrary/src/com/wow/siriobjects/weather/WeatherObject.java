package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.system.DomainObject;


public class WeatherObject extends DomainObject {
	private static final long	serialVersionUID	= -1330731249454531950L;
	public static final String	clazz				= "Object";
	public static final String	group				= "com.apple.ace.weather";

	public static final String	ViewDAILYValue		= "DAILY";
	public static final String	ViewHOURLYValue		= "HOURLY";

	WeatherCurrentConditions	currentConditions;
	List<WeatherDailyForecast>	dailyForecasts;
	String						extendedForecastUrl;
	List<WeatherHourlyForecast>	hourlyForecasts;
	WeatherUnits				units;
	String						view;
	WeatherLocation				weatherLocation;

	public WeatherObject() {
		super(clazz, group);
	}

	public WeatherObject(String identifier, WeatherCurrentConditions currentConditions, List<WeatherDailyForecast> dailyForecasts, String extendedForecastUrl,
			List<WeatherHourlyForecast> hourlyForecasts, WeatherUnits units, String view, WeatherLocation weatherLocation) {
		super(clazz, group, identifier);
		this.currentConditions = currentConditions;
		this.dailyForecasts = dailyForecasts;
		this.extendedForecastUrl = extendedForecastUrl;
		this.hourlyForecasts = hourlyForecasts;
		this.units = units;
		this.view = view;
		this.weatherLocation = weatherLocation;
	}

	public NSDictionary toPlist() {
		if (view != null)
			properties.put("view", new NSString(view));
		if (extendedForecastUrl != null)
			properties.put("extendedForecastUrl", new NSString(extendedForecastUrl));
		if (dailyForecasts != null)
			properties.put("dailyForecasts", PropertyListUtils.convertPlistSerializableListToNSArray(dailyForecasts));
		if (hourlyForecasts != null)
			properties.put("hourlyForecasts", PropertyListUtils.convertPlistSerializableListToNSArray(hourlyForecasts));
		if (currentConditions != null)
			properties.put("currentConditions", currentConditions.toPlist());
		if (units != null)
			properties.put("units", units.toPlist());
		if (weatherLocation != null)
			properties.put("weatherLocation", weatherLocation.toPlist());

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherObject fromPlist(NSDictionary plist) {
		String identifier = PropertyListUtils.getStringProperty(plist, "identifier");
		String extendedForecastUrl = PropertyListUtils.getStringProperty(plist, "extendedForecastUrl");
		String view = PropertyListUtils.getStringProperty(plist, "view");
		List<WeatherDailyForecast> dailyForecasts = PropertyListUtils.getListObjects(plist, "dailyForecasts");
		List<WeatherHourlyForecast> hourlyForecasts = PropertyListUtils.getListObjects(plist, "hourlyForecasts");
		;
		WeatherCurrentConditions currentConditions = PropertyListUtils.getObject(plist, "currentConditions");
		WeatherUnits units = PropertyListUtils.getObject(plist, "units");
		WeatherLocation weatherLocation = PropertyListUtils.getObject(plist, "weatherLocation");

		return new WeatherObject(identifier, currentConditions, dailyForecasts, extendedForecastUrl, hourlyForecasts, units, view, weatherLocation);
	}

	/**
	 * @return the currentConditions
	 */
	public WeatherCurrentConditions getCurrentConditions() {
		return currentConditions;
	}

	/**
	 * @return the dailyForecasts
	 */
	public List<WeatherDailyForecast> getDailyForecasts() {
		return dailyForecasts;
	}

	/**
	 * @return the extendedForecastUrl
	 */
	public String getExtendedForecastUrl() {
		return extendedForecastUrl;
	}

	/**
	 * @return the hourlyForecasts
	 */
	public List<WeatherHourlyForecast> getHourlyForecasts() {
		return hourlyForecasts;
	}

	/**
	 * @return the units
	 */
	public WeatherUnits getUnits() {
		return units;
	}

	/**
	 * @return the view
	 */
	public String getView() {
		return view;
	}

	/**
	 * @return the weatherLocation
	 */
	public WeatherLocation getWeatherLocation() {
		return weatherLocation;
	}

}
