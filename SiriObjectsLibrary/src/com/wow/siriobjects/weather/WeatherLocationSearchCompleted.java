package com.wow.siriobjects.weather;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.PropertyListUtils;
import com.wow.siriobjects.ServerBoundCommand;


public class WeatherLocationSearchCompleted extends ServerBoundCommand {
	private static final long	serialVersionUID	= -2587972616572162834L;
	public static final String	clazz				= "LocationSearchCompleted";
	public static final String	group				= "com.apple.ace.weather";

	List<WeatherLocation>		weatherLocations;

	public WeatherLocationSearchCompleted(String aceId, String refId, List<WeatherLocation> weatherLocations) {
		super(clazz, group, aceId, refId);
		this.weatherLocations = weatherLocations;
	}

	public WeatherLocationSearchCompleted() {
	}

	public NSDictionary toPlist() {
		if (weatherLocations != null)
			properties.put("weatherLocations", PropertyListUtils.convertPlistSerializableListToNSArray(weatherLocations));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}
}
