package com.wow.siriobjects.weather;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class WeatherForecast extends AceObject {
	private static final long	serialVersionUID	= 6986758442014701616L;
	public static final String	clazz				= "Forecast";
	public static final String	group				= "com.apple.ace.weather";

	double						chanceOfPerception;
	WeatherCondition			condition;
	boolean						isUserRequested;
	int							timeIndex;

	public WeatherForecast() {
	}

	public WeatherForecast(double chanceOfPerception, WeatherCondition condition, boolean isUserRequested, int timeIndex) {
		super(clazz, group);
		this.chanceOfPerception = chanceOfPerception;
		this.condition = condition;
		this.isUserRequested = isUserRequested;
		this.timeIndex = timeIndex;
	}

	public NSDictionary toPlist() {
		if (condition != null)
			properties.put("condition", condition.toPlist());
		properties.put("chanceOfPrecipitation", new NSNumber(chanceOfPerception));
		properties.put("isUserRequested", new NSNumber(isUserRequested));
		properties.put("timeIndex", new NSNumber(timeIndex));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WeatherForecast fromPlist(NSDictionary plist) {
		WeatherCondition condition = PropertyListUtils.getObject(plist, "condition");
		double chanceOfPrecipitation = PropertyListUtils.getDoubleProperty(plist, "chanceOfPrecipitation");
		boolean isUserRequested = PropertyListUtils.getBooleanProperty(plist, "isUserRequested");
		int timeIndex = PropertyListUtils.getIntProperty(plist, "timeIndex");

		return new WeatherForecast(chanceOfPrecipitation, condition, isUserRequested, timeIndex);
	}

	/**
	 * @return the chanceOfPrecipitation
	 */
	public double getChanceOfPrecipitation() {
		return chanceOfPerception;
	}

	/**
	 * @return the condition
	 */
	public WeatherCondition getCondition() {
		return condition;
	}

	/**
	 * @return the isUserRequested
	 */
	public boolean isUserRequested() {
		return isUserRequested;
	}

	/**
	 * @return the timeIndex
	 */
	public int getTimeIndex() {
		return timeIndex;
	}
}
