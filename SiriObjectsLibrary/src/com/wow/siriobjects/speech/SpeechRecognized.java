package com.wow.siriobjects.speech;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class SpeechRecognized extends ClientBoundCommand {
	public final static String	clazz	= "SpeechRecognized";
	public final static String	group	= "com.apple.ace.speech";

	String						sessionId;
	Recognition					recognition;

	public SpeechRecognized() {
	}

	public SpeechRecognized(String aceId, String refId, Recognition recognition, String sesstionId) {
		super(clazz, group, aceId, refId, null);
		this.recognition = recognition;
		this.sessionId = sesstionId;
	}

	public static SpeechRecognized fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		String sessionId = PropertyListUtils.getStringProperty(plist, "sessionId");

		NSDictionary recognition = (NSDictionary) PropertyListUtils.getNSObjectProperty(plist, "recognition");

		Recognition clearRecognition = null;
		if (recognition != null)
			clearRecognition = Recognition.fromPlist(recognition);

		return new SpeechRecognized(aceId, refId, clearRecognition, sessionId);
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @return the recognition
	 */
	public Recognition getRecognition() {
		return recognition;
	}

}
