package com.wow.siriobjects.speech;

import com.dd.plist.NSDictionary;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class Token extends AceObject {
	public static final String	clazz	= "Token";
	public static final String	group	= "com.apple.ace.speech";

	String						text;
	long						startTime;
	long						endTime;
	Double						confidenceScore;
	boolean						removeSpaceBefore;
	boolean						removeSpaceAfter;

	public Token(String text, long startTime, long endTime, Double confidenceScore, boolean removeSpaceBefore, boolean removeSpaceAfter) {
		super(clazz, group);
		this.text = text;
		this.startTime = startTime;
		this.endTime = endTime;
		this.confidenceScore = confidenceScore;
		this.removeSpaceBefore = removeSpaceBefore;
		this.removeSpaceAfter = removeSpaceAfter;
	}

	public static Token fromPlist(NSDictionary plist) {
		String text = PropertyListUtils.getStringProperty(plist, "text");
		long startTime = PropertyListUtils.getLongProperty(plist, "startTime");
		long endTime = PropertyListUtils.getLongProperty(plist, "endTime");
		Double confidenceScore = PropertyListUtils.getDoubleProperty(plist, "confidenceScore");
		boolean removeSpaceAfter = PropertyListUtils.getBooleanProperty(plist, "removeSpaceAfter");
		boolean removeSpaceBefore = PropertyListUtils.getBooleanProperty(plist, "removeSpaceBefore");

		return new Token(text, startTime, endTime, confidenceScore, removeSpaceBefore, removeSpaceAfter);
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public long getEndTime() {
		return endTime;
	}

	/**
	 * @return the confidenceScore
	 */
	public Double getConfidenceScore() {
		return confidenceScore;
	}

	/**
	 * @return the removeSpaceBefore
	 */
	public boolean isRemoveSpaceBefore() {
		return removeSpaceBefore;
	}

	/**
	 * @return the removeSpaceAfter
	 */
	public boolean isRemoveSpaceAfter() {
		return removeSpaceAfter;
	}

}
