package com.wow.siriobjects.speech;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class Phrase extends AceObject {
	public static final String		clazz	= "Phrase";
	public static final String		group	= "com.apple.ace.speech";

	private List<Interpretation>	interpretations;
	private boolean					lowConfidence;

	public Phrase(List<Interpretation> interpretations, boolean lowConfidence) {
		super(clazz, group);
		this.interpretations = interpretations;
		this.lowConfidence = lowConfidence;
	}

	public static Phrase fromPlist(NSDictionary plist) {
		NSArray interpretations = (NSArray) PropertyListUtils.getNSObjectProperty(plist, "interpretations");
		boolean lowConfidence = PropertyListUtils.getBooleanProperty(plist, "lowConfidence");

		List<Interpretation> clearInterpretations = null;
		if (interpretations != null) {
			clearInterpretations = new ArrayList<Interpretation>();
			for (NSObject o : interpretations) {
				NSDictionary interpretation = (NSDictionary) o;
				Interpretation clearInterpretation = Interpretation.fromPlist(interpretation);
				clearInterpretations.add(clearInterpretation);
			}
		}

		return new Phrase(clearInterpretations, lowConfidence);
	}

	/**
	 * @return the interpretations
	 */
	public List<Interpretation> getInterpretations() {
		return interpretations;
	}

	/**
	 * @return the lowConfidence
	 */
	public boolean isLowConfidence() {
		return lowConfidence;
	}

}
