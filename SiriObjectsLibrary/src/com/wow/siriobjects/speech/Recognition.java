package com.wow.siriobjects.speech;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class Recognition extends AceObject {
	public static final String	clazz	= "Recognition";
	public static final String	group	= "com.apple.ace.speech";

	List<Phrase>				phrases;

	public Recognition(List<Phrase> phrases) {
		super(clazz, group);
		this.phrases = phrases;
	}

	public static Recognition fromPlist(NSDictionary plist) {
		NSArray phrases = (NSArray) PropertyListUtils.getNSObjectProperty(plist, "phrases");

		List<Phrase> clearPhrases = null;
		if (phrases != null) {
			clearPhrases = new ArrayList<Phrase>();
			for (NSObject o : phrases) {
				NSDictionary phrase = (NSDictionary) o;
				Phrase clearPhrase = Phrase.fromPlist(phrase);
				clearPhrases.add(clearPhrase);
			}
		}
		return new Recognition(clearPhrases);
	}

	/**
	 * @return the phrases
	 */
	public List<Phrase> getPhrases() {
		return phrases;
	}

}
