package com.wow.siriobjects.speech;

import java.util.ArrayList;
import java.util.List;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.PropertyListUtils;


public class Interpretation extends AceObject {
	public static final String	clazz	= "Interpretation";
	public static final String	group	= "com.apple.ace.speech";
	private List<Token>			tokens;

	public Interpretation(List<Token> tokens) {
		super(clazz, group);

		this.tokens = tokens;
	}

	public static Interpretation fromPlist(NSDictionary plist) {
		NSArray tokens = (NSArray) PropertyListUtils.getNSObjectProperty(plist, "tokens");

		List<Token> clearTokens = null;
		if (tokens != null) {
			clearTokens = new ArrayList<Token>();
			for (NSObject o : tokens) {
				NSDictionary token = (NSDictionary) o;
				Token clearToken = Token.fromPlist(token);
				clearTokens.add(clearToken);
			}
		}

		return new Interpretation(clearTokens);
	}

	/**
	 * @return the tokens
	 */
	public List<Token> getTokens() {
		return tokens;
	}

}
