package com.wow.siriobjects.speech;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ServerBoundCommand;


public class StartCorrectedSpeechRequest extends ServerBoundCommand {
	public static final String	clazz	= "StartCorrectedSpeechRequest";
	public static final String	group	= "com.apple.ace.speech";

	String						utterance;

	public StartCorrectedSpeechRequest(String aceId, String utterance) {
		super(clazz, group, aceId, null);
		this.utterance = utterance;
	}

	public NSDictionary toPlist() {
		if (utterance != null)
			properties.put("utterance", new NSString(utterance));

		return super.toPlist();
	}
}
