package com.wow.siriobjects.speech;

import com.wow.siriobjects.ClientBoundCommand;

public class SpeechFailure extends ClientBoundCommand {

	public SpeechFailure(String encodedClassName, String groupId, String aceId, String refId) {
		super(encodedClassName, groupId, aceId, refId, null);
	}

}
