package com.wow.siriobjects.websearch;

import java.util.List;

import com.dd.plist.NSDictionary;
import com.dd.plist.NSString;
import com.wow.siriobjects.ClientBoundCommand;
import com.wow.siriobjects.PropertyListUtils;


public class WebSearch extends ClientBoundCommand {
	public static final String	clazz		= "Search";
	public static final String	group		= "com.apple.ace.websearch";

	String						query		= "";
	String						provider	= "Default";
	String						targetAppId	= "";

	public WebSearch() {
	}

	public WebSearch(String aceId, String refId, List<ClientBoundCommand> callbacks, String query, String provider, String targetAppId) {
		super(clazz, group, aceId, refId, callbacks);
		this.query = query;
		this.provider = provider;
		this.targetAppId = targetAppId;
	}

	public NSDictionary toPlist() {
		if (query != null)
			properties.put("query", new NSString(query));
		if (provider != null)
			properties.put("provider", new NSString(provider));
		if (targetAppId != null)
			properties.put("targetAppId", new NSString(targetAppId));

		return super.toPlist();
	}

	public String toString() {
		toPlist();
		return super.toString();
	}

	public static WebSearch fromPlist(NSDictionary plist) {
		String aceId = PropertyListUtils.getAceId(plist);
		String refId = PropertyListUtils.getRefId(plist);
		List<ClientBoundCommand> callbacks = PropertyListUtils.getCallbacks(plist);

		String query = PropertyListUtils.getStringProperty(plist, "query");
		String provider = PropertyListUtils.getStringProperty(plist, "provider");
		String targetAppId = PropertyListUtils.getStringProperty(plist, "targetAppId");

		return new WebSearch(aceId, refId, callbacks, query, provider, targetAppId);
	}

	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @return the targetAppId
	 */
	public String getTargetAppId() {
		return targetAppId;
	}

}
