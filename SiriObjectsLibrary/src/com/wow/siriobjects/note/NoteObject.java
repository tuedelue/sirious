package com.wow.siriobjects.note;

import com.wow.siriobjects.AceObject;

public class NoteObject extends AceObject {
	String contents;
	String identifier;

	public NoteObject(String contents, String identifier) {
		super("Object", "com.apple.ace.notes");
		this.contents = contents;
		this.identifier = identifier;
	}

}
