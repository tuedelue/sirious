package com.wow.siriobjects.note;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.AceObject;


public class NoteSnippet extends AceObject {
	List<NoteObject> notes;
	boolean temporary;
	String dialogPhase;

	public NoteSnippet(List<NoteObject> notes, boolean temporary,
			String dialogPhase) {
		super("Snippet", "com.apple.ace.note");
		this.notes = (notes != null) ? notes : new ArrayList<NoteObject>();
		this.temporary = temporary;
		this.dialogPhase = dialogPhase;
	}

}
