package com.wow.siriobjects.calendar;

import java.util.Date;

import com.wow.siriobjects.AceObject;


public class Event extends AceObject {
	String timeZoneId;
	String title;
	Date startDate;
	Date endDate;

	public Event(String timeZoneId, String title, Date startDate, Date endDate) {
		super("Event", "com.apple.ace.calendar");
		this.timeZoneId = timeZoneId;
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
	}

}
