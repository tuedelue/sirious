package com.wow.siriobjects.calendar;

import java.util.ArrayList;
import java.util.List;

import com.wow.siriobjects.AceObject;
import com.wow.siriobjects.ui.ConfirmationOptions;


public class EventSnippet extends AceObject {
	boolean temporary;
	String dialogPhase;
	List<Event> events;
	ConfirmationOptions confirmationOptions;

	public EventSnippet(boolean temporary, String dialogPhase,
			List<Event> events, ConfirmationOptions confirmationOptions) {
		super("EventSnippet", "com.apple.ace.calendar");
		this.temporary = temporary;
		this.dialogPhase = dialogPhase;
		this.events = (events != null) ? events : new ArrayList<Event>();
		this.confirmationOptions = confirmationOptions;
	}

}
