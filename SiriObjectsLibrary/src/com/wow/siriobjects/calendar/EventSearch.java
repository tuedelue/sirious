package com.wow.siriobjects.calendar;

import java.util.Date;

import com.wow.siriobjects.ClientBoundCommand;


public class EventSearch extends ClientBoundCommand {
	String timeZoneId;
	Date startDate;
	Date endDate;
	Object limit;
	String identifier;

	public EventSearch(String refId, String timeZoneId, Date startDate,
			Date endDate, Object limit, String identifier) {
		super("EventSearch", "com.apple.ace.calendar", null, refId, null);
		this.timeZoneId = timeZoneId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.limit = limit;
		this.identifier = identifier;
	}

}
